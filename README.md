# System Identification Software

This repository contains the software used by the RoboFEI - Small Size League Team to extract the models of their robots.

In order to use all the features of the software you must have the MATLAB software installed, as well as the System Identification
toolbox.

Currently, this software only supports robots from the RoboFEI team, however, there are plans on integrating a generic protocol
to use with other teams robots.

More details about the research made in order to build this software can be seen on [this repository](https://gitlab.com/leo_costa/ic_identificacao).

# Parameters

According to the [research](https://gitlab.com/leo_costa/ic_identificacao) made, the best parameters for the test signals are:
1. PRBS:
    * <img src="https://render.githubusercontent.com/render/math?math=T_{b} = 250 ms">
    * Amplitudes:
        * <img src="https://render.githubusercontent.com/render/math?math=X = 0.3 m/s">
        * <img src="https://render.githubusercontent.com/render/math?math=Y = 0.3 m/s">
        * <img src="https://render.githubusercontent.com/render/math?math=\dot{\theta} = 1.25 rad/s">
2. GBN:
    * Probabilities:
        * <img src="https://render.githubusercontent.com/render/math?math=X = 0.35">
        * <img src="https://render.githubusercontent.com/render/math?math=Y = 0.35">
        * <img src="https://render.githubusercontent.com/render/math?math=\dot{\theta} = 0.8">
    * Amplitudes:
        * <img src="https://render.githubusercontent.com/render/math?math=X = 0.3 m/s">
        * <img src="https://render.githubusercontent.com/render/math?math=Y = 0.3 m/s">
        * <img src="https://render.githubusercontent.com/render/math?math=\dot{\theta} = 1.25 rad/s">

And the model parameters are:

1. ARX:
    * <img src="https://render.githubusercontent.com/render/math?math=n_a=6">
    * <img src="https://render.githubusercontent.com/render/math?math=n_b=5">
    * <img src="https://render.githubusercontent.com/render/math?math=n_k=6">
2. ARMAX:
    * <img src="https://render.githubusercontent.com/render/math?math=n_a=3">
    * <img src="https://render.githubusercontent.com/render/math?math=n_b=2">
    * <img src="https://render.githubusercontent.com/render/math?math=n_c=2">
    * <img src="https://render.githubusercontent.com/render/math?math=n_k=5">
3. Transfer Function (nummber of poles and zeros):
    * <img src="https://render.githubusercontent.com/render/math?math=p=2">
    * <img src="https://render.githubusercontent.com/render/math?math=z=0">