#ifndef ROBOFEINETWORK_H
#define ROBOFEINETWORK_H

#include "robofeicontroller.h"
#include <QDateTime>
#include <QFile>
#include <QHostAddress>
#include <QNetworkInterface>
#include <QObject>
#include <QUdpSocket>

class RoboFeiNetwork
{
public:
  RoboFeiNetwork();
  void conecta(const QString& _sgroupAddressRefBox, const QString& _sgroupAdressVision, int _ipRefBox, int _ipVision);
  void desconecta();
  void processa_ssl_refbox();
  void processa_ssl_vision();
  void setController(RoboFeiController & _RoboController);

private:
  //Ui::RoboFeiVISAO *ui;
  QUdpSocket *udpSocket_refbox; //socket de comunicação com ssl_refbox
  QUdpSocket *udpSocket_sslvision; //socket de comunicação com ssl_vision
  int refbox_packet, sslvision_packet;
  int idetection,igeometry;
  RoboFeiController RoboController;
};

#endif // ROBOFEINETWORK_H
