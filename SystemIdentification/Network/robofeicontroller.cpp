#include "robofeicontroller.h"

RoboFeiController::RoboFeiController()
{
    //ssl_vision
  this->logActive = false;

}

void RoboFeiController::addVision( const SSL_WrapperPacket& _s ){
  if( _s.ByteSize() >0 ){
     this->mutex_vision.lock();
     this->ssl_vision.append(_s);
     if( this->ssl_vision.size() > QTD_MAX_PCT){
        this->ssl_vision.removeFirst();
     }
     this->mutex_vision.unlock();
  }
  return;
}

void RoboFeiController::addReferee( const SSL_Referee& _s ){
  //verificar o pct recebido está cheio e se não é o mesmo time stamp (pct repetido)
  if( _s.ByteSize() > 0 && _s.packet_timestamp() != this->getLastReferee().packet_timestamp())
  {
     this->mutex_referee.lock();
     this->ssl_referee.append(_s);
     if( this->ssl_referee.size() > QTD_MAX_PCT){
        this->ssl_referee.removeFirst();
     }
     this->mutex_referee.unlock();
   }
}


SSL_WrapperPacket RoboFeiController::getLastVision(){
  this->mutex_vision.lock();
  SSL_WrapperPacket _s; // = NULL;
  if( this->ssl_vision.size() > 0 ) _s = this->ssl_vision.last() ;//ssl_vision[ ssl_vision.size() -1];
  this->mutex_vision.unlock();
  return _s;
}

SSL_Referee RoboFeiController::getLastReferee(){
  this->mutex_referee.lock();
  SSL_Referee _s;
  if( this->ssl_referee.size() > 0) _s = this->ssl_referee.last();
  this->mutex_referee.unlock();
  //_s.
  return _s;
}


QString RoboFeiController::toStringWrapperPacket(const SSL_WrapperPacket& _p ){
  std::stringstream out_wrapper;

  //out_wrapper << _p.packet_timestamp();

  if( _p.has_geometry() ){
     const SSL_GeometryData  & geometria = _p.geometry();

     const SSL_GeometryFieldSize & geom_field = geometria.field();
     out_wrapper << "Geom_field_size:"
               //<< dataHeader.timestamp << ";"
               //<< _p.packet_timestamp_fei() << ";"
               << geom_field.field_length() << ";"
               << geom_field.field_width() << ";"
               << geom_field.goal_width() << ";"
               << geom_field.goal_depth() << ";"
               << geom_field.boundary_width()
               << std::endl;

     int qtd_field_lines = geom_field.field_lines_size();
     for( int i=0; i<qtd_field_lines; ++i){
        const SSL_FieldLineSegment & gls = geom_field.field_lines(i);
        out_wrapper  << "Geom_field_lines:"
               //   << dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << gls.name() << ";"
                  << gls.p1().x() << ";"
                  << gls.p1().y() << ";"
                  << gls.p2().x() << ";"
                  << gls.p2().y() << ";"
                  << gls.thickness()
                  << std::endl;
     }

     int qtd_field_arcs = geom_field.field_arcs_size();
     for( int i=0; i < qtd_field_arcs; ++i){
        //std::cout << "FIELD-ARCS" << std::endl;
        const SSL_FieldCicularArc & gla = geom_field.field_arcs(i);
        out_wrapper  << "Geom_field_cicular_arc:"
                  //<< dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << gla.name() << ";"
                  << gla.center().x() << ";"
                  << gla.center().y() << ";"
                  << gla.radius() << ";"
                  << gla.a1() << ";"
                  << gla.a2() << ";"
                  << gla.thickness()
                  << std::endl;
     }

     int qtd_cam_calib = geometria.calib_size();
     //const SSL_GeometryCameraCalibration & calib
     for( int i=0; i<qtd_cam_calib;++i){
        const SSL_GeometryCameraCalibration & calib = geometria.calib(i);
        out_wrapper  << "Calibration:"
                  //<< dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << calib.camera_id() << ";"
                  << calib.focal_length() << ";"
                  << calib.principal_point_x() << ";"
                  << calib.principal_point_y() << ";"
                  << calib.distortion() << ";"
                  << calib.q0() << ";"
                  << calib.q1() << ";"
                  << calib.q2() << ";"
                  << calib.q3() << ";"
                  << calib.tx() << ";"
                  << calib.ty() << ";"
                  << calib.tz() << ";"
                  << calib.derived_camera_world_tx() << ";"
                  << calib.derived_camera_world_ty()
                  << calib.derived_camera_world_tz() << std::endl;
     }
  }

  if( _p.has_detection() ){
      const SSL_DetectionFrame & frame = _p.detection();
      out_wrapper << "Frame:"
                //<< dataHeader.timestamp << ";"
                //<< _p.packet_timestamp_fei() << ";"
                << frame.frame_number() <<  ";"
                << frame.t_capture() << ";"
                << frame.t_sent() << ";"
                << frame.camera_id() << std::endl;

      int qtd_bolas = frame.balls_size();
     for( int i=0;i<qtd_bolas;++i ){

        const SSL_DetectionBall & bola = frame.balls(i);
        out_wrapper << "Bola:"
                  //<< dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << bola.confidence() << ";"
                  << bola.area() << ";"
                  << bola.x() << ";"
                  << bola.y() << ";"
                  << bola.z() << ";"
                  << bola.pixel_x() << ";"
                  << bola.pixel_y() << std::endl;
     }

     int qtd_blue = frame.robots_blue_size();
     for( int i=0; i<qtd_blue; ++i){
        const SSL_DetectionRobot & blue = frame.robots_blue(i);

        out_wrapper << "Blue:"
                  //<< dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << blue.confidence() << ";"
                  << blue.robot_id() << ";"
                  << blue.x() << ";"
                  << blue.y() << ";"
                  << blue.orientation() << ";"
                  << blue.pixel_x() << ";"
                  << blue.pixel_y() << ";"
                  << blue.height() << std::endl;
     }

     int qtd_yellow = frame.robots_yellow_size();
     for( int i=0; i<qtd_yellow; ++i){
        const SSL_DetectionRobot & yellow = frame.robots_yellow(i);

        out_wrapper << "Yellow:"
                  //<< dataHeader.timestamp << ";"
                  //<< _p.packet_timestamp_fei() << ";"
                  << i << ";"
                  << yellow.confidence() << ";"
                  << yellow.robot_id() << ";"
                  << yellow.x() << ";"
                  << yellow.y() << ";"
                  << yellow.orientation() << ";"
                  << yellow.pixel_x() << ";"
                  << yellow.pixel_y() << ";"
                  << yellow.height() << std::endl;

     }
  }
  return QString( out_wrapper.str().c_str() );
}


QString RoboFeiController::toStringReferee( const SSL_Referee& _s){

  std::stringstream out_referee;
  out_referee << "Referee:"
              << _s.packet_timestamp() << ";"
              //<< _s.packet_timestamp_fei() << ";"
              << _s.stage() << ";"
              << _s.stage_time_left() << ";"
              << _s.command() << ";"
              << _s.command_counter() << ";"
              << _s.yellow().name() << ";"
              << _s.yellow().score() << ";"
              << _s.yellow().red_cards() << ";"
              << _s.yellow().yellow_cards() << ";"
              << _s.yellow().timeouts() << ";"
              << _s.yellow().timeout_time() << ";"
              << _s.yellow().goalie() << ";"
              << _s.blue().name() << ";"
              << _s.blue().score() << ";"
              << _s.blue().red_cards() << ";"
              << _s.blue().yellow_cards() << ";"
              << _s.blue().timeouts() << ";"
              << _s.blue().timeout_time() << ";"
              << _s.blue().goalie() << ";"
              << std::endl;

   return QString( out_referee.str().c_str() );
}

//gerar log - 11/09/2017 - MAL
void RoboFeiController::start_log(const QString& _lr, const QString& _lv)
{
      refereeLogFile.setFileName(_lr);
      visionLogFile.setFileName(_lv);
      refereeLogFile.open(QIODevice::WriteOnly | QIODevice::Text |QIODevice::Append);//, QFile::AutoCloseHandle);
      visionLogFile.open(QIODevice::WriteOnly | QIODevice::Text |QIODevice::Append);//, QFile::AutoCloseHandle);
      logActive = true;

}

void RoboFeiController::write_log_vision(  ){
   if( logActive ){
       QTextStream outv(&visionLogFile);
       outv << RoboFeiController::toStringWrapperPacket( this->getLastVision());
   }
}

void RoboFeiController::write_log_referee( ){
   if( logActive ){
       QTextStream outr(&refereeLogFile);
       outr << RoboFeiController::toStringReferee(this->getLastReferee());
   }
}

void RoboFeiController::stop_log(){
   refereeLogFile.close();
   visionLogFile.close();
   logActive = false;
}

QString RoboFeiController::getStage(const SSL_Referee & ssl_referee){
   switch(ssl_referee.stage()){
      case SSL_Referee::NORMAL_FIRST_HALF_PRE:{
         return QString("Normal First Half Pre");
        }
      case SSL_Referee::NORMAL_FIRST_HALF:{
          return QString("Normal First Half");
        }
      case SSL_Referee::NORMAL_HALF_TIME:{
          return QString("Normal Half Time");
        }
      case SSL_Referee::NORMAL_SECOND_HALF_PRE:{
          return QString("Normal Second Half Pre");
        }
      case SSL_Referee::NORMAL_SECOND_HALF:{
          return QString("Normal Second Half");
        }
      case SSL_Referee::EXTRA_TIME_BREAK:{
          return QString("Extra Time Break");
        }
      case SSL_Referee::EXTRA_FIRST_HALF_PRE:{
          return QString("Extra First Half Pre");
        }
      case SSL_Referee::EXTRA_FIRST_HALF:{
          return QString("Extra First Half");
        }
      case SSL_Referee::EXTRA_HALF_TIME:{
          return QString("Extra Half Time");
        }
      case SSL_Referee::EXTRA_SECOND_HALF_PRE:{
          return QString("Extra Second Half Pre");
        }
      case SSL_Referee::EXTRA_SECOND_HALF:{
          return QString("Exta Second Half");
        }
      case SSL_Referee::PENALTY_SHOOTOUT_BREAK:{
          return QString("Penalty Shootout Break");
        }
      case SSL_Referee::PENALTY_SHOOTOUT:{
          return QString("Penalty Shootout");
        }
      case SSL_Referee::POST_GAME:{
          return QString("Post Game");
        }
      }
   return QString("Unknow");
}

QString RoboFeiController::getCommand(const SSL_Referee & ssl_referee){
     switch(ssl_referee.command()) {
       case SSL_Referee::HALT:{
          return QString("Halt");
       }
       case SSL_Referee::STOP:{
          return QString("Stop");
       }
       case SSL_Referee::NORMAL_START:{
          return QString("Normal Start");
       }
       case SSL_Referee::FORCE_START:{
          return QString("Force Start");
       }
       case SSL_Referee::PREPARE_KICKOFF_YELLOW:{
          return QString("Prepare Kickoff Yellow");
       }
       case SSL_Referee::PREPARE_KICKOFF_BLUE:{
          return QString("Prepare Kickoff Blue");
       }
       case SSL_Referee::PREPARE_PENALTY_YELLOW:{
          return QString("Prepare Penalty Yellow");
       }
       case SSL_Referee::PREPARE_PENALTY_BLUE:{
          return QString("Prepare Penalty Blue");
       }
       case SSL_Referee::DIRECT_FREE_YELLOW:{
          return QString("Direct Free Yellow");
       }
       case SSL_Referee::DIRECT_FREE_BLUE:{
          return QString("Direct Free Blue");
       }
       case SSL_Referee::INDIRECT_FREE_YELLOW:{
         return QString("Indirect Free Yellow");
       }
       case SSL_Referee::INDIRECT_FREE_BLUE:{
         return QString("Indirect Free Blue");
       }
       case SSL_Referee::TIMEOUT_YELLOW:{
          return QString("Timeout Yellow");
       }
       case SSL_Referee::TIMEOUT_BLUE:{
          return QString("Timeout Blue");
       }
       case SSL_Referee::GOAL_YELLOW:{
          return QString("Goal Yellow");
       }
       case SSL_Referee::GOAL_BLUE:{
          return QString("Goal Blue");
       }
       case SSL_Referee::BALL_PLACEMENT_YELLOW:{
          return QString("Ball Placement Yellow");
       }
       case SSL_Referee::BALL_PLACEMENT_BLUE:{
          return QString("Ball Placement Blue");
       }
    }
    return QString("Unknow");
}

