#include "robofeinetwork.h"


RoboFeiNetwork::RoboFeiNetwork()
{

}

//void RoboFeiNetwork::setController(RoboFeiController & _RoboController){
//  this->RoboController = _RoboController;
//}

void RoboFeiNetwork::conecta(const QString& _sgroupAddressRefBox, const QString& _sgroupAdressVision, int _ipRefBox, int _ipVision){
 // ui->c_pb_capture_packets->setText("Stop Packets Capture");

  /*QHostAddress groupAddressRefBox(_sgroupAddressRefBox);
  udpSocket_refbox = new QUdpSocket(this);
  if (udpSocket_refbox->state() != udpSocket_refbox->BoundState){
     udpSocket_refbox->bind(QHostAddress::AnyIPv4, _ipRefBox, QUdpSocket::ShareAddress);
  }
  udpSocket_refbox->open(QUdpSocket::ReadWrite);
  udpSocket_refbox->joinMulticastGroup(groupAddressRefBox);
  connect(udpSocket_refbox, SIGNAL(readyRead()),
          this, SLOT(processa_ssl_refbox()));

  QHostAddress groupAddressSslVision(_sgroupAdressVision);
  udpSocket_sslvision = new QUdpSocket(this);
  if (udpSocket_sslvision->state() != udpSocket_sslvision->BoundState){
     udpSocket_sslvision->bind(QHostAddress::AnyIPv4, _ipVision, QUdpSocket::ShareAddress);
  }
  udpSocket_sslvision->open(QUdpSocket::ReadWrite);
  udpSocket_sslvision->joinMulticastGroup(groupAddressSslVision);
  connect(udpSocket_sslvision, SIGNAL(readyRead()),
          this, SLOT(processa_ssl_vision()));

  connect(ui->c_pb_capture_packets, SIGNAL (released()), this, SLOT (desconecta()));

 // RoboController.start_log( ui->c_le_referee_logfile->text(), ui->c_le_vision_logfile->text(),
  //                          ui->m_rb_write_log->isChecked() );
 // ui->m_rb_write_log->setEnabled(false);*/
}


void RoboFeiNetwork::desconecta(){
  //connect(ui->c_pb_capture_packets, SIGNAL (released()), this, SLOT (conecta()));
  //ui->c_pb_capture_packets->setText("Start Packets Capture");
  //RoboController.stop_log();
  //ui->m_rb_write_log->setEnabled(true);

  //udpSocket_refbox->leaveMulticastGroup();
  udpSocket_refbox->disconnect(SIGNAL(readyRead()));
  udpSocket_refbox->close();
  udpSocket_sslvision->disconnect(SIGNAL(readyRead()));
  udpSocket_sslvision->close();
}


void RoboFeiNetwork::processa_ssl_vision(){
//  ui->m_te_geometry->setText("Estou aqui");

  while (udpSocket_sslvision->hasPendingDatagrams()) {
     QByteArray datagram;
     datagram.resize(udpSocket_sslvision->pendingDatagramSize());
     datagram.fill(0,udpSocket_sslvision->pendingDatagramSize());
     udpSocket_sslvision->readDatagram(datagram.data(), datagram.size());
     SSL_WrapperPacket sslvision;

     sslvision.ParseFromArray(datagram,datagram.size());
     //timestamp.setDate(localtime(NULL));

     //sslvision.set_packet_timestamp_fei(QDateTime::currentDateTime().toTime_t() ); //coloca o timestamp da FEI
     RoboController.addVision(sslvision);
     RoboController.write_log_vision();

     if( sslvision.has_detection() ){
    //    ui->m_te_detection_counter->setText(QString::number(idetection));
        ++idetection;
     }
     if( sslvision.has_geometry() ){
      //  ui->m_te_geometry->setText(QString::number(igeometry));
        ++igeometry;
     }

     //updateRobotPos();//Atualiza as coordenadas do robo atraves da visao
  }
}

void RoboFeiNetwork::processa_ssl_refbox(){
  while (udpSocket_refbox->hasPendingDatagrams()) {
          QByteArray datagram;
          datagram.resize(udpSocket_refbox->pendingDatagramSize());
          datagram.fill(0,udpSocket_refbox->pendingDatagramSize());
          udpSocket_refbox->readDatagram(datagram.data(), datagram.size());

          SSL_Referee ssl_referee;

          ssl_referee.ParseFromArray(datagram,datagram.size());//captura os dados do ssl_refbox

          //ssl_referee.set_packet_timestamp_fei(QDateTime::currentDateTime().toTime_t() ); //coloca o timestamp da FEI
       //   RoboController.addReferee(ssl_referee);
          RoboController.write_log_referee();



          //general
          /*QDateTime timestamp;
          timestamp.setTime_t(ssl_referee.packet_timestamp()/1000000);
          ui->m_te_time_stamp->setText(timestamp.toString("HH:mm:ss"));
          ui->m_te_stage->setText( SSL_Ref::getStage(ssl_referee) );
          ui->m_te_command->setText( SSL_Ref::getCommand(ssl_referee));
          ++refbox_packet;
          ui->m_te_packet_counter->setText( QString::number(refbox_packet));
          ui->m_te_command_counter->setText( QString::number(ssl_referee.command_counter()));
          timestamp.setTime_t(ssl_referee.command_timestamp()/1000000);
          ui->m_te_command_time_stamp->setText(timestamp.toString("HH:mm:ss"));

       //   timestamp.setDate(localtime(NULL));


          //yellow
          ui->m_te_yellow_name->setText(FSYELLOW+ QString(ssl_referee.yellow().name().c_str()) + FEYELLOW);
          ui->m_te_yellow_placar->setText( FSYELLOW+QString::number(ssl_referee.yellow().score())+ FEYELLOW);
          ui->m_te_red_card_yellow->setText(FSYELLOW+ QString::number(ssl_referee.yellow().red_cards())+ FEYELLOW);
          ui->m_te_yellow_card_yellow->setText(FSYELLOW+ QString::number(ssl_referee.yellow().yellow_cards())+ FEYELLOW);
          if( ssl_referee.yellow().yellow_card_times_size() >0 ){
             //for( int i=0; i<ssl_referee.yellow().yellow_card_times_size();++i){
             timestamp.setTime_t(ssl_referee.yellow().yellow_card_times(0)/1000000); //pega o primeiro
             ui->m_te_yellow_card_time_yellow_0->setText( FSYELLOW+timestamp.toString("mm:ss")+FEYELLOW);
             if( ssl_referee.yellow().yellow_card_times_size()>1){
                 timestamp.setTime_t(ssl_referee.yellow().yellow_card_times(1)/1000000); //pega o segundo, se tiver mais de 2 cartões, azar
                 ui->m_te_yellow_card_time_yellow_1->setText( FSYELLOW+timestamp.toString("mm:ss")+FEYELLOW);
             }
          }
          else{
               ui->m_te_yellow_card_time_yellow_0->setText(FSYELLOW+QString("00:00")+FEYELLOW);
               ui->m_te_yellow_card_time_yellow_1->setText(FSYELLOW+QString("00:00")+FEYELLOW);
          }
          ui->m_te_timeouts_yellow->setText(FSYELLOW +QString::number(ssl_referee.blue().timeouts())+ FEYELLOW);
          timestamp.setTime_t(ssl_referee.yellow().timeout_time()/1000000);
          ui->m_te_timeout_time_yellow->setText(FSYELLOW +timestamp.toString("mm:ss"));
          ui->m_te_goalie_yellow->setText( FSYELLOW + QString::number(ssl_referee.blue().goalie()) + FEYELLOW);

          //blue
          ui->m_te_blue_name->setText( FSBLUE + QString(ssl_referee.blue().name().c_str()) + FEBLUE);
          ui->m_te_blue_score->setText( FSBLUE +QString::number(ssl_referee.blue().score())+ FEBLUE);
          ui->m_te_red_card_blue->setText( FSBLUE +QString::number(ssl_referee.blue().red_cards())+ FEBLUE);
          ui->m_te_yellow_card_blue->setText( FSBLUE +QString::number(ssl_referee.blue().yellow_cards())+ FEBLUE);
          if( ssl_referee.blue().yellow_card_times_size() >0 ){
             //for( int i=0; i<ssl_referee.blue().yellow_card_times_size();++i){
             timestamp.setTime_t(ssl_referee.blue().yellow_card_times(0)/1000000); //pega o primeiro
             ui->m_te_yellow_card_time_blue_0->setText( FSBLUE+timestamp.toString("mm:ss")+FEBLUE);
             if( ssl_referee.blue().yellow_card_times_size()>1){
                 timestamp.setTime_t(ssl_referee.blue().yellow_card_times(1)/1000000); //pega o segundo, se tiver mais de 2 cartões, azar
                 ui->m_te_yellow_card_time_blue_1->setText( FSBLUE+timestamp.toString("mm:ss")+FEBLUE);
             }
          }
          else{
               ui->m_te_yellow_card_time_blue_0->setText(FSBLUE+QString("00:00")+FEBLUE);
               ui->m_te_yellow_card_time_blue_1->setText(FSBLUE+QString("00:00")+FEBLUE);
          }
          ui->m_te_timeouts_blue->setText(FSBLUE +QString::number(ssl_referee.blue().timeouts())+ FEBLUE);
          timestamp.setTime_t(ssl_referee.blue().timeout_time()/1000000);
          ui->m_te_timeout_time_blue->setText(FSBLUE +timestamp.toString("mm:ss"));
          ui->m_te_goalie_blue->setText( FSBLUE + QString::number(ssl_referee.blue().goalie()) + FEBLUE);
*/

      }
}
