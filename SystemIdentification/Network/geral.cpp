#include "geral.h"

QString SSL_Ref::getStage(const SSL_Referee & ssl_referee){
   switch(ssl_referee.stage()){
      case SSL_Referee::NORMAL_FIRST_HALF_PRE:{
         return QString("Normal First Half Pre");
        }
      case SSL_Referee::NORMAL_FIRST_HALF:{
          return QString("Normal First Half");
        }
      case SSL_Referee::NORMAL_HALF_TIME:{
          return QString("Normal Half Time");
        }
      case SSL_Referee::NORMAL_SECOND_HALF_PRE:{
          return QString("Normal Second Half Pre");
        }
      case SSL_Referee::NORMAL_SECOND_HALF:{
          return QString("Normal Second Half");
        }
      case SSL_Referee::EXTRA_TIME_BREAK:{
          return QString("Extra Time Break");
        }
      case SSL_Referee::EXTRA_FIRST_HALF_PRE:{
          return QString("Extra First Half Pre");
        }
      case SSL_Referee::EXTRA_FIRST_HALF:{
          return QString("Extra First Half");
        }
      case SSL_Referee::EXTRA_HALF_TIME:{
          return QString("Extra Half Time");
        }
      case SSL_Referee::EXTRA_SECOND_HALF_PRE:{
          return QString("Extra Second Half Pre");
        }
      case SSL_Referee::EXTRA_SECOND_HALF:{
          return QString("Exta Second Half");
        }
      case SSL_Referee::PENALTY_SHOOTOUT_BREAK:{
          return QString("Penalty Shootout Break");
        }
      case SSL_Referee::PENALTY_SHOOTOUT:{
          return QString("Penalty Shootout");
        }
      case SSL_Referee::POST_GAME:{
          return QString("Post Game");
        }
      }
   return QString("Unknow");
}

QString SSL_Ref::getCommand(const SSL_Referee & ssl_referee){
     switch(ssl_referee.command()) {
       case SSL_Referee::HALT:{
          return QString("Halt");
       }
       case SSL_Referee::STOP:{
          return QString("Stop");
       }
       case SSL_Referee::NORMAL_START:{
          return QString("Normal Start");
       }
       case SSL_Referee::FORCE_START:{
          return QString("Force Start");
       }
       case SSL_Referee::PREPARE_KICKOFF_YELLOW:{
          return QString("Prepare Kickoff Yellow");
       }
       case SSL_Referee::PREPARE_KICKOFF_BLUE:{
          return QString("Prepare Kickoff Blue");
       }
       case SSL_Referee::PREPARE_PENALTY_YELLOW:{
          return QString("Prepare Penalty Yellow");
       }
       case SSL_Referee::PREPARE_PENALTY_BLUE:{
          return QString("Prepare Penalty Blue");
       }
       case SSL_Referee::DIRECT_FREE_YELLOW:{
          return QString("Direct Free Yellow");
       }
       case SSL_Referee::DIRECT_FREE_BLUE:{
          return QString("Direct Free Blue");
       }
       case SSL_Referee::INDIRECT_FREE_YELLOW:{
         return QString("Indirect Free Yellow");
       }
       case SSL_Referee::INDIRECT_FREE_BLUE:{
         return QString("Indirect Free Blue");
       }
       case SSL_Referee::TIMEOUT_YELLOW:{
          return QString("Timeout Yellow");
       }
       case SSL_Referee::TIMEOUT_BLUE:{
          return QString("Timeout Blue");
       }
       case SSL_Referee::GOAL_YELLOW:{
          return QString("Goal Yellow");
       }
       case SSL_Referee::GOAL_BLUE:{
          return QString("Goal Blue");
       }
       case SSL_Referee::BALL_PLACEMENT_YELLOW:{
          return QString("Ball Placement Yellow");
       }
       case SSL_Referee::BALL_PLACEMENT_BLUE:{
          return QString("Ball Placement Blue");
       }
    }
    return QString("Unknow");
}
