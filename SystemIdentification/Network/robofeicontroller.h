#include "protofiles/messages_robocup_ssl_detection.pb.h"
#include "protofiles/messages_robocup_ssl_geometry.pb.h"
#include "protofiles/messages_robocup_ssl_refbox_log.pb.h"
#include "protofiles/messages_robocup_ssl_wrapper.pb.h"
#include "protofiles/referee.pb.h"
#include <QFile>
#include <QMutex>
#include <QString>
#include <QTextStream>
#include <QVector>
#include <sstream>
#include <QUdpSocket>
#include <QNetworkInterface>

#define QTD_MAX_PCT 200

using namespace std;

#ifndef ROBOFEICONTROLLER_H
#define ROBOFEICONTROLLER_H

class RoboFeiController
{
    private:
    //trocado de vector por qlist em 09/09/2017 - MAL
    QList <SSL_WrapperPacket> ssl_vision;
    QList <SSL_Referee> ssl_referee;
    //controle por mutex em 09/09/2017 - MAL
    QMutex mutex_vision;
    QMutex mutex_referee;

    //log formato humano em 11/09/2017 - MAL
    QFile refereeLogFile;
    QFile visionLogFile;
    bool logActive;
public:
  RoboFeiController();
  void addVision( const SSL_WrapperPacket& _s);
  void addReferee( const SSL_Referee& _s );
  SSL_WrapperPacket getLastVision();
  SSL_Referee getLastReferee();
  QString toStringReferee( const  SSL_Referee& _s);
  QString toStringWrapperPacket(const  SSL_WrapperPacket& _p );
  void start_log(const QString& _lr, const QString& _lv);
  void write_log_referee();
  void write_log_vision();
  void stop_log();
  static QString getStage(const SSL_Referee & ssl_referee);
  static QString getCommand(const SSL_Referee & ssl_referee);
};

#endif // ROBOFEICONTROLLER_H
