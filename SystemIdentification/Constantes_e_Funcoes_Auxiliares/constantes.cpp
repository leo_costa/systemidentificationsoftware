#include "constantes.h"
#include <QString>


int tempoAmostragem = 15;
//****************************************** Movimentacao ************************
double dTempoAceleracaoPadrao      = 0.4; // s, definido empiricamente no lab 08/03/2018 - Leonardo Costa e Carlos Cadamuro
double dTempoDesaceleracaoPadrao   = 1;   // s, definido empiricamente no lab 08/03/2018 - Leonardo Costa e Carlos Cadamuro
double dVelocidadeStop = 1.5;
double dDistanciaAceleracaoPadrao = 400; //mm
double dDistanciaFrenagemPadrao = 600; //mm
double dVelocidadeTestes = 40;
int iTamanhoTracoRobo = 50;
int iVelocidadeDelta = 30; // 0 - 100
//****************************************** Movimentacao ************************

//****************************************** Parametros do Campo *****************
int iTamanhoBufferBola = 20;
int iDeslocamentoBola = 180;//Deslocamento minimo da bola para considerar que ela se moveu e o jogo deve seguir
int iOffsetCampo       = 100; //Tamanho a mais das linhas limites do campo
int iLarguraDivisaoCampo = 1500;
int iRaioRefereeCircle = 500;
int iFramesParaSairDaVisao = 20; //Quantidade de pacotes para considerar que o robo saiu da visao
double dFPS = 62.5;
bool bDesenharAreaSeguranca = false; //Habilita/Desabilita desenhar a área de segurança em volta dos robôs
int iAnguloGolContra = 60;
//****************************************** Parametros do Campo *****************

//****************************************** Parametros Path-Planners *********************
float fDistanciaSeguranca = iRaioSegurancaRobo;
int iRaioSegurancaRobo = 240; //Distância de segurança entre os robôs
int iRaioSegurancaBola = 300; //Distância de segurança entre o robô e a bola
int iRaioSegurancaOponente = 240; //Distancia de seguranca entre os robos oponentes
//****************************************** Parametros Mapa *********************

//****************************************** Jogadas *****************************
int iLarguraMinimaMira = 270;
int iEspacamentoRobosDelta = 235;
int iDistanciaRobosDefesaGolAreaPenalty = 360;
int iDistanciaReceptorCobranca = 3000;
int iForcaChuteCalibrado = 15;
int iTempoCalculoPosicionamento = 500;
qint64 iTempoAvancoRobo = 1e9; //2e9 ns = 2s
int iNumeroRobosDelta = 2;
//****************************************** Jogadas *****************************

QString strTempoReferee = QStringLiteral("5:00");

QMutex mtInterface_Estrategia;
QMutex mtInterface_Movimentacao;
QMutex mtEstrategia_Movimentacao;
QMutex mtTeste;


