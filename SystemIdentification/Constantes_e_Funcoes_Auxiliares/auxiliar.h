
#ifndef AUXILIAR_H
#define AUXILIAR_H

#include <QVector>
#include <QVector2D>
#include <QVector4D>
#include <cmath>
#include <QDebug>
#include <eigen3/Eigen/LU>
#include <eigen3/Eigen/Dense>
#include <QRandomGenerator>
#include <qmath.h>


#include <iostream>
#include <cstdlib>


/**
 * @brief
 *
 */
class clsAuxiliar
{
public:
    /**
     * @brief
     *
     */
    clsAuxiliar();

    /**
     * @brief
     *
     * @param _angulo
     * @return int
     */
    static int iCalculaQuadrante180(double _angulo);

    /**
     * @brief
     *
     * @param _angulo
     * @return int
     */
    static int iCalculaQuadrante360(double _angulo);

    //Retorna um ponto P3 na direcao da reta P1-P2 a uma distancia Dp1 do ponto P1
    /**
     * @brief
     *
     * @param _vt2dPonto1
     * @param _vt2dPonto2
     * @param fDistanciaPonto1
     * @return QVector2D
     */
    static QVector2D vt2dReajustaPonto(QVector2D _vt2dPonto1, QVector2D _vt2dPonto2, float fDistanciaPonto1);

    //Rotaciona o ponto P2 em torno de P1 em um certo angulo
    /**
     * @brief
     *
     * @param _vt2dP1
     * @param _vt2dP2
     * @param _dAngulo_graus
     * @return QVector2D
     */
    static QVector2D vt2dRotaciona(QVector2D _vt2dP1, QVector2D _vt2dP2, double _dAngulo_graus);

    //Determina se um ponto esta contido dentro do retangulo definido pelos 3 pontos enviados
    /**
     * @brief
     *
     * @param ponto
     * @param R1
     * @param R2
     * @param R3
     * @return bool
     */
    static bool bPontoContidoRetangulo(QVector2D ponto, QVector2D R1, QVector2D R2, QVector2D R3);

    //Calcula o angulo entre dois vetores
    /**
     * @brief
     *
     * @param vetor1
     * @param vetor2
     * @return float
     */
    static float fAnguloVetor1Vetor2(QVector2D vetor1, QVector2D vetor2);

    //Calcula um vetor perpendicular a projecao do vetor V1 em V2
    /**
     * @brief
     *
     * @param V1
     * @param V2
     * @return QVector2D
     */
    static QVector2D vt2dVetorPerpendicular(QVector2D V1, QVector2D V2);
};



#endif // AUXILIAR_H
