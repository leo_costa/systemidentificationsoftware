#ifndef CONSTANTES_H
#define CONSTANTES_H

#include <QMutex>
#include <qmath.h>

#define tempoAtualizacaoInterface  20 //ms
extern int tempoAmostragem;//ms

#define tempoAtualizacaoStep 2 //ms
#define tempoAtualizacaoSeno 5 //ms

//****************************************** Movimentacao ************************
extern double dTempoAceleracaoPadrao; // 0.4s em aprox 400mm, definido empiricamente no lab 08/03/2018 - Leonardo Costa e Carlos Cadamuro
extern double dTempoDesaceleracaoPadrao;   // 0.6s em aprox 600mm, definido empiricamente no lab 08/03/2018 - Leonardo Costa e Carlos Cadamuro
extern double dVelocidadeStop;
extern double dDistanciaAceleracaoPadrao; //mm
extern double dDistanciaFrenagemPadrao; //mm
extern double dVelocidadeTestes;// 0 - 100
extern int iTamanhoTracoRobo;
extern int iVelocidadeDelta; // 0 - 100
const double dVelocidadeMaximaRobo = 2.5; // m/s
const double dRotacaoMaximoRobo = 6.25; // rad/s
const int iTempoLoop = 50; //50ms
//****************************************** Movimentacao ************************

//****************************************** Parametros do Campo *****************
#define XPositivo  1
#define XNegativo -1
#define CampoReal  1
#define GrSim      0
#define TesteMeioCampo 1
#define CampoInteiro 0
#define Testes 0
#define Jogo   1
#define DesenhoVertical 1
#define DesenhoHorizontal 0
const int iNumeroTotalCameras = 4;
const int iDiametroBola       = 50;  //Diâmetro da bola
const int iDiametroRobo       = 180; //Diâmetro do robô
const int PLAYERS_PER_SIDE    = 12;
const int iEspessuraLinha     = 0;  //Espessura das linhas do campo
const int iTamanhoBufferRobo  = 5;

enum Time
{
    timeAzul,
    timeAmarelo,
    timeNenhum,
};

extern int iTamanhoBufferBola;
extern int iDeslocamentoBola;
extern int iOffsetCampo; //Tamanho a mais das linhas limites do campo
extern int iLarguraDivisaoCampo;
extern int iRaioRefereeCircle;
extern int iFramesParaSairDaVisao; //Quantidade de pacotes para considerar que o robo saiu da visao
extern double dFPS;
extern bool bDesenharAreaSeguranca; //Habilita/Desabilita desenhar a área de segurança em volta dos robôs
extern QString strTempoReferee;
extern int iAnguloGolContra;
//****************************************** Parametros do Campo *****************

//****************************************** Parametros Path-Planners *********************
#define bIgnoraBola    1 //Indica para os pathplanners se a bola deve ou não ser considerada como obstáculo
#define bNaoIgnoraBola 0 // "
extern float fDistanciaSeguranca;
extern int iRaioSegurancaRobo; //Distância de segurança entre os robôs
extern int iRaioSegurancaBola; //Distância de segurança entre o robô e a bola
extern int iRaioSegurancaOponente; //Distancia de seguranca entre os robos oponentes
const float fResolucaoCampo  = 50;  //Resolução do mapa em mm
const int iAnguloGrafo = 60; //Angulo em graus entre os pontos criados para cada objeto no grafo
//****************************************** Parametros Mapa *********************

//****************************************** Jogadas *****************************
//Tipo de cruzamento numa cobrança, no cruzamento em Y o chute é feito predominantemente na direção +- Y
//no cruzamente em X o chute é feito predominantemente na direção +- X
#define CruzamentoY 1
#define CruzamentoX 0
const int iJogadasAtaque = 6;
extern int iLarguraMinimaMira;
extern int iEspacamentoRobosDelta;
extern int iDistanciaRobosDefesaGolAreaPenalty;
extern int iDistanciaReceptorCobranca;
extern int iForcaChuteCalibrado;
extern int iTempoCalculoPosicionamento;
extern qint64 iTempoAvancoRobo;
extern int iNumeroRobosDelta;
//****************************************** Jogadas *****************************

extern QMutex mtInterface_Estrategia;
extern QMutex mtInterface_Movimentacao;
extern QMutex mtEstrategia_Movimentacao;
extern QMutex mtTeste;

enum ID_Jogadas
{
    Ataque_Cobrador_KickOff            ,//0
    Ataque_Cobrador_Penalty            ,//1
    Ataque_Cobrador_Indirect           ,//2
    Ataque_Cobrador_Direct             ,//3
    Normal_Game_Robo_Extra_Bola        ,//4
    Ataque_Receptor_Indirect           ,//5
    Nenhuma                            ,//6
    Marcador_Bola_Referee_Circle       ,//7
    Halt                               ,//8
    Time_Out                           ,//9
    Normal_Game                        ,//10
    PSO_Ataque_Indirect                ,//11
    PSO_Defesa_Indirect                ,//12
    PSO_Ataque_Direct                  ,//13
    PSO_Defesa_Direct                  ,//14
    PSO_Normal_Game                    ,//15
    Defesa_Marcador_Indirect           ,//16
    Defesa_Marcador_Direct             ,//17
    Normal_Game_Robo_Extra_Gol         ,//18
    Defesa_Referee_Circle              ,//19
    Recebendo_Passe                    ,//20
    Delta                              ,//21
    Defesa_KickOff                     ,//22
    Goleiro                            ,//23
    Posicionando_Cobrador              ,//24
    Posicionando_Receptor              ,//25
    Defesa_Penalty                     ,//26
    Posicionando_Passe_InGame          ,//27
};

enum ID_Testes
{
    tstNormal          ,
    tstStop_Game       ,
    tstHalt            ,
    tstForce_Start     ,
    tstTimeout         ,
    tstDirect_Azul     ,
    tstIndirect_Azul   ,
    tstKickoff_Azul    ,
    tstPenalty_Azul    ,
    tstDirect_Amarelo  ,
    tstIndirect_Amarelo,
    tstKickoff_Amarelo ,
    tstPenalty_Amarelo ,
    tstPSO             ,
    tstMovInd          ,
    tstPathPlanner     ,
    tstMovClick        ,
    tstPasses          ,
    tstDelta           ,
};

typedef struct structPasses
{
    int iRoboID1, iRoboID2;
    int iDistanciaRobos;
    int iForcaChute, iVelocidadeRoller;

    structPasses()
    {
        iRoboID1 = -1;
        iRoboID2 = -1;
        iDistanciaRobos = -1;
        iForcaChute = -1;
        iVelocidadeRoller = -1;
    }
}structPasses;



#endif // CONSTANTES_H
