#include "auxiliar.h"

clsAuxiliar::clsAuxiliar()
= default;

int clsAuxiliar::iCalculaQuadrante180(double _angulo)
{
    if(_angulo >= 0 && _angulo < M_PI/2.0)//Primeiro
    {
        return 1;
    }
    else if(_angulo > M_PI/2.0 && _angulo < M_PI)//Segundo
    {
        return 2;
    }
    else if(_angulo < -M_PI/2.0 && _angulo > -M_PI)//Terceiro
    {
        return 3;
    }
    else if(_angulo < 0 && _angulo > -M_PI/2.0)//Quarto
    {
        return 4;
    }

    return 0;
}

int clsAuxiliar::iCalculaQuadrante360(double _angulo)
{
    if(_angulo >= 0 && _angulo < M_PI/2.0)//Primeiro
    {
        return 1;
    }
    else if(_angulo > M_PI/2.0 && _angulo < M_PI)//Segundo
    {
        return 2;
    }
    else if(_angulo < 3.0*M_PI/4.0 && _angulo > M_PI)//Terceiro
    {
        return 3;
    }
    else if(_angulo < 2*M_PI && _angulo > 3.0*M_PI/4.0)//Quarto
    {
        return 4;
    }

    return 0;
}

bool clsAuxiliar::bPontoContidoRetangulo(QVector2D ponto, QVector2D R1, QVector2D R2, QVector2D R3)
{
    QVector2D vt2dP;
    float fP_PaPb = 0, fPaPb_PaPb = 0, fP_PcPb = 0, fPcPb_PcPb =0;

    vt2dP = ponto - R2;

    fP_PaPb = vt2dP.dotProduct(vt2dP, (R1-R2));
    fPaPb_PaPb = vt2dP.dotProduct((R1-R2), (R1-R2));
    fP_PcPb = vt2dP.dotProduct(vt2dP, (R3-R2));
    fPcPb_PcPb = vt2dP.dotProduct((R3-R2), (R3-R2));

    if(fP_PaPb >= 0 && fP_PaPb <= fPaPb_PaPb && // Se 0 < P→ . (Pa-Pb)→ < |Pa-Pb|² E 0 < P→ . (Pc-Pb)→ < |Pc-Pb|² o ponto P3 está
       fP_PcPb >= 0 && fP_PcPb <= fPcPb_PcPb  ) // dentro do retângulo
        return true;
    return false;
}

float clsAuxiliar::fAnguloVetor1Vetor2(QVector2D vetor1, QVector2D vetor2)
{
    double ang = (180/M_PI) * qAcos(vetor1.dotProduct(vetor1, vetor2)/(vetor1.length()*vetor2.length()));
    if(qIsNaN(ang))
    {
        ang = 0;
    }
    return ang;
}

QVector2D clsAuxiliar::vt2dVetorPerpendicular(QVector2D V1, QVector2D V2)
{
    return (V1 - ((V1.dotProduct(V1, V2)/V2.lengthSquared()) * V2));// W→ = V1→ - ( ( (V1→ . V2→)/ |V2→|²) * V2→)
}

QVector2D clsAuxiliar::vt2dReajustaPonto(QVector2D _vt2dPonto1, QVector2D _vt2dPonto2, float fDistanciaPonto1)
{
    QVector2D vt2dP, vt2dU, vt2dPonto3;

    vt2dP = _vt2dPonto2 - _vt2dPonto1;
    vt2dP.normalize();

    vt2dU = vt2dP * fDistanciaPonto1;

    vt2dPonto3 = vt2dU + _vt2dPonto1;

    return vt2dPonto3;
}

QVector2D clsAuxiliar::vt2dRotaciona(QVector2D _vt2dP1, QVector2D _vt2dP2, double _dAngulo_graus)
{
    QVector2D vt2dAux = _vt2dP2 - _vt2dP1;
    QVector2D vt2dPontoRot;

    _dAngulo_graus = qDegreesToRadians(_dAngulo_graus);

    vt2dPontoRot.setX(vt2dAux.x()*static_cast<float>(qCos(_dAngulo_graus)) - vt2dAux.y()*static_cast<float>(qSin(_dAngulo_graus)) );
    vt2dPontoRot.setY(vt2dAux.x()*static_cast<float>(qSin(_dAngulo_graus)) + vt2dAux.y()*static_cast<float>(qCos(_dAngulo_graus)) );

    vt2dPontoRot = vt2dPontoRot + _vt2dP1;

    return vt2dPontoRot;

}
