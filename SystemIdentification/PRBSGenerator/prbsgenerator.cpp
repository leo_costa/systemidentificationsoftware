#include "prbsgenerator.h"

PRBSGenerator::PRBSGenerator()
{
    buffer = 0;
}

PRBSGenerator::~PRBSGenerator()
{

}


void PRBSGenerator::startPRBS()
{
    buffer = static_cast<quint8>(QRandomGenerator::global()->generate());// 0x01; //Load the buffer with any non-zero value
    count = 0;
    initialValue = buffer;
}

double PRBSGenerator::dGetValue(double Amplitude)
{
    int outValue = 0;
    double out = 0;
    bool aux = ((buffer>>7)&0x01) ^ ((buffer>>3)&0x01) ^ ((buffer>>2)&0x01) ^ ((buffer>>1)&0x01);//XOR of the first two bits
    outValue = buffer>>7;//Gets the value of the first bit
    buffer = buffer << 1;//Shift the register 1 bit to the left

    //insert 1 or 1 into the last bit
    if(aux)
        buffer = buffer | 0x01;
    else
        buffer = buffer & 0xFE;

    count++;

    out = outValue*Amplitude;

    if(buffer == initialValue || count > 255 )
        out = -950;

    return out;
}

int PRBSGenerator::getCount() const
{
    return count;
}

