#ifndef PRBSGENERATOR_H
#define PRBSGENERATOR_H

#include <qmath.h>
#include <QDebug>
#include <QObject>
#include <QRandomGenerator>

class PRBSGenerator : public QObject
{
    Q_OBJECT

    quint8 buffer;
    quint8 initialValue;
    int count;
public:
    PRBSGenerator();
    ~PRBSGenerator();

    void startPRBS();
    double dGetValue(double Amplitude);
    int getCount() const;
};


#endif // PRBSGENERATOR_H
