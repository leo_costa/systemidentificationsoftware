#ifndef RADIOBASE_H
#define RADIOBASE_H

#include "radiobase.h"
#include <QDataStream>
#include <QDir>
#include <QMutex>
#include <QTextStream>
#include <QThread>
#include <QVector>
#include <QtCore/QtGlobal>
#include <QtSerialPort/QSerialPort>
#include <iostream>


#include "Ambiente/atributos.h"


typedef struct//Estrutura com os parâmetros da porta serial
{
    bool bConectar; //Indica se o radio deve conectar-se ou desconectar-se
    QString Porta;  //Porta em que o radio ira conectar-se
    QSerialPort::DataBits dataBits;
    QSerialPort::FlowControl flowControl;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    int baudRate; //Baud rate da conexao
    bool localEchoEnabled;
}ConfiguracaoRadio;

enum ErrorCode{
            CHECKSUM_ERROR			= 0x0A,
            DELIMITER_NOT_FOUND		= 0x0B,
            NOPCKT_ERROR			= 0x0C,
            PACKET_INCOMPLETE		= 0x0D,
            OTHER_RADIO_PCKT        = 0x0E,
            SAFETY_THRESHOLD_ERROR  = 0x0F,
        };

Q_DECLARE_METATYPE(ConfiguracaoRadio) //Declaracao dos parametros do radio para que seja possivel envia-los via sinal
Q_DECLARE_METATYPE(ErrorCode)

class RadioBase : public QObject
{
    Q_OBJECT

private:
    bool openPort();//Abre a porta serial

    ErrorCode DelimiterFinder(QByteArray & packet, int bytestart , int &delimiter_pos);//

    static const unsigned char INIT_DELIMITER = 0x7E;//
    static const int PREAMBLE_SIZE=6;//
    ConfiguracaoRadio *config;//

public:
    //Métodos utilizados no envio de um pacote
    bool sendData(Atributos &robot);//Faz o processo de codificação das informacoes e envia
    bool sendData(const QByteArray &pacote);//Envia um pacote ja codificado
    QByteArray encodePacket(QByteArray &data);//
    QByteArray RadioPacketize(Atributos &robot);//

    //Métodos utilizados no recebimento de um pacote
    ErrorCode readData(QByteArray &retPack);//
    ErrorCode decodePacket(QByteArray &encodedPacket);//

    //Métodos utilizados na configuração da porta serial
    void portConnections();
    void closePort();//
    void setConfig(const ConfiguracaoRadio pconfig);//

    QSerialPort *serial;//
    QTime *time;//

    //Variáveis utilizadas na decodificação dos pacotes recebidos
    QVector<double> measured_1[PLAYERS_PER_SIDE], measured_2[PLAYERS_PER_SIDE], measured_3[PLAYERS_PER_SIDE], measured_4[PLAYERS_PER_SIDE];//
    QVector<double> timeStamp;//

    float battery_level[PLAYERS_PER_SIDE];//
    unsigned char kick_sensor[PLAYERS_PER_SIDE];//
    int rx_counter[PLAYERS_PER_SIDE][2];
    int rx_lost[PLAYERS_PER_SIDE];
    int sensor_type;//
    int rcv_safetyerr_counter,buffer_maxed_counter;//


    RadioBase();//Construtor da classe
    ~RadioBase() override;

public slots:
    bool configureSerialPort();//Configura a porta serial
    void handleError(QSerialPort::SerialPortError error);//



};
#endif // RADIOBASE_H
