#include "radiobase.h"

#include <QDebug>
#include <QTime>

RadioBase::RadioBase()
{
  serial = new QSerialPort();
  config = new ConfiguracaoRadio;
  sensor_type=0;
  rcv_safetyerr_counter=0;
  buffer_maxed_counter=0;

  time = new QTime(QTime::currentTime());
  time->start();

  portConnections();
}

RadioBase::~RadioBase()
{
    if(serial->isOpen())
        serial->close();

    delete serial;
    delete config;
    delete time;
}

bool RadioBase::configureSerialPort()
{
    serial->setPortName(config->Porta);
    serial->setBaudRate(config->baudRate);
    serial->setDataBits(config->dataBits);
    serial->setParity(config->parity);
    serial->setStopBits(config->stopBits);
    serial->setFlowControl(config->flowControl);

   return openPort();
}

bool RadioBase::openPort()
{
    return serial->open(QIODevice::ReadWrite);
}

void RadioBase::closePort()
{
    if (serial->isOpen())
    {
        serial->close();
    }
}

ErrorCode RadioBase::readData(QByteArray &retPack)
{

    //while (serial->waitForReadyRead(10))
    //{
        retPack.append(serial->readAll());

//        qDebug() << retPack.size();
   // }

    return (this->decodePacket(retPack));
}

ErrorCode RadioBase::decodePacket(QByteArray &encodedPacket)
{
    const int byte_start = 6;
    int delim_pos =-1;
    int packet_size;
    int robotid;
    //char checksum = 0; O robo nao usa o checksum
    float a1,a2;//Variáveis utilizadas na conversão dos valores recebidos da odometria e sensor de corrente
    ErrorCode error_code = ErrorCode::NOPCKT_ERROR;

    const int INTERM_MAX_SIZE = 256;
    //const int SAFETY_THRESHOLD = 50;

    int safety_counter = 0;

    if (encodedPacket.size() > INTERM_MAX_SIZE )
    { //if buffer became too big, delete older packets - temporary - to be re-evaluated
        encodedPacket.remove(0,encodedPacket.size() - INTERM_MAX_SIZE); //remove older packets .... temporary
        buffer_maxed_counter++;
    }

    for( int i=0; i<encodedPacket.size()/32;++i){
        QByteArray new_encodedPacket = encodedPacket.mid(i*32,32);

        //while (encodedPacket.size() > 4)
        //{ //there's enough data to mean something
        safety_counter++; // The safety check is a measure to prevent the routine from getting stuck - it should never occur under normal conditions

        //desnecessário pois nunca chegará no 50
        //if (safety_counter > SAFETY_THRESHOLD)
        //{ //if safety counter reaches the threshold
        //    encodedPacket.clear();
        //    rcv_safetyerr_counter++;
        //    return SAFETY_THRESHOLD_ERROR;
        //}

        error_code = DelimiterFinder(new_encodedPacket, 0, delim_pos);//Procura o delimitador de inicio a partir do primeiro byte do pacote

        if(error_code != ErrorCode::NOPCKT_ERROR)//Checando se houve algum erro ao procurar o delimitador
        {
            return error_code;
        }

        if (delim_pos > 0) new_encodedPacket.remove(0,delim_pos); //discards garbage before the delimiter

        // packet_size is the size given by Length bytes + 1 byte delimiter + 2 bytes lenght + 1 byte checksum = Length + 4
        packet_size = (int)(new_encodedPacket[1]*256 + new_encodedPacket[2]) + 4;

        if (new_encodedPacket.size() < delim_pos + PREAMBLE_SIZE)//Pacote incompleto
        {
            error_code = ErrorCode::PACKET_INCOMPLETE;
            return error_code;
        }
        else if(new_encodedPacket.size() < (packet_size)) //the packet isn't complete
            return ErrorCode::PACKET_INCOMPLETE;


        if (new_encodedPacket.at(3) != static_cast<char>(0x90) )
        { // 0x90 indicates a packet sent from other radio
            encodedPacket.clear(); //erase data, unhandled type of packet
            return ErrorCode::OTHER_RADIO_PCKT;
        }
        //Aparentemente o checksum nao esta funcionando, por isso esta parte está comentada

        //        for(int n = delim_pos + 3; n < packet_size-1; n++)//calcula o valor do checksum para o pacote recebido
        //        {                                               //'delim_pos+3' é a posição do primeiro valor significativo do pacote (0x90)
        //            checksum += encodedPacket[n];
        //        }
        //        checksum = ~checksum;

        //        if(checksum != encodedPacket[packet_size-1])//'packet_size-1' é a posição onde está o valor do checksum
        //        {
        //            error_code = ErrorCode::CHECKSUM_ERROR;
        //            return error_code;
        //        }

        if (new_encodedPacket.at(byte_start) == static_cast<char>(0x62)
                && new_encodedPacket.at(packet_size-2) == static_cast<char>(0x65))//0x62 é o começo do pacote com os dados do robô e 0x65 é o final
        {
            robotid = new_encodedPacket[byte_start+2];
            if (robotid < PLAYERS_PER_SIDE && robotid >= 0)
            {

                //calculation of the battery level, according to the hardware of the main board
                battery_level[robotid] = (float)new_encodedPacket[byte_start+3] * 5.0f/4096.f * 11.f * 10.f;

                if((int)new_encodedPacket[byte_start+1] == 0x01)
                    kick_sensor[robotid] = true;//(int)encodedPacket[byte_start+1];
                else if((int)new_encodedPacket[byte_start+1] == 0x00)
                    kick_sensor[robotid] = false;

                //if (robotid == monitored_robotid)
                //{
                if(new_encodedPacket.at(byte_start+4) == static_cast<char>(sensor_type))
                { // type of sensor data received. 0=odometry, 1=current
                    if (sensor_type ==0)
                    {
                        a1=0;
                        a2=1.0f; //offset and scale multiplier for odometry - basically there's no conversion
                    }
                    else
                    {
                        a1=-37.5f;
                        a2=0.01831f;
                    }//offset and scale multiplier for the current sensor - values according to IC datasheet
                    //according to the datasheet the current is i = -37.5 + 0.01831*ADCRead

                    if (packet_size > byte_start + 20)
                    { //if packet is big enough to have full sensor data

                        //Atribuição do valor da odometria de cada motor
                        measured_1[robotid].append(a1 + a2 * (float)new_encodedPacket[byte_start+5]);
                        measured_2[robotid].append(a1 + a2 * (float)new_encodedPacket[byte_start+9]);
                        measured_3[robotid].append(a1 + a2 * (float)new_encodedPacket[byte_start+13]);
                        measured_4[robotid].append(a1 + a2 * (float)new_encodedPacket[byte_start+17]);
                        timeStamp.append(time->elapsed());


                        //                            if(measured_1.size() > 250 && measured_2.size() > 250 && measured_3.size()> 250 && measured_4.size()>250)
                        //                            {
                        //                                measured_1.removeFirst();
                        //                                measured_2.removeFirst();
                        //                                measured_3.removeFirst();
                        //                                measured_4.removeFirst();
                        //                                timeStamp.removeFirst();
                        //                            }

                    }
                }
                //}

                //checks if there are lost packets on the transmission
                rx_counter[robotid][0] = static_cast<int>(new_encodedPacket.at(packet_size-3));
                if ( (rx_counter[robotid][0] - rx_counter[robotid][1] > 1) && !(rx_counter[robotid][0] ==0 && rx_counter[robotid][1]==255) )
                {
                    int tmp1 = rx_counter[robotid][0] - rx_counter[robotid][1];
                    if (tmp1 < 0)
                        tmp1 +=255;
                    rx_lost[robotid] += tmp1;
                }
                rx_counter[robotid][1] = rx_counter[robotid][0];
            }
        }
    }
    encodedPacket.clear(); //delete the packet. Always needed after a packet has been processed
    return error_code;
}

ErrorCode RadioBase::DelimiterFinder(QByteArray & packet, int bytestart , int &delimiter_pos)
{

    for(int n = bytestart; n< packet.size(); n++){
        if(packet.at(n) == INIT_DELIMITER){
            delimiter_pos = n;
            return ErrorCode::NOPCKT_ERROR;
        }
    }

    return ErrorCode::DELIMITER_NOT_FOUND;

}

bool RadioBase::sendData(Atributos &robot/*QByteArray &encodedPacket*/)
{
    //this->mutex_radio.lock();

    QByteArray encodedPacket = this->RadioPacketize(robot);
    if (!serial->isOpen())return false;

    QDataStream stream(serial);
    stream << encodedPacket;
    qDebug() << serial->errorString();
    serial->clearError();

    //this->mutex_radio.unlock();

    return true;
}

bool RadioBase::sendData(const QByteArray &pacote)
{
    if (!serial->isOpen())return false;

    QDataStream stream(serial);
    stream << pacote;

    return true;
}

void RadioBase::handleError(QSerialPort::SerialPortError error)
{
    if (error == QSerialPort::ResourceError) closePort();
}

void RadioBase::portConnections()
{
    connect(serial, static_cast<void (QSerialPort::*)(QSerialPort::SerialPortError)>(&QSerialPort::error),
            this, &RadioBase::handleError);

}

void RadioBase::setConfig(const ConfiguracaoRadio pconfig)
{
//  memcpy(this->config, pconfig, sizeof(ConfiguracaoRadio));
  *config = pconfig;
}

QByteArray RadioBase::encodePacket(QByteArray &packet)
{

    unsigned char checksum=0;
    int packetsize = PREAMBLE_SIZE + packet.size() + 1;
    int packetlength = packetsize - 4;
    QByteArray encodedPacket;
    encodedPacket.resize(packetsize);

    //Preamble - Nordic RoboFei protocol v1.0
    encodedPacket[0] = INIT_DELIMITER; //delimiter
    encodedPacket[1] = (unsigned char)packetlength/256;;//Length MSB
    encodedPacket[2] = (unsigned char)packetlength;//Length LSB
    encodedPacket[3] = 0x10; //TX_request
    encodedPacket[4] = 0x0;//Frame ID (byte 5)
    encodedPacket[5]= packet[1]; //ID do robô destino

    for (int n=0; n<(int)packet.size();n++)//counts packet size
    {
        encodedPacket[PREAMBLE_SIZE + n] = packet[n];
    }

    //checksum calculation
    for (int n=3; n<encodedPacket.length();n++)
    {
        checksum += encodedPacket[n];
    }

    checksum = ~checksum;
    encodedPacket[packetsize -1] = checksum;


    return encodedPacket;
}

QByteArray RadioBase::RadioPacketize(Atributos &robot)
{

    QByteArray data_pack;
    data_pack.resize(16);

    //Insert the begin and end bytes into the packet. A simple safety measure against interference
    data_pack[0]=0x62;
    data_pack[15]=0x65;
    //packet counter
    data_pack[7]=robot.radiotx_counter++;

//    if (robot.radiotx_counter %2)
    //Mandar 0x80 faz com que o robo retorne pacotes
    data_pack[8]=0x80;//sync_radio
//    else
//        data_pack[8]=0x00;//sync_radio

    if(robot.roller == true)
    data_pack[10]=(unsigned char)robot.cmd_rollerspd; //roller_spd

    data_pack[1]=(unsigned char)robot.id;//robot id
    data_pack[5]=(unsigned char)robot.cmd_avgspd;

    if (robot.hw_version == v2013)
    {
        data_pack[2]=(unsigned char)qCeil(robot.cmd_vx);
        data_pack[3]=(unsigned char)qCeil(robot.cmd_vy);
        data_pack[4]=(unsigned char)qCeil(robot.cmd_w);
    }
    else
    {
        data_pack[2]=(unsigned char)robot.cmd_r;
        data_pack[3]=(unsigned char)robot.cmd_rho;
        data_pack[4]=(unsigned char)robot.cmd_theta;
    }

    //roller//PACOTE NOVO
    if (robot.roller) data_pack[6]=0x40;
    else data_pack[6] = 0x00;


    //kick
    if(robot.kick == KICK_NONE)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }
        data_pack[6]=temp;
    }

    else if (robot.kick == KICK_STRONG)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x04);
    }

    else if (robot.kick == KICK_CUSTOM)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x02);
    }

    else if (robot.kick == KICK_SOFT)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x01);
    }

    //chip kick
    else if (robot.kick == CHIP_KICK_STRONG)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x20);
    }

    else if (robot.kick == CHIP_KICK_CUSTOM)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x10);
    }

    else if (robot.kick == CHIP_KICK_SOFT)
    {
        u_int8_t temp = 0x00;
        if(robot.roller)
        {
            temp = temp | 0x40;
        }

        data_pack[6] = (temp | 0x08);
    }
    //PACOTE NOVO


//    //PACOTE VELHO

//    if(robot.kick == KICK_STRONG)
//        data_pack[6] =0x03;

//    else if(robot.kick == KICK_CUSTOM)
//        data_pack[6] =0x02;

//    else if(robot.kick == KICK_SOFT)
//        data_pack[6] =0x01;

//    else if(robot.kick == CHIP_KICK_STRONG)
//        data_pack[6] =0x0C;

//    else if(robot.kick == CHIP_KICK_CUSTOM)
//        data_pack[6] =0x08;

//    else if(robot.kick == CHIP_KICK_SOFT)
//        data_pack[6] =0x04;

//    if(robot.roller)
//        data_pack[6]= 0x30;

//    //PACOTE VELHO



    //directional kick
    else if(robot.kick >= ANGLE_KICK_SOFT)
    {
        char temp=0;
        //aqui o kickStrength terá a regiao do angulo a ser chutado e seu respectivo sinal
        temp = robot.kickStrength;

        temp = temp | robot.kick; /*mascara para definir o tipo do chute, utilizada para introduzir os
                                           valores nos bits 4 e 3 da variavel temp*/
        data_pack[6]=temp;

    }


    data_pack[9]=(unsigned char)robot.kickStrength;

//    QByteArray temp_byte_1;
//    temp_byte_1.clear();
//    temp_byte_1.resize(4);

//    temp_byte_1.setNum(robot.cmd_rho);
    //Enviar 0 pois esta forma de enviar as velocidades nao e mais utilizada
    data_pack[11] = '0';
    data_pack[12] = '0';
    data_pack[13] = '0';
    data_pack[14] = '0';

    data_pack = encodePacket(data_pack);


    return data_pack;

}

