#include "robot.h"

Robo::Robo(int _robotID )
{
    atributos.id = _robotID;
    atributos.bEmCampo  = false;
    atributos.bIgnorarBola = bIgnoraBola;
    atributos.jogadaAtual = Nenhuma ;//Nenhuma jogada

    atributos.dTempoAceleracao = dTempoAceleracaoPadrao;
    atributos.dTempoDesaceleracao = dTempoAceleracaoPadrao;
    atributos.dTempoVelocidadeConstante = 0;

    atributos.dTempoRobo  = 0;
    atributos.dVelocidade = 0;

    atributos.kick = KICK_NONE;
    atributos.kickSensor = false;
    atributos.kickStrength = iForcaChuteCalibrado;
    atributos.roller = false;
    atributos.battery = 0;
    atributos.hw_version = v2013;
    atributos.cmd_avgspd = 0;
    atributos.cmd_vx = 0;
    atributos.cmd_vy = 0;
    atributos.cmd_w = 0;

    atributos.cmd_rollerspd = 0;

    atributos.vt2dPosicaoAtual = QVector2D(99999999, 99999999);
    atributos.vt2dPosicaoAnterior = QVector2D(99999999, 99999999);
    atributos.vt2dPontoAnguloDestino = QVector2D(0,0);
    atributos.vt2dVelocidadeNaoConvertida = QVector2D(0,0);
    atributos.fAnguloDestino = 0;
    atributos.vt2dVelocidade = QVector2D(0,0);
    atributos.rotation = 0;
    atributos.dVelocidadeAngular = 0;

    atributos.iCameraRobo = 0;

    for(unsigned int & i : atributos.iCameraFrame)
        i = 0;

    atributos.minhaRole = NULO;
}

Robo::~Robo()
= default;


void Robo::vSetaRobo(Atributos &_atributos)
{
    this->atributos = _atributos;
}

void Robo::vAtualizaRobo(Atributos &_atributos)
{
    this->atributos.vt2dPosicaoAtual    = _atributos.vt2dPosicaoAtual;
    this->atributos.vt2dPosicaoAnterior = _atributos.vt2dPosicaoAnterior;
    this->atributos.rotation            = _atributos.rotation;
    this->atributos.dVelocidadeAngular  = _atributos.dVelocidadeAngular;
    this->atributos.bEmCampo            = _atributos.bEmCampo;
    this->atributos.battery             = _atributos.battery;
    this->atributos.kickSensor          = _atributos.kickSensor;
    this->atributos.iCameraRobo         = _atributos.iCameraRobo;
    this->atributos.dVelocidade         = _atributos.dVelocidade;
    this->atributos.minhaRole           = _atributos.minhaRole;
    this->atributos.vt2dVelocidade      = _atributos.vt2dVelocidade;
    //this->atributos.vt2dVelocidadeNaoConvertida = _atributos.vt2dVelocidadeNaoConvertida;

    for(unsigned int n = 0; n < iNumeroTotalCameras; ++n)
        this->atributos.iCameraFrame[n] = _atributos.iCameraFrame[n];
}

void Robo::vAtualizaRoboEstrategia(Atributos &_atributos)
{
    this->atributos.vt2dDestino = _atributos.vt2dDestino;
    this->atributos.vt2dPontoAnguloDestino = _atributos.vt2dPontoAnguloDestino;
    this->atributos.bIgnorarBola = _atributos.bIgnorarBola;
    this->atributos.jogadaAtual = _atributos.jogadaAtual;
    this->atributos.minhaRole = _atributos.minhaRole;
    this->atributos.roller = _atributos.roller;
    this->atributos.kickStrength = _atributos.kickStrength;
    this->atributos.kick = _atributos.kick;
    this->atributos.cmd_avgspd = _atributos.cmd_avgspd;
    this->atributos.cmd_rollerspd = _atributos.cmd_rollerspd;
    this->atributos.kickSensor = _atributos.kickSensor;
}

Atributos Robo::atbRetornaRobo() const
{
    Atributos tempAtb = atributos;
    return tempAtb;
}

void Robo::vSetaVelocidade(float Vx, float Vy, float Vw, int vMax)
{
    atributos.cmd_vx = Vx;
    atributos.cmd_vy = Vy;
    atributos.cmd_w  = Vw;
    atributos.cmd_avgspd = vMax;
}

void Robo::vSetaJogada(ID_Jogadas _IDProcesso)
{
    if(atributos.bEmCampo == true)
        atributos.jogadaAtual = _IDProcesso;
}

void Robo::vSetaRole(quint8 _role)
{
    atributos.minhaRole = _role;
}

void Robo::vSetaDestino(QVector2D _destino)
{
    atributos.vt2dDestino = _destino;
}

void Robo::vSetaPosicao(QVector2D _vt2dPos)
{
    atributos.vt2dPosicaoAnterior = atributos.vt2dPosicaoAtual;
    atributos.vt2dPosicaoAtual = _vt2dPos;
}

void Robo::vSetaPontoAnguloDestino(QVector2D _pontoAngulo)
{
    atributos.vt2dPontoAnguloDestino = _pontoAngulo;
}

void Robo::vSetaIgnorarBola(bool _bIgnorarBola)
{
    atributos.bIgnorarBola = _bIgnorarBola;
}

void Robo::vSetaVelocidadeMaxima(int _iVmax)
{
    atributos.cmd_avgspd = _iVmax;
}

void Robo::vSetaAnguloDestino(float _angulo)
{
    atributos.fAnguloDestino = _angulo;
}

bool Robo::bPodeChutar(QVector2D _vt2dMira)
{
    float fAnguloRobo = atributos.rotation;

    QVector2D vetorRobo = QVector2D(qCos(fAnguloRobo), qSin(fAnguloRobo));

    float fProdEsc = vetorRobo.dotProduct(vetorRobo, _vt2dMira);
    float fAnguloBola = qAcos(fProdEsc/(vetorRobo.length()*_vt2dMira.length()));


    if( qAbs(fAnguloBola*(180/M_PI)) < 5)//Só atribue o chute quando o robô está no ângulo certo
        return true;

    qDebug() << "O robo " << this->atributos.id << "nao pode chutar ainda. Angulo de Erro = " << qAbs(fAnguloBola*(180/M_PI));
    return false;
}

void Robo::vLigaRoller(int _velRoller)
{
    atributos.roller = true;
    atributos.cmd_rollerspd = _velRoller;
}

void Robo::vDesligaRoller()
{
    atributos.roller = false;
    atributos.cmd_rollerspd = 0;

}

void Robo::vChutar(int _kick, int _forca, int _angulo, int _anguloMax)
{
    atributos.kick = _kick;


    if(_kick < ANGLE_KICK_SOFT)//Se nao for chute direcional
        atributos.kickStrength = _forca;

    else
    {
        int kickAngle = abs(_angulo);

        //ajusta o valor do angulo no slider para alguma das regioes, neste caso esta sendo considerado 4 regioes

        if(kickAngle >= 0 && kickAngle < _anguloMax/3)kickAngle = 0; //primeira regiao
        else if(kickAngle >= _anguloMax/3 && kickAngle < _anguloMax*2/3)kickAngle = 1;//segunda regiao
        else if(kickAngle >= _anguloMax*2/3 && kickAngle < _anguloMax)kickAngle = 2;//terceira regiao
        else kickAngle = 3;//quarta regiao

        if(_angulo < 0)kickAngle = kickAngle | 0x04;/*mascara para definir se o angulo é positivo ou negativo*/

        atributos.kickStrength = kickAngle;//O angulo do chute é passado no campo da força de chute
    }
}


void Robo::vAndarXY(float _VelX, float _VelY, float _VelRot, int _VelMedia)//Função utilizada para movimentar o robô de modo que ela própria já envia o pacote
{
    atributos.cmd_vx     = _VelX;
    atributos.cmd_vy     = _VelY;
    atributos.cmd_w      = _VelRot;
    atributos.cmd_avgspd = _VelMedia;

    atributos.hw_version = v2013;
}

void Robo::vAndarPolar(int _R, int _Rho, int _Theta, int _VelMedia)
{
    atributos.cmd_r      = _R;
    atributos.cmd_rho    = _Rho;
    atributos.cmd_theta  = _Theta;
    atributos.cmd_avgspd = _VelMedia;
}

bool Robo::vEstouComBola()
{
    return atributos.kickSensor;
}

float Robo::vMinhaBateria()
{
    return atributos.battery;
}

void Robo::vResetFrameNumber()
{
    for(unsigned int & i : atributos.iCameraFrame)
        i = 0;
}

QVector2D Robo::vt2dDestinoRobo()
{
    return atributos.vt2dDestino;
}

QVector2D Robo::vt2dPosicaoAtualRobo()
{
    return atributos.vt2dPosicaoAtual;
}

QVector2D Robo::vt2dPontoAnguloDestino()
{
    return atributos.vt2dPontoAnguloDestino;
}

QVector3D Robo::vt3dPegaVelocidadesRobo()
{
    return {atributos.cmd_vx, atributos.cmd_vy, atributos.cmd_w};
}

QVector3D Robo::vt3dPegaVelocidadeRobo()
{
    //Trocar cmd_w para a velocidade de rotação do robo
    return QVector3D(atributos.vt2dVelocidade, atributos.dVelocidadeAngular);
}

ID_Jogadas Robo::jJogadaAtual()
{
    return atributos.jogadaAtual;
}

bool Robo::bPegaIgnorarBola()
{
    return atributos.bIgnorarBola;
}

quint8 Robo::ucMinhaRole()
{
    return atributos.minhaRole;
}

float Robo::fAnguloDestino()
{
    return atributos.fAnguloDestino;
}
