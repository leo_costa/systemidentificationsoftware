﻿#ifndef FUTBOLENVIRONMENT_H
#define FUTBOLENVIRONMENT_H

#include "protofiles/referee.pb.h"
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Radio/radiobase.h"
#include "Ambiente/atributos.h"
#include "Ambiente/robot.h"
#include <QVector2D>

typedef struct RGB
{
    int R,G,B;
}RGB;
//estrutura que contém todos os componentes do ambiente (jogo) (robôs, robôs adversários, bola, dimensões do campo, estado do jogo e quem está com a bola)

class AmbienteCampo
{
private:
    Bola *blBola;
    QVector2D vt2dTamanhoCampo;//Tamanho absoluto do campo

    int iProfundidadeAreaPenalty;
    int iLarguraAreaPenalty;
    int iLarguraGol;
    int iProfundidadeGol;
    int iRaioCirculoCentroCampo;//Raio do circulo do centro de campo
    QVector2D vt2dCentroPositivo;
    QVector2D vt2dCentroNegativo;

    int iRobosEmCampo; //Indica quantos robos estao sendo vistos no campo

public:
    Robo *rbtRoboAliado[PLAYERS_PER_SIDE];
    Robo *rbtRoboAdversario[PLAYERS_PER_SIDE];


    //-------------------  FILTRO  -----------------------
    QVector<QVector2D> vtBufferPosBolaVisao;// Posição da bola vinda da visão (Sem filtro)
    QVector<QVector2D> vtBufferPosRobosAliadosVisao[PLAYERS_PER_SIDE];
    QVector<QVector2D> vtBufferPosRobosAdversariosVisao[PLAYERS_PER_SIDE];

    QVector<QVector2D> vtBufferBolaKALMAN;///< :Posicao filtrada da bola
    QVector<QVector2D> vtBufferRoboAliadosKALMAN[PLAYERS_PER_SIDE];///< :Posicao filtrada dos aliados
    QVector<QVector2D> vtBufferRoboAdversariosKALMAN[PLAYERS_PER_SIDE];///< :Posicao filtrada dos oponentes
    //----------------------------------------------------

    Time CorTime; ///< :0 = Azul; 1 = Amarelo



public:
    AmbienteCampo();

    AmbienteCampo(const AmbienteCampo &_acAmbiente);

    //AmbienteCampo &  operator= ( const AmbienteCampo &_acAmbiente);

    ~AmbienteCampo();

    void vSetaBola(Bola *_bola );

    void vSetaPosicaoBola(QVector2D _vt2dPos);

    void vSetaVelocidadeBola(double dVelocidade);

    QVector2D vt2dPosicaoAtualBola();

    QVector2D vt2dPosicaoAnteriorBola();

    void vResetFrameBola();

    void vSetaTamanhoCampo(QVector2D _campo);

    void vSetaProfundidadeAreaPenalty(int _iProfundidadeArea);

    void vSetaLarguraAreaPenalty(int _iLarguraArea);

    void vSetaLarguraGol(int _larguraGol);

    void vSetaProfundidadeGol(int _iProfundidadeGol);

    void vSetaRaioCirculoCentroCampo(int _raio);

    void vPegaBola(Bola &_bola) const;

    void vSetaRobosEmCampo(int n);

    int iPegaRobosEmCampo() const;

    QVector2D vt2dPegaTamanhoCampo() const;

    QVector2D vt2dCentroGolAliado() const;

    int iPegaProfundidadeAreaPenalty() const;

    int iPegaLarguraAreaPenalty() const;

    int iPegaLarguraGol() const;

    int iPegaProfundidadeGol() const;

    int iPegaRaioCirculoCentroCampo() const;


//==========================================================================================================================================

};

Q_DECLARE_METATYPE(AmbienteCampo)

#endif // FUTBOLENVIRONMENT_H
