#ifndef ROBOT_H
#define ROBOT_H

#include "atributos.h"
#include <QDebug>


class Robo
{

private:

    Atributos atributos;

public:


    Robo(int _robotID = 0);
    ~Robo();

    void vSetaRobo(Atributos &_atributos);

    void vAtualizaRobo(Atributos &_atributos);

    void vAtualizaRoboEstrategia(Atributos &_atributos);

    Atributos atbRetornaRobo() const;

    void vSetaVelocidade(float Vx, float Vy, float Vw, int vMax);

    void vSetaRole(quint8 _role);

    void vSetaJogada(ID_Jogadas _IDProcesso);

    void vSetaDestino(QVector2D _destino);

    void vSetaPosicao(QVector2D _vt2dPos);

    void vSetaPontoAnguloDestino(QVector2D _pontoAngulo);

    void vSetaIgnorarBola(bool _bIgnorarBola);

    void vSetaVelocidadeMaxima(int _iVmax);

    void vLigaRoller(int _velRoller);

    void vDesligaRoller();

    void vChutar(int _kick, int _forca, int _angulo = 0, int _anguloMax = 0);

    void vAndarXY(float _VelX, float _VelY, float _VelRot, int _VelMedia);//Função utilizada para movimentar o robô de modo que ela própria já envia o pacote

    void vAndarPolar(int _R, int _Rho, int _Theta, int _VelMedia);

    bool vEstouComBola();

    float vMinhaBateria();

    void vResetFrameNumber();

    QVector2D vt2dDestinoRobo();

    QVector2D vt2dPosicaoAtualRobo();

    QVector2D vt2dPontoAnguloDestino();

    QVector3D vt3dPegaVelocidadesRobo(); ///< : Retorna a velocidade Vx, Vy e W sendo enviada para o robo

    QVector3D vt3dPegaVelocidadeRobo(); //Velocidade do robo no campo

    ID_Jogadas jJogadaAtual();

    bool bPegaIgnorarBola();

    quint8 ucMinhaRole();

    float fAnguloDestino();

    void vSetaAnguloDestino(float _angulo);

    bool bPodeChutar(QVector2D _vt2dMira);
};


Q_DECLARE_METATYPE(QVector<Robo>) //Declaracao dos parametros do radio para que seja possivel envia-los via sinal




#endif // ROBOT_H
