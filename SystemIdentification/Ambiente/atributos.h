#ifndef ATRIBUTOS_H
#define ATRIBUTOS_H

#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include <QElapsedTimer>
#include <QVector>
#include <QVector2D>
#include <QVector3D>


//#define KICK_STRONG 8
//#define KICK_MEDIUM 4
//#define KICK_SOFT 2

//#define CHIP_KICK_STRONG 5
//#define CHIP_KICK_MEDIUM 3
//#define CHIP_KICK_SOFT 1


enum KICKTYPE
{
    KICK_NONE,
    KICK_SOFT,
    KICK_CUSTOM,
    KICK_STRONG,
    CHIP_KICK_SOFT,
    CHIP_KICK_CUSTOM,
    CHIP_KICK_STRONG,

    //    CHIP_KICK_SOFT    = 0x04,//PACOTE NOVO
    //    CHIP_KICK_CUSTOM  = 0x08,//PACOTE NOVO
    //    CHIP_KICK_STRONG  = 0x0C,//PACOTE NOVO
    ANGLE_KICK_SOFT   =    8,   /*valores utilizados na mascara para construir o protocolo que identifica o chute*/
    ANGLE_KICK_STRONG =   16,
    ANGLE_KICK_CUSTOM =   24,
};

enum ROLES
{
    NULO       ,
    ATACANTE   ,
    DEFENSOR   ,
    GOLEIRO    ,
    RONALDINHO ,
};

enum TipoObjeto
{
    otOponente,      //0
    otAliado,        //1
    otBola,          //2
    otLinhaPenalty,   //3
    otVazio
};

enum ROBOTHARDWARE
{
    v2010b,
    v2013
};


//estrutura com a posição da bola
class Bola
{
public:

    Bola()
    {
        vt2dPosicaoAtual      = QVector2D(0,0);
        vt2dPosicaoAnterior   = QVector2D(0,0);

        dVelocidade = 0;
        dTempoBola  = 0;

        iCameraBola = 0;
        for(int i=0; i < iNumeroTotalCameras; ++i)
        {
            iCameraFrame[i] = 0;
        }

        bEmCampo = false;
    }

    void vSetaBola(Bola *_bola)
    {
        vt2dPosicaoAtual      = _bola->vt2dPosicaoAtual;
        vt2dPosicaoAnterior   = _bola->vt2dPosicaoAnterior;

        dVelocidade = _bola->dVelocidade;
        dTempoBola  = _bola->dTempoBola;

        iCameraBola = _bola->iCameraBola;
        for(int i=0; i < iNumeroTotalCameras; ++i)
        {
            iCameraFrame[i] = _bola->iCameraFrame[i];
        }

        bEmCampo = _bola->bEmCampo;
    }


    QVector2D vt2dPosicaoAtual, vt2dPosicaoAnterior;

    double dVelocidade;
    double dTempoBola;

    uint iCameraBola;//Indica o ID da última câmera em que a bola foi vista
    uint iCameraFrame[iNumeroTotalCameras];//Guarda o número do frame em que a bola foi visto

    bool bEmCampo;//Mostra se a bola está sendo detectado pela visão, definido como padrão em false
}; /*Bola;*/


//estrutura com o valores relativos aos robôs (vetor posição, ângulo e velocidade)
typedef struct
{

    int id;///< :The id and radio address of the robot
    int hw_version;

    QVector2D vt2dPosicaoAtual; ///< :Posicao atual do robo
    QVector2D vt2dPosicaoAnterior; ///< :Posicao anterior do robo
    QVector2D vt2dDestino;///< :Destino do robo
    QVector2D vt2dPontoAnguloDestino;///<Ponto em que o robo deve estar olhando ao chegar no destino
    float fAnguloDestino; ///< :Angulo em que o robo deve estar olhando ao chegar no destino
    float rotation; ///< :Angulo do robo em radianos
    float dVelocidadeAngular;

    QVector2D vt2dVelocidade;
    double dVelocidade;
    double dTempoAceleracao;
    double dTempoDesaceleracao;
    double dTempoVelocidadeConstante;
    double dTempoRobo;

    bool bEmCampo;///< :Mostra se o robô está sendo detectado pela visão, definido como padrão em false
    bool bIgnorarBola;///< :Indica se o robo deve ignorar a bola como obstaculo ou nao

    uint iCameraRobo; ///< :Indica o ID da última câmera em que o robô foi visto
    uint iCameraFrame[iNumeroTotalCameras]; ///< :Guarda o número do frame em que o robô foi visto

    bool kickSensor; ///< :flag that contains robot ball sensor
    float battery;
    QVector<double> dOdometriaRoda1, dOdometriaRoda2, dOdometriaRoda3, dOdometriaRoda4;

    ID_Jogadas jogadaAtual;
    quint8 minhaRole;
    QVector2D vt2dVelocidadeNaoConvertida;

    unsigned char radiotx_counter;

    // robot actuators
    float cmd_r, cmd_rho, cmd_theta, cmd_avgspd; ///< :commands to actuators of the robot in polar coordinates
    float cmd_vx, cmd_vy, cmd_w;///< :commands to actuators of the robot in cartesian coordinates
    int cmd_rollerspd;    ///< :command roller velocity

    int kick; ///< :strength of the kick. 2(weak), 4(medium), 8(strong)
    bool roller; ///< :activates the roller/dribler
    float kickStrength;///< :intensity/energy of the kick or chip

} Atributos;



//typedef QString CustomString;
//int id = qRegisterMetaType <CustomString>("CustomString");

#endif // ATRIBUTOS_H
