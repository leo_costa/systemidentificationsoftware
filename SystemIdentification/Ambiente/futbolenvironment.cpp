#include "futbolenvironment.h"
#include <QDebug>
#include <QTimer>

AmbienteCampo::AmbienteCampo()
{
    iRobosEmCampo = 0;

    for (int n=0; n< PLAYERS_PER_SIDE; n++)
    {
        rbtRoboAliado[n] = new Robo(n);
        rbtRoboAdversario[n] = new Robo(n);


        vtBufferPosRobosAliadosVisao[n].clear(); // pos dos robos antes do filtro
        vtBufferPosRobosAdversariosVisao[n].clear();

        vtBufferRoboAliadosKALMAN[n].clear();
        vtBufferRoboAdversariosKALMAN[n].clear();
    }

    vtBufferBolaKALMAN.clear();
    vtBufferPosBolaVisao.clear();
    blBola = new Bola;
    CorTime = timeAzul;

    vt2dTamanhoCampo = QVector2D(0,0);
    iRaioCirculoCentroCampo = 0;
    vt2dCentroNegativo = QVector2D(0,0);
    vt2dCentroPositivo = QVector2D(0,0);
}



AmbienteCampo::AmbienteCampo(const AmbienteCampo &_acAmbiente)
{

    this->iRobosEmCampo = _acAmbiente.iRobosEmCampo;

    Bola blBola_;
    Atributos atbRobo;
    QVector2D tamanhoCampo;

    mtTeste.lock();

    this->blBola = new Bola;
    _acAmbiente.vPegaBola(blBola_);
    this->vSetaBola(&blBola_);

    //Atualiza informacoes dos robos e da bola
    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        this->rbtRoboAliado[n] = new Robo;
        this->rbtRoboAdversario[n] = new Robo;

        atbRobo = _acAmbiente.rbtRoboAliado[n]->atbRetornaRobo();
        this->rbtRoboAliado[n]->vSetaRobo(atbRobo);
        atbRobo = _acAmbiente.rbtRoboAdversario[n]->atbRetornaRobo();
        this->rbtRoboAdversario[n]->vSetaRobo(atbRobo);

        //Kalman
        this->vtBufferPosRobosAliadosVisao[n].clear(); // Pre Kalman
        this->vtBufferPosRobosAdversariosVisao[n].clear();

        this->vtBufferPosRobosAliadosVisao[n].append(_acAmbiente.vtBufferPosRobosAliadosVisao[n]);
        this->vtBufferPosRobosAdversariosVisao[n].append(_acAmbiente.vtBufferPosRobosAdversariosVisao[n]);

        this->vtBufferRoboAdversariosKALMAN[n].clear();
        this->vtBufferRoboAdversariosKALMAN[n].append(_acAmbiente.vtBufferRoboAdversariosKALMAN[n]);

        this->vtBufferRoboAliadosKALMAN[n].clear();
        this->vtBufferRoboAliadosKALMAN[n].append(_acAmbiente.vtBufferRoboAliadosKALMAN[n]);
        //Kalman
    }



    //Filtro de Kalman
    this->vtBufferBolaKALMAN.clear();
    this->vtBufferBolaKALMAN.append(_acAmbiente.vtBufferBolaKALMAN);

    //Cor do time, tipo do ambiente (grSim/CampoReal) e lado defesa
    this->CorTime = _acAmbiente.CorTime;

    //Dimensoes campo
    tamanhoCampo = _acAmbiente.vt2dPegaTamanhoCampo();
    this->vSetaTamanhoCampo(tamanhoCampo);
    this->iRaioCirculoCentroCampo = _acAmbiente.iRaioCirculoCentroCampo;
    this->iLarguraGol = _acAmbiente.iLarguraGol;
    this->iProfundidadeGol = _acAmbiente.iProfundidadeGol;
    this->iLarguraAreaPenalty = _acAmbiente.iLarguraAreaPenalty;
    this->iProfundidadeAreaPenalty = _acAmbiente.iProfundidadeAreaPenalty;
    this->vt2dCentroNegativo = _acAmbiente.vt2dCentroNegativo;
    this->vt2dCentroPositivo = _acAmbiente.vt2dCentroPositivo;

    mtTeste.unlock();

}

AmbienteCampo::~AmbienteCampo()
{
    if( blBola!= nullptr)
    {
       delete blBola;
        blBola = nullptr;
     }

    for(int n=0 ; n < PLAYERS_PER_SIDE; n++)
    {
        if( rbtRoboAliado[n] != nullptr)
        {
            delete rbtRoboAliado[n];
            rbtRoboAliado[n] = nullptr;
        }
        if( rbtRoboAdversario[n] != nullptr)
        {
           delete rbtRoboAdversario[n];
           rbtRoboAdversario[n]=nullptr;
        }
    }
}



void AmbienteCampo::vSetaBola(Bola *_bola)
{
    this->blBola->vSetaBola(_bola);
}

void AmbienteCampo::vSetaPosicaoBola(QVector2D _vt2dPos)
{
    blBola->vt2dPosicaoAnterior = blBola->vt2dPosicaoAtual;
    blBola->vt2dPosicaoAtual = _vt2dPos;
}

void AmbienteCampo::vSetaVelocidadeBola(double dVelocidade)
{
    blBola->dVelocidade = dVelocidade;
}

QVector2D AmbienteCampo::vt2dPosicaoAtualBola()
{
    return blBola->vt2dPosicaoAtual;
}

QVector2D AmbienteCampo::vt2dPosicaoAnteriorBola()
{
    return blBola->vt2dPosicaoAnterior;
}


void AmbienteCampo::vResetFrameBola()
{
    for(unsigned int & i : blBola->iCameraFrame)
    {
        i = 0;
    }
}

void AmbienteCampo::vSetaTamanhoCampo(QVector2D _campo)
{
    vt2dTamanhoCampo = _campo;
}

void AmbienteCampo::vSetaProfundidadeAreaPenalty(int _iProfundidadeArea)
{
    iProfundidadeAreaPenalty = _iProfundidadeArea;
}

void AmbienteCampo::vSetaLarguraAreaPenalty(int _iLarguraArea)
{
    iLarguraAreaPenalty = _iLarguraArea;
}

void AmbienteCampo::vSetaLarguraGol(int _larguraGol)
{
    iLarguraGol = _larguraGol;
}

void AmbienteCampo::vSetaProfundidadeGol(int _iProfundidadeGol)
{
    iProfundidadeGol = _iProfundidadeGol;
}

void AmbienteCampo::vSetaRaioCirculoCentroCampo(int _raio)
{
    iRaioCirculoCentroCampo = _raio;
}

void AmbienteCampo::vSetaRobosEmCampo(int n)
{
    iRobosEmCampo = n;
}

int AmbienteCampo::iPegaRobosEmCampo() const
{
    return iRobosEmCampo;
}


void AmbienteCampo::vPegaBola(Bola &_bola) const
{
        _bola = *blBola;
}

QVector2D AmbienteCampo::vt2dPegaTamanhoCampo() const
{
    QVector2D tempCamp = vt2dTamanhoCampo;
    return tempCamp;
}


int AmbienteCampo::iPegaProfundidadeAreaPenalty() const
{
    return iProfundidadeAreaPenalty;
}



int AmbienteCampo::iPegaLarguraAreaPenalty() const
{
    return iLarguraAreaPenalty;
}



int AmbienteCampo::iPegaLarguraGol() const
{
    return iLarguraGol;
}



int AmbienteCampo::iPegaProfundidadeGol() const
{
    return iProfundidadeGol;
}



int AmbienteCampo::iPegaRaioCirculoCentroCampo() const
{
    int tempCircRad = iRaioCirculoCentroCampo;
    return tempCircRad;
}


#include "futbolenvironment.h"
