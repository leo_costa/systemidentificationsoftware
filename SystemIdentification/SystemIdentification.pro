#-------------------------------------------------
#
# Project created by QtCreator 2019-04-20T21:25:48
#
#-------------------------------------------------

QT       += core gui
QT += network
QT += serialport
QT += widgets
QT += gamepad
QT += multimedia
QT += charts

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET = SystemIdentification
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

PROTOS = protofiles/game_event.proto protofiles/messages_robocup_ssl_detection.proto protofiles/messages_robocup_ssl_geometry.proto \
protofiles/messages_robocup_ssl_refbox_log.proto protofiles/messages_robocup_ssl_wrapper.proto \
protofiles/rcon.proto protofiles/referee.proto protofiles/savestate.proto  protofiles/grSim_Replacement.proto protofiles/grSim_Commands.proto protofiles/grSim_Packet.proto \

include(proto_compile.pri)

CONFIG += c++11

SOURCES += \
        Ambiente/futbolenvironment.cpp \
        Ambiente/robot.cpp \
        Constantes_e_Funcoes_Auxiliares/auxiliar.cpp \
        Constantes_e_Funcoes_Auxiliares/constantes.cpp \
        GBNGenerator/gbngenerator.cpp \
        Map/drawmap.cpp \
        Network/geral.cpp \
        Network/robofeicontroller.cpp \
        Network/robofeinetwork.cpp \
        PRBSGenerator/prbsgenerator.cpp \
        QCustomPlot/qcustomplot.cpp \
        Radio/radiobase.cpp \
        Simulador/simulator.cpp \
        main.cpp \
        systemidentification.cpp

HEADERS += \
        Ambiente/atributos.h \
        Ambiente/futbolenvironment.h \
        Ambiente/robot.h \
        Constantes_e_Funcoes_Auxiliares/auxiliar.h \
        Constantes_e_Funcoes_Auxiliares/constantes.h \
        GBNGenerator/gbngenerator.h \
        Map/drawmap.h \
        Network/geral.h \
        Network/robofeicontroller.h \
        Network/robofeinetwork.h \
        PRBSGenerator/prbsgenerator.h \
        QCustomPlot/qcustomplot.h \
        Radio/radiobase.h \
        Simulador/simulator.h \
        systemidentification.h

FORMS += \
        systemidentification.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

LIBS += -lprotobuf

DISTFILES += \
    proto_compile.pri \
    protofiles/game_event.proto \
    protofiles/grSim_Commands.proto \
    protofiles/grSim_Packet.proto \
    protofiles/grSim_Replacement.proto \
    protofiles/messages_robocup_ssl_detection.proto \
    protofiles/messages_robocup_ssl_geometry.proto \
    protofiles/messages_robocup_ssl_geometry_legacy.proto \
    protofiles/messages_robocup_ssl_refbox_log.proto \
    protofiles/messages_robocup_ssl_wrapper.proto \
    protofiles/messages_robocup_ssl_wrapper_legacy.proto \
    protofiles/rcon.proto \
    protofiles/referee.proto \
    protofiles/savestate.proto

RESOURCES += \
    Figures/figures.qrc
