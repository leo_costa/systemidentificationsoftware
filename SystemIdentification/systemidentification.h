#ifndef SYSTEMIDENTIFICATION_H
#define SYSTEMIDENTIFICATION_H

#include <stdio.h>
#include <stdlib.h>

#include <QMainWindow>
#include <QDebug>
#include <QDesktopWidget>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QTimer>
#include <QMutex>
#include <QMessageBox>
#include <QMouseEvent>
#include <QTableView>
#include <QTableWidget>
#include <QFileDialog>
#include <QVector4D>
#include <QtConcurrent/QtConcurrent>

#include <QCustomPlot/qcustomplot.h>

#include "Network/robofeicontroller.h"
#include "Radio/radiobase.h"
#include "Ambiente/futbolenvironment.h"
#include "Map/drawmap.h"
#include "Simulador/simulator.h"

#include "PRBSGenerator/prbsgenerator.h"
#include "GBNGenerator/gbngenerator.h"

#include "Constantes_e_Funcoes_Auxiliares/auxiliar.h"

namespace Ui {
class systemidentification;
}

enum Output
{
    X,
    Y,
    W,
    None
};

class systemidentification : public QMainWindow
{
    Q_OBJECT

public:
    explicit systemidentification(QWidget *parent = nullptr);
    ~systemidentification();

private:
    Ui::systemidentification *ui;
    AmbienteCampo *futbolEnvironment;
    QTimer *tmrUpdateInterface;
    int counterInterfaceLED, ledIndex;
    QList<QString> strImages;
    QFuture<void> matlabAppThread;

    QString matlabRuntDir;
    QString matlabAppDir;

    QElapsedTimer etmFPS;
    QElapsedTimer etmTeste;
    QVector<int> iFPSCounter;
    QPixmap *pmFieldImage;
    drawMap *dmFieldPainter;

    RoboFei_Simulator *grsimSimulator;

    void vConfigureUI();
    void vConfigurePlots(QString _velocidade, QString _unidade, QCustomPlot *customPlot);
    void vUpdatePlots();
    void vResetPlots();
    void vSaveData();
    void vOpenFiles(QString fileName, QFile &file);
    void vSaveData(QString dados, QFile &file);
    void vFillSignalParameters(QString path);

private slots:
    void vUpdateUI();
    void vRobotColorChanged(int index);

//=============================================================== Network ===========================================
private:
    QUdpSocket *udpSSLVision; //Communication socket with ssl vision
    RoboFeiController RoboController;

    QUdpSocket *udpGrsim;

    void vUpdateField();
    void vDetectRobot(SSL_DetectionFrame frameDeteccao);
    void vUpdateBall(SSL_DetectionFrame frameDeteccao, int nBola);
    void vUpdateAlly(SSL_DetectionFrame frameDeteccao, Time timeAtual, int nRobo);
    void vUpdateOpponent(SSL_DetectionFrame frameDeteccao, Time timeAtual, int nRobo);
    void vUpdateGeometry(SSL_GeometryData frameGeometria, bool &tamanhoCampoMudou);
    QString strCheckMatlabRuntimeFolder();
    QString strCheckMatlabAppFolder();

private slots:
    void vConnectNetwork();
    void vDisconnectNetwork();
    void vProcessVision();

//=============================================================== Radio =============================================
public:
    RadioBase *rdRadio;

private:
    void vConnectSerialPort(const ConfiguracaoRadio &ConfiguracoesRadio);
    void vHandleRadioError(const QString &strErro);
    void vHandlePacketError(ErrorCode erro);

private slots:
    void vUpdateSerialPorts();
    void vConnectRadio();
    void vDisconnectRadio();
    void vGetSerialPortData();
//=============================================================== System identification =============================
private slots:
    void on_startButton_clicked();

    void on_stopButton_clicked();

    void on_saveButton_clicked();

    void on_antialiasingCheck_toggled(bool checked);

    //PRBS
    void on_signalPRBS_clicked(bool checked);
    //PRBS

    //STEP
    void on_signalStep_clicked(bool checked);
    //STEP

    //GBN
    void on_signalGBN_clicked(bool checked);
    //GBN

    //Sine
    void on_signalSine_clicked(bool checked);
    //Sine

    void on_radCampoReal_clicked();

    void on_radGrSim_clicked();

    void on_createModel_clicked();

    void on_setRuntimeFolder_clicked();

    void on_setMATLABAppFolder_clicked();

public:
    double dCurrentOutputX, dCurrentOutputY, dCurrentOutputW;//Stores the current output signal
    Output outCurrentOutput; //Stores the current output being excited

private:
    PRBSGenerator *prbsGenX, *prbsGenY, *prbsGenW;
    GBNGenerator  *gbnGenX , *gbnGenY , *gbnGenW ;

    QTimer *tmrTimerPRBS, *tmrTimerStep, *tmrTimerGBN, *tmrTimerSine;
    QTimer *tmrUpdateRobot;
    QTimer *tmrSampleData;
    QElapsedTimer timestampTimer;

    QFile fileX, fileY, fileW, fileXYW;
    QString dataX, dataY, dataW;

    double dTStep; //Stores the current step response time
    double dTSine; //Stores the current sine wave time
    QVector2D vt2dInitialPosition; // Posicao inicial do robo para calcular o deslocamento

    QVector2D decompoeVelocidadeRobo(QVector2D tempVel, Atributos &_robot);
    int iCalculaQuadrante(double _angulo);
    double dGetStepValue(double amplitude);
    double dGetSineValue(double amplitude);

    void vGetIndividualPRBS();
    void vGetSimultaneousPRBS();

    void vGetIndividualGBN();
    void vGetSimultaneousGBN();

    void vGetIndividualSTEP();
    void vGetSimultaneousSTEP();

public slots:
    void vGetPRBSValue();
    void vGetStepValue();
    void vGetGBNValue();
    void vGetSineValue();
    void vAtualizaRobo();
    void vAmostrar();
};

#endif // SYSTEMIDENTIFICATION_H
