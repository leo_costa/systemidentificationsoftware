#include "gbngenerator.h"

GBNGenerator::GBNGenerator()
{
    count = prevVal = 0;
    randomGen = QRandomGenerator::securelySeeded();
}

void GBNGenerator::startGBN(int samples, double _p)
{
    count = 0;
    nSamples = samples;
    p = _p;
    prevVal = 0;
}

double GBNGenerator::dGetValue(double Amplitude)
{
    int outValue = prevVal;
    double out = 0;

    //Generates a value between 0 and 1 with 6 decimal precision
    double randomValue = randomGen.bounded(0, 1e6)/1.0e6;

    if(randomValue < p)//Change value
    {
        if(qAbs(prevVal) > 0)
            outValue = 0;
        else
            outValue = Amplitude;
    }

    prevVal = out = outValue;
    count++;

    if(count >= nSamples)
        out = -950;

    return out;
}

int GBNGenerator::getCount() const
{
    return count;
}
