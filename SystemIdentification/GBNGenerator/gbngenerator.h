#ifndef GBNGENERATOR_H
#define GBNGENERATOR_H

#include <qmath.h>
#include <QDebug>
#include <QObject>
#include <QRandomGenerator>

class GBNGenerator
{
    int count;
    int nSamples;
    double p;
    double prevVal;
    QRandomGenerator randomGen;

public:
    GBNGenerator();
    ~GBNGenerator();

    void startGBN(int samples, double _p);
    double dGetValue(double Amplitude);
    int getCount() const;
};

#endif // GBNGENERATOR_H
