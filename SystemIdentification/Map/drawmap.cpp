#include "Map/drawmap.h"

drawMap::drawMap(QVector2D _vt2dTamanhoCampo, QVector2D _vt2dTamanhoLabel, float _fEscalaX, float _fEscalaY, int _iLarguraGol, int _iProfundidadeGol, int _iLarguraPenalty, int _iProfundidadePenalty)
{
    fEscalaX = _fEscalaX;
    fEscalaY = _fEscalaY;
    iLarguraGol = _iLarguraGol;
    iProfundidadeGol = _iProfundidadeGol;
    iLarguraPenalty = _iLarguraPenalty;
    iProfundidadePenalty = _iProfundidadePenalty;

    vt2dTamanhoCampo = _vt2dTamanhoCampo;


    pen = new QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
    vt2dTamanhoLabel = _vt2dTamanhoLabel;

}

drawMap::~drawMap()
{
    delete pen;
}

QVector2D drawMap::vt2dEscalas()
{
    return QVector2D(fEscalaX, fEscalaY);
}

void drawMap::vMudarDimensoes(QVector2D _vt2dTamanhoCampo, QVector2D _vt2dTamanhoLabel, float _fEscalaX, float _fEscalaY, int _iLarguraGol, int _iProfundidadeGol, int _iLarguraPenalty, int _iProfundidadePenalty)
{
    fEscalaX = _fEscalaX;
    fEscalaY = _fEscalaY;
    iLarguraGol = _iLarguraGol;
    iProfundidadeGol = _iProfundidadeGol;
    iLarguraPenalty = _iLarguraPenalty;
    iProfundidadePenalty = _iProfundidadePenalty;

    vt2dTamanhoCampo = _vt2dTamanhoCampo;

    vt2dTamanhoLabel = _vt2dTamanhoLabel;

}

void drawMap::vDesenhaAmbiente(AmbienteCampo *acAmbiente, QPixmap &pmImagem, bool _bVerticalHorizontal)
{
    QRect retanguloCampo(iOffsetCampo/fEscalaX , (-1)*(iOffsetCampo/fEscalaY ), qRound((vt2dTamanhoCampo.x()) / fEscalaX ), (-1)*qRound((vt2dTamanhoCampo.y()) / fEscalaY ));

    QPixmap pmAux = pmImagem;
    QPainter paint(&pmAux);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    pmAux.fill(QColor(0,128,0));



    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ), ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(*pen);
    paint.drawRect(retanguloCampo);

    retanguloCampo = QRect(iOffsetCampo/fEscalaX , (iOffsetCampo/fEscalaY ), qRound(vt2dTamanhoCampo.x() / fEscalaX ), qRound(vt2dTamanhoCampo.y() / fEscalaY ));
    paint.drawLine((retanguloCampo.width())/2 + iOffsetCampo/fEscalaX , (-1)*(iOffsetCampo/fEscalaY ),
                   (retanguloCampo.width())/2 + iOffsetCampo/fEscalaX , (-1)*(retanguloCampo.height()+iOffsetCampo/fEscalaY ) );
    paint.drawLine((iOffsetCampo + iProfundidadePenalty)/fEscalaX                         , (-1)*((retanguloCampo.height())/2 + iOffsetCampo/fEscalaY ),
                    retanguloCampo.width()+(iOffsetCampo - iProfundidadePenalty)/fEscalaX , (-1)*((retanguloCampo.height())/2 + iOffsetCampo/fEscalaY ) );

    vDesenhaAreaPenalty(&paint);
    vDesenhaRegioesCampo(&paint);

    paint.drawEllipse(QPoint( (retanguloCampo.width())/2 + iOffsetCampo/fEscalaX, (-1)*((retanguloCampo.height())/2 + iOffsetCampo/fEscalaY)),
                              qRound(acAmbiente->iPegaRaioCirculoCentroCampo()/fEscalaX), qRound(acAmbiente->iPegaRaioCirculoCentroCampo()/fEscalaY));

    Atributos atbInimigo, atbAliado;
    Bola blBola;

    QVector2D vt2dPontoRobo;

    int iRaio = iRaioSegurancaOponente;

    for(auto &n : acAmbiente->rbtRoboAdversario)
    {
        atbInimigo = n->atbRetornaRobo();

        if(atbInimigo.bEmCampo == true)
        {
            if(acAmbiente->CorTime == timeAzul)
            {
                paint.setPen(QPen(Qt::yellow, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
                paint.setBrush(Qt::darkYellow);
            }
            else
            {
                paint.setPen(QPen(Qt::cyan, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
                paint.setBrush(Qt::blue);
            }

            vt2dPontoRobo = atbInimigo.vt2dPosicaoAtual;

            vConvertePontoRealParaImagem(vt2dPontoRobo);

            vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);

            paint.drawEllipse(vt2dPontoRobo.toPointF(), static_cast<int>(iDiametroRobo*0.5/fEscalaX), static_cast<int>(iDiametroRobo*0.5/fEscalaY));

            paint.setBrush(Qt::transparent);

            if(bDesenharAreaSeguranca)
                paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(iRaio/(fEscalaX)), qRound(iRaio/(fEscalaY)));

            vt2dPontoRobo = vt2dPontoRobo - QVector2D(iDiametroRobo/(2*fEscalaX), iDiametroRobo/(2*fEscalaY));

            paint.drawText(vt2dPontoRobo.toPoint(), QString("  %1").arg(atbInimigo.id));

//            QTransform transform;
//            float fAngulo = atbInimigo.rotation;
//            fAngulo = fAngulo*180/PI-90;

//            if(fAngulo < 0)
//                fAngulo += 360;

            //transform = transform.rotate(-fAngulo);
            //QImage imImagemAuxiliar = pngRobos.at(iIndiceImagem).transformed(transform);

            //paint.drawImage(vt2dPontoRobo.toPoint(), imImagemAuxiliar);
        }
    }

    iRaio = iRaioSegurancaRobo;

    for(auto &n : acAmbiente->rbtRoboAliado)
    {
        atbAliado = n->atbRetornaRobo();

        if(atbAliado.bEmCampo == true)
        {
            if(acAmbiente->CorTime == timeAzul)
            {
                paint.setPen(QPen(Qt::cyan, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
                paint.setBrush(Qt::blue);
            }
            else
            {
                paint.setPen(QPen(Qt::yellow, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
                paint.setBrush(Qt::darkYellow);
            }

            vt2dPontoRobo = atbAliado.vt2dPosicaoAtual;

            vConvertePontoRealParaImagem(vt2dPontoRobo);

            vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);

            paint.drawEllipse(vt2dPontoRobo.toPointF(), static_cast<int>(iDiametroRobo*0.5/fEscalaX), static_cast<int>(iDiametroRobo*0.5/fEscalaY));

            paint.setBrush(Qt::transparent);

            if(bDesenharAreaSeguranca)
                paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(iRaio/(fEscalaX)), qRound(iRaio/(fEscalaY)));

            vt2dPontoRobo = vt2dPontoRobo - QVector2D(0/*iDiametroRobo/(2*fEscalaX)*/, iDiametroRobo/(2*fEscalaY));

            paint.setPen(QPen(Qt::black, 2.5, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));// Amarelo

            paint.drawText(vt2dPontoRobo.toPoint(), QString("%1").arg(atbAliado.id));

//            QTransform transform;
//            float fAngulo = atbAliado.rotation;
//            fAngulo = fAngulo*180/PI-90;

//            if(fAngulo < 0)
//                fAngulo += 360;

//            transform = transform.rotate(-fAngulo);
//            QImage imImagemAuxiliar = pngRobos.at(iIndiceImagem).transformed(transform);

//            paint.drawImage(vt2dPontoRobo.toPoint(), imImagemAuxiliar);
        }
    }

    iRaio = iRaioSegurancaBola;
    acAmbiente->vPegaBola(blBola);

    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {

        if(blBola.bEmCampo == true)
        {
            paint.setPen(QPen(QColor(255,180,0), 2.5, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));//Cor Laranja
            paint.setBrush(QColor(255,180,0));

            vt2dPontoRobo = blBola.vt2dPosicaoAtual;

            vConvertePontoRealParaImagem(vt2dPontoRobo);

            vt2dPontoRobo.setY(vt2dPontoRobo.y()*-1);
            paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(iDiametroBola/(fEscalaX)), qRound(iDiametroBola/(fEscalaY)));

            paint.setBrush(Qt::transparent);
            paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(200/fEscalaX), qRound(200/fEscalaY));

            if(bDesenharAreaSeguranca)
                paint.drawEllipse(vt2dPontoRobo.toPoint(), qRound(iRaio/(fEscalaX)), qRound(iRaio/(fEscalaY)));
//            paint.drawText(vt2dPontoRobo.toPoint(), QString("Bola"));
//            vt2dPontoRobo = vt2dPontoRobo - QVector2D(iDiametroBola/(2*fEscalaX), iDiametroBola/(2*fEscalaY));

//            paint.drawImage(vt2dPontoRobo.toPoint(), pngBola);
        }
    }

    paint.setPen(QPen(Qt::white, 2, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
    paint.end();
    pmImagem = pmAux;
}

void drawMap::vDesenhaGol(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz, Time _time, int _iLadoCampo)
{
    if(_time == timeAzul)
    {
        if(_iLadoCampo == XPositivo)
        {
            pntPincel->setPen(QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
            //Desenha o gol no lado direito, X Positivo
            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->setPen(QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

            //Desenha o gol no lado esquerdo, X Negativo
            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);
        }
        else
        {
            pntPincel->setPen(QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

            //Desenha o gol no lado esquerdo, X Negativo
            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->setPen(QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
            //Desenha o gol no lado direito, X Positivo
            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);
        }
    }

    else if(_time == timeAmarelo)
    {
        if(_iLadoCampo == XPositivo)
        {
            pntPincel->setPen(QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
            //Desenha o gol no lado direito, X Positivo
            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->setPen(QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

            //Desenha o gol no lado esquerdo, X Negativo
            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);
        }
        else
        {
            pntPincel->setPen(QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

            //Desenha o gol no lado esquerdo, X Negativo
            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                 iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                iOffsetCampo/fEscalaX - iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->setPen(QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
            //Desenha o gol no lado direito, X Positivo
            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

            pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                                vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX + iProfundidadeGol/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraGol/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);
        }
    }

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
}

void drawMap::vDesenhaGol(QPainter *pntPincel, Time _time, int _iLadoCampo)
{
    QPen penXPositivo = pntPincel->pen();
    QPen penXNegativo = pntPincel->pen();

    if(_time == timeAzul)
    {
        if(_iLadoCampo == XPositivo)
        {
            penXPositivo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
            penXNegativo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
        }
        else
        {
            penXPositivo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
            penXNegativo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
        }
    }

    else if(_time == timeAmarelo)
    {
        if(_iLadoCampo == XPositivo)
        {
            penXPositivo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
            penXNegativo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
        }
        else
        {
            penXPositivo = QPen(Qt::blue, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
            penXNegativo = QPen(Qt::yellow, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin);
        }
    }

    //Desenha o gol no lado direito, X Positivo
    pntPincel->setPen(penXPositivo);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo) / fEscalaX                   , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo) / fEscalaX                   , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo + iProfundidadeGol) / fEscalaX, (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY);


    //Desenha o gol no lado esquerdo, X Negativo
    pntPincel->setPen(penXNegativo);

    pntPincel->drawLine( iOffsetCampo/fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( iOffsetCampo/fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (iOffsetCampo - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraGol/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo - iProfundidadeGol) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraGol/2 + iOffsetCampo) / fEscalaY);

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
}

void drawMap::vDesenhaAreaPenalty(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz)
{
    //Desenha a area do penalty no lado direito
    pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                        vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX - iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

    pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                        vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX - iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

    pntPincel->drawLine(vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX - iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                        vt2dTamanhoMatriz.x() * fResolucaoCampo / fEscalaX - iOffsetCampo/fEscalaX - iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

    //Desenha a area do penalty no lado esquerdo
    pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                         iOffsetCampo/fEscalaX + iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY);

    pntPincel->drawLine( iOffsetCampo/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                         iOffsetCampo/fEscalaX + iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);

    pntPincel->drawLine(iOffsetCampo/fEscalaX + iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 + iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo / fEscalaY,
                        iOffsetCampo/fEscalaX + iProfundidadePenalty/fEscalaX, (-1)*( vt2dTamanhoMatriz.y()/2 - iLarguraPenalty/2 / fResolucaoCampo) * fResolucaoCampo/ fEscalaY);
}

void drawMap::vDesenhaAreaPenalty(QPainter *pntPincel)
{
    //Desenha a area do penalty no lado direito
    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo) / fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo - iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo) / fEscalaX                        , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo - iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (vt2dTamanhoCampo.x() + iOffsetCampo - iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (vt2dTamanhoCampo.x() + iOffsetCampo - iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);

    //Desenha a area do penalty no lado esquerdo
    pntPincel->drawLine( iOffsetCampo/fEscalaX                            , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo + iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( iOffsetCampo/fEscalaX                            , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo + iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);

    pntPincel->drawLine( (iOffsetCampo + iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 + iLarguraPenalty/2 + iOffsetCampo) / fEscalaY,
                         (iOffsetCampo + iProfundidadePenalty) / fEscalaX , (-1)*( vt2dTamanhoCampo.y()/2 - iLarguraPenalty/2 + iOffsetCampo) / fEscalaY);
}

void drawMap::vDesenhaRegioesCampo(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz)
{
    int y1,y2, xRegiao, nRegioes;
    QPoint P1, P2;

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(0.3);

    nRegioes = qCeil((vt2dTamanhoCampo.x())/iLarguraDivisaoCampo);

    y1 = (vt2dTamanhoMatriz.y()*fResolucaoCampo - iOffsetCampo)/fEscalaY;
    y2 = iOffsetCampo/fEscalaY;

    P1.setY(-y1);
    P2.setY(-y2);

    for(int n=0; n < nRegioes; ++n)
    {
        xRegiao = (iOffsetCampo + iLarguraDivisaoCampo*n)/fEscalaX;
        P1.setX(xRegiao);
        P2.setX(xRegiao);

        pntPincel->drawLine(P1,P2);
    }

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(1);
}

void drawMap::vDesenhaRegioesCampo(QPainter *pntPincel)
{
    int y1,y2, xRegiao, nRegioes;
    QPoint P1, P2;

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::DashLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(0.3);

    nRegioes = qCeil((vt2dTamanhoCampo.x())/iLarguraDivisaoCampo);

    y1 = (vt2dTamanhoCampo.y() + iOffsetCampo)/fEscalaY;
    y2 = iOffsetCampo/fEscalaY;

    P1.setY(-y1);
    P2.setY(-y2);

    for(int n=0; n < nRegioes; ++n)
    {
        xRegiao = (iOffsetCampo + iLarguraDivisaoCampo*n)/fEscalaX;
        P1.setX(xRegiao);
        P2.setX(xRegiao);

        pntPincel->drawLine(P1,P2);
    }

    pntPincel->setPen(QPen(Qt::white, dLarguraLinhasBrancas, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    pntPincel->setOpacity(1);
}

void drawMap::vDesenhaPosicaoTimeout(QPainter *pntPincel, QVector2D _vt2dPosicaoTimeout)
{
    vConvertePontoRealParaImagem(_vt2dPosicaoTimeout);

    pntPincel->drawImage(QPoint(_vt2dPosicaoTimeout.x() - iDiametroRobo/(2*fEscalaX), -(_vt2dPosicaoTimeout.y() + iDiametroRobo/(2*fEscalaY))), pngPosicaoTimeout);

}

void drawMap::vConvertePontoRealParaImagem(QVector2D &_vt2dPonto)
{
    QVector2D vetorDeConversao(((vt2dTamanhoCampo.x() - iEspessuraLinha) / 2) + iOffsetCampo,
                               ((vt2dTamanhoCampo.y() - iEspessuraLinha) / 2) + iOffsetCampo);//torna as coordenadas dos robôs positivas

    _vt2dPonto.setX((_vt2dPonto.x() + vetorDeConversao.x() ) /fEscalaX);
    _vt2dPonto.setY((_vt2dPonto.y() + vetorDeConversao.y() ) /fEscalaY);

}

void drawMap::vConverteCaminhoRealParaImagem(QVector<QVector2D> &_caminho, QVector2D _tamanhoCampo)
{
    QVector2D vetorDeConversao(((_tamanhoCampo.x() - iEspessuraLinha) / 2) + iOffsetCampo,
                               ((_tamanhoCampo.y() - iEspessuraLinha) / 2) + iOffsetCampo);//torna as coordenadas dos robôs positivas

    for(auto & n : _caminho)
    {
        n.setX((n.x() + vetorDeConversao.x() ) /fEscalaX);
        n.setY((n.y() + vetorDeConversao.y() ) /fEscalaY);
    }
}

void drawMap::vDesenhaPontos(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor &_clrCorCaminho, const double _dTamanhoPonto, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QPixmap *temp ;
    temp = &outMap;

    caminho = std::move(_caminho);

    vConverteCaminhoRealParaImagem(caminho, vt2dTamanhoCampo);

    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);


    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ),
                          ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ) );//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(QPen(_clrCorCaminho, _dTamanhoPonto, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for(int n=0; n < (int)caminho.size(); ++n)
    {
        paint.drawPoint(caminho[n].x(), (-1)*caminho[n].y());
    }

    paint.end();

}

void drawMap::vDesenhaLinhas(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor &_clrCorCaminho, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QPixmap *temp ;
    temp = &outMap;

    caminho = std::move(_caminho);

    vConverteCaminhoRealParaImagem(caminho, vt2dTamanhoCampo);

    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ), ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }



    for(int n=0; n < (int)caminho.size()-1; ++n)
    {
        paint.setPen(QPen(_clrCorCaminho, 3, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        paint.drawLine(caminho[n].x(), (-1)*caminho[n].y() , caminho[n+1].x(), (-1)*caminho[n+1].y() );

        paint.setPen(QPen(_clrCorCaminho, 5, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));
        paint.drawPoint(caminho[n].x(), (-1)*caminho[n].y());

    }

    paint.end();

}

void drawMap::vDesenhaLinhas(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor &_clrCorCaminho, int _iEspessuraLinha, bool _bVerticalHorizontal)
{
    QVector<QVector2D> caminho;

    QPixmap *temp ;
    temp = &outMap;

    caminho = std::move(_caminho);

    vConverteCaminhoRealParaImagem(caminho, vt2dTamanhoCampo);

    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);


    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ), ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }



    for(int n=0; n < (int)caminho.size()-1; ++n)
    {
        paint.setPen(QPen(_clrCorCaminho, _iEspessuraLinha, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin));

        paint.drawLine(caminho[n].x(), (-1)*caminho[n].y() , caminho[n+1].x(), (-1)*caminho[n+1].y() );

    }

    paint.end();

}

void drawMap::vDesenhaPonto(QVector2D _vt2dPonto, QPixmap &outMap, const QColor &_clrCorCaminho, bool _bVerticalHorizontal)
{

    QPixmap *temp ;
    temp = &outMap;

    vConvertePontoRealParaImagem(_vt2dPonto);

    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ), ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(QPen(_clrCorCaminho, 5, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    paint.drawPoint(_vt2dPonto.x(), (-1)*_vt2dPonto.y());

    paint.end();

}

void drawMap::vDesenhaCirculo(QVector2D _vt2dCentro, QPixmap &outMap, int _iRaio, const QColor &_clrCorCirculo, int _iEspessuraLinha, bool _bVerticalHorizontal)
{
    QPixmap *temp ;
    temp = &outMap;

    vConvertePontoRealParaImagem(_vt2dCentro);

    _vt2dCentro.setY(_vt2dCentro.y()*-1);

//    QRect rectRetangulo;
//    rectRetangulo.setCoords(_vt2dCentro.x() - _iRaio/fEscalaX, -1*(_vt2dCentro.y() - _iRaio/fEscalaY),
//                            _vt2dCentro.x() + _iRaio/fEscalaX, -1*(_vt2dCentro.y() + _iRaio/fEscalaY) );


    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    if(_bVerticalHorizontal == DesenhoHorizontal)
        paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    else
    {
        paint.rotate(270);
        paint.translate( -((vt2dTamanhoCampo.x()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaX ),
                          ((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ) );//Desloca a origem do sistema de coordenadas do QT para baixo
    }

    paint.setPen(QPen(_clrCorCirculo, _iEspessuraLinha, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
    paint.drawEllipse(_vt2dCentro.toPointF(), static_cast<int>(_iRaio/fEscalaX), static_cast<int>(_iRaio/fEscalaY));

    paint.end();
}

void drawMap::vDesenhaArvoreRRT(QVector<QVector2D> _vt2dPontos, QVector<QVector2D> _vt2dParentes, QPixmap &outMap, const QColor &_clrCorArvore)
{
    QPixmap *temp ;
    temp = &outMap;
    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    vConverteCaminhoRealParaImagem(_vt2dPontos  , vt2dTamanhoCampo);
    vConverteCaminhoRealParaImagem(_vt2dParentes, vt2dTamanhoCampo);

    paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));
    paint.setPen(QPen(_clrCorArvore, 1, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));

    for(int n=0; n < _vt2dPontos.size(); ++n)
    {
        paint.drawLine(_vt2dPontos.at(n).x()  , (-1)*_vt2dPontos.at(n).y(),
                       _vt2dParentes.at(n).x(), (-1)*_vt2dParentes.at(n).y() );
        //            paint.drawPoint(_vrArvore[n].vt2dPosicao.x(), (-1)*_vrArvore[n].vt2dPosicao.y());
    }

    paint.end();

}

void drawMap::vTesteMeioCampo(int _iLadoUtilizado, QPixmap &outMap)
{
    QPixmap *temp ;
    temp = &outMap;
    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));
    paint.setOpacity(0.7);

    double dX1 = 0, dX2 = 0;

    if(_iLadoUtilizado == XPositivo)
    {
        dX1 = 0;
        dX2 = qRound((vt2dTamanhoCampo.x()/2 + iOffsetCampo) / fEscalaX );
    }
    else if(_iLadoUtilizado == XNegativo)
    {
        dX1 = qRound((vt2dTamanhoCampo.x()/2 + iOffsetCampo) / fEscalaX );
        dX2 = qRound((vt2dTamanhoCampo.x() + iOffsetCampo)   / fEscalaX );
    }
    QRect retanguloCampo(dX1,
                           0,
                         dX2,
                         (-1)*qRound((vt2dTamanhoCampo.y()   + 2*iOffsetCampo) / fEscalaY ) );

    paint.fillRect(retanguloCampo, Qt::darkGray);

    paint.end();
}

void drawMap::vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola, QVector2D _vt2dPosicaoBola, QPixmap &outMap, const QColor &_clrCorCaminho)
{
    QVector<QVector2D> caminho;

    QPixmap *temp ;
    temp = &outMap;

    caminho = std::move(_vt2dMiraBola);

    vConverteCaminhoRealParaImagem(caminho, vt2dTamanhoCampo);
    vConvertePontoRealParaImagem(_vt2dPosicaoBola);

    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));

    paint.setOpacity(0.1);

    for(int n=0; n < (int)caminho.size(); ++n)
    {
        paint.setPen(QPen(_clrCorCaminho, 3, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
        paint.drawLine(caminho[n].x(), (-1)*caminho[n].y() , _vt2dPosicaoBola.x(), (-1)*_vt2dPosicaoBola.y() );

        //            paint.setPen(QPen(Qt::green, 4, Qt::SolidLine, Qt::FlatCap, Qt::RoundJoin));
        //            paint.drawPoint(caminho[n].x(), (-1)*caminho[n].y());

    }

    paint.setOpacity(1);

    paint.end();

}

void drawMap::vMostrarFPS(int FPS, QPixmap &outmap)
{
    QPixmap *temp ;
    temp = &outmap;
    QPainter paint(temp);
    paint.setRenderHint(QPainter::HighQualityAntialiasing);

    paint.translate(0,((vt2dTamanhoCampo.y()+ iOffsetCampo*2 - iEspessuraLinha) / fEscalaY ));
    QVector2D aux = QVector2D(-vt2dTamanhoCampo.x()/2,vt2dTamanhoCampo.y()/2-200);
    vConvertePontoRealParaImagem(aux);
    paint.drawText(aux.x(), -1*aux.y(), QString::number(FPS) + " FPS");

    paint.end();
}
