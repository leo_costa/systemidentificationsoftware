#include <utility>

#ifndef DRAWMAP_H
#define DRAWMAP_H

#include <QVector2D>
#include <QPainter>
#include <QPen>
#include <QPixmap>
#include <qmath.h>
#include "Constantes_e_Funcoes_Auxiliares/constantes.h"
#include "Ambiente/futbolenvironment.h"

const double dLarguraLinhasBrancas = 2.5;


class drawMap
{
    int  iLarguraGol, iProfundidadeGol, iLarguraPenalty, iProfundidadePenalty;
    QVector2D vt2dTamanhoCampo;
    QVector2D vt2dTamanhoLabel;
    float fEscalaX, fEscalaY;
    QPen *pen;
    QImage pngBola,pngGrama, pngPosicaoTimeout, pngPadreSaboia, pngInimigo;
    QVector<QImage> pngRobos;
    QVector <QString> pngAddress;


public:

    drawMap(QVector2D _vt2dTamanhoCampo, QVector2D _vt2dTamanhoLabel, float _fEscalaX, float _fEscalaY, int _iLarguraGol, int _iProfundidadeGol, int _iLarguraPenalty, int _iProfundidadePenalty);

    ~drawMap();

    QVector2D vt2dEscalas();

    void vMudarDimensoes(QVector2D _vt2dTamanhoCampo, QVector2D _vt2dTamanhoLabel, float _fEscalaX, float _fEscalaY, int _iLarguraGol, int _iProfundidadeGol, int _iLarguraPenalty, int _iProfundidadePenalty);

    void vDesenhaAmbiente(AmbienteCampo *acAmbiente, QPixmap &pmImagem, bool _bVerticalHorizontal = DesenhoHorizontal);

    void vDesenhaGol(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz, Time _time, int _iLadoCampo);

    void vDesenhaGol(QPainter *pntPincel, Time _time, int _iLadoCampo);

    void vDesenhaAreaPenalty(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz);

    void vDesenhaAreaPenalty(QPainter *pntPincel);

    void vDesenhaRegioesCampo(QPainter *pntPincel, QVector2D vt2dTamanhoMatriz);

    void vDesenhaRegioesCampo(QPainter *pntPincel);

    void vDesenhaPosicaoTimeout(QPainter *pntPincel, QVector2D _vt2dPosicaoTimeout);

    void vConvertePontoRealParaImagem(QVector2D &_vt2dPonto);

    void vConverteCaminhoRealParaImagem(QVector<QVector2D> &_caminho, QVector2D _tamanhoCampo);

    void vDesenhaPontos(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor& _clrCorCaminho, const double _dTamanhoPonto, bool _bVerticalHorizontal = DesenhoHorizontal);

    void vDesenhaLinhas(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor& _clrCorCaminho, bool _bVerticalHorizontal = DesenhoHorizontal);

    void vDesenhaLinhas(QVector<QVector2D> _caminho, QPixmap &outMap, const QColor &_clrCorCaminho, int _iEspessuraLinha, bool _bVerticalHorizontal);

    void vDesenhaPonto(QVector2D _vt2dPonto, QPixmap &outMap, const QColor& _clrCorCaminho, bool _bVerticalHorizontal = DesenhoHorizontal);

    void vDesenhaCirculo(QVector2D _vt2dCentro, QPixmap &outMap, int _iRaio, const QColor& _clrCorCirculo, int _iEspessuraLinha, bool _bVerticalHorizontal = DesenhoHorizontal);

    void vDesenhaArvoreRRT(QVector<QVector2D> _vt2dPontos, QVector<QVector2D> _vt2dParentes, QPixmap &outMap, const QColor& _clrCorArvore);

    void vTesteMeioCampo(int _iLadoUtilizado, QPixmap &outMap);

    void vDesenhaMiraBola(QVector<QVector2D> _vt2dMiraBola, QVector2D _vt2dPosicaoBola, QPixmap &outMap, const QColor& _clrCorCaminho);

    void vMostrarFPS(int FPS, QPixmap &outmap);
};

#endif // DRAWMAP_H
