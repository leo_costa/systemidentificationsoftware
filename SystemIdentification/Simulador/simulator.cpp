//#include "geral.h"


#include "simulator.h"

grSim_Packet RoboFei_Simulator::create_packet(ROBO_SIM _r, int _team){
  grSim_Packet packet;
  packet.mutable_commands()->set_timestamp(0.0);
  packet.mutable_commands()->set_isteamyellow(_team);
  grSim_Robot_Command* command = packet.mutable_commands()->add_robot_commands();
  command->set_id(_r.id);

  command->set_wheelsspeed(_r.wheelsspeed);
  command->set_wheel1(_r.wheel1);
  command->set_wheel2(_r.wheel2);
  command->set_wheel3(_r.wheel3);
  command->set_wheel4(_r.wheel4);
  command->set_veltangent(_r.veltangent);
  command->set_velnormal(_r.velnormal);
  command->set_velangular(_r.velangular);

  command->set_kickspeedx(_r.kickspeedx);
  command->set_kickspeedz(_r.kickspeedz);
  command->set_spinner(_r.spinner);

  return packet;
}

void RoboFei_Simulator::send_packet(grSim_Packet _p){
  //grSim_Packet packet;
  //bool yellow = false;
  //if (cmbTeam->currentText()=="Yellow") yellow = true;
  //packet.mutable_commands()->set_isteamyellow(_team);

  QByteArray dgram;
  dgram.resize(_p.ByteSize());
  _p.SerializeToArray(dgram.data(), dgram.size());
  this->udpsocket->writeDatagram(dgram, this->addr_simulator, this->port_simulator);
}
