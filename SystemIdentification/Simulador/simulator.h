#include "protofiles/grSim_Packet.pb.h"
#include "protofiles/grSim_Commands.pb.h"
#include "protofiles/grSim_Replacement.pb.h"
#include <QtNetwork>
#include <QUdpSocket>
#include <QHostAddress>

#ifndef SIMULATOR_H
#define SIMULATOR_H


typedef struct {
  int id;
  float kickspeedx, kickspeedz;
  float veltangent, velnormal, velangular;
  bool spinner, wheelsspeed;
  float wheel1, wheel2, wheel3, wheel4;
} ROBO_SIM;

#define BLUE_TEAM 0
#define YELLOW_TEAM 1

class RoboFei_Simulator{
   public:

   RoboFei_Simulator(QUdpSocket * _u, QHostAddress _addr,  quint16 _port){
     udpsocket = _u;
     addr_simulator = _addr;
     port_simulator = _port;
   }

   void send_packet(grSim_Packet _p);
   grSim_Packet create_packet(ROBO_SIM _r, int _team);
   private:
   QUdpSocket * udpsocket;
   QHostAddress addr_simulator;
   quint16 port_simulator;

};

#endif // SIMULATOR_H
