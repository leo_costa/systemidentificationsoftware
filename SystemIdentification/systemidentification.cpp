#include "systemidentification.h"
#include "ui_systemidentification.h"

void runMATLABApp(QString openAppString)
{
    const char *c_str2 = openAppString.toLocal8Bit().data();

    system(c_str2);

    QMessageBox msgBox;
    msgBox.setText("App closed !");
    msgBox.exec();
}

systemidentification::systemidentification(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::systemidentification)
{
    ui->setupUi(this);

    rdRadio                  = new RadioBase;
    futbolEnvironment        = new AmbienteCampo;
    pmFieldImage = nullptr;
    dmFieldPainter = nullptr;
    etmFPS.start();
    iFPSCounter.clear();

    counterInterfaceLED = 0;
    ledIndex = 0;
    strImages.append(":Images/Leds/square_green.svg");
    strImages.append(":Images/Leds/square_grey.svg");

    grsimSimulator = nullptr;

    //PRBS Setup
    tmrTimerPRBS = new QTimer(this);
    connect(tmrTimerPRBS, &QTimer::timeout, this, &systemidentification::vGetPRBSValue);

    prbsGenX = new PRBSGenerator();
    prbsGenY = new PRBSGenerator();
    prbsGenW = new PRBSGenerator();

    //GBN Setup
    tmrTimerGBN = new QTimer(this);
    connect(tmrTimerGBN, &QTimer::timeout, this, &systemidentification::vGetGBNValue);

    gbnGenX = new GBNGenerator();
    gbnGenY = new GBNGenerator();
    gbnGenW = new GBNGenerator();

    //Step Setup
    tmrTimerStep = new QTimer(this);
    connect(tmrTimerStep, &QTimer::timeout, this, &systemidentification::vGetStepValue);

    //Sine Setup
    tmrTimerSine = new QTimer(this);
    connect(tmrTimerSine, &QTimer::timeout, this, &systemidentification::vGetSineValue);

    tmrUpdateRobot = new QTimer(this);
    connect(tmrUpdateRobot, &QTimer::timeout, this, &systemidentification::vAtualizaRobo);

    tmrSampleData = new QTimer(this);
    connect(tmrSampleData, &QTimer::timeout, this, &systemidentification::vAmostrar);

    dCurrentOutputX = dCurrentOutputY = dCurrentOutputW = 0;
    outCurrentOutput = None;

    fileX.close();
    fileY.close();
    fileW.close();
    fileXYW.close();
    dataX = dataY = dataW = "";

    vConfigureUI();

    tempoAmostragem = ui->samplingTime->value();
}

systemidentification::~systemidentification()
{
    if(pmFieldImage != nullptr)
    {
        delete pmFieldImage;
    }
    if(grsimSimulator != nullptr)
    {
        delete grsimSimulator;
    }
    delete futbolEnvironment;
    delete prbsGenX;
    delete prbsGenY;
    delete prbsGenW;
    delete ui;
}

void systemidentification::vConfigureUI()
{
    //Interface
    QPixmap pmLed;
    pmLed.load(":Images/Leds/circle_blue.svg", "svg");
    pmLed = pmLed.scaled(40,40,Qt::KeepAspectRatioByExpanding);
    ui->robotColor->addItem(QIcon(pmLed),"Blue", timeAzul);
    pmLed.load(":Images/Leds/circle_yellow.svg", "svg");
    pmLed = pmLed.scaled(40,40,Qt::KeepAspectRatioByExpanding);
    ui->robotColor->addItem(QIcon(pmLed),"Yellow", timeAmarelo);
    pmLed.load(":Images/Leds/square_red.svg", "svg");
    pmLed = pmLed.scaled(40,40,Qt::KeepAspectRatioByExpanding);
    ui->labelStatusImage->setPixmap(pmLed);

    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
    ui->saveButton->setEnabled(false);

    tmrUpdateInterface = new QTimer(this);
    connect(tmrUpdateInterface, &QTimer::timeout, this, &systemidentification::vUpdateUI);

    connect(ui->robotColor, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged), this, &systemidentification::vRobotColorChanged);
    //Interface

    //Network
    connect(ui->connectVision, &QPushButton::clicked, this, &systemidentification::vConnectNetwork);
    QList<QNetworkInterface> ifInterfaceLocal = QNetworkInterface::allInterfaces();
    for(auto & i : ifInterfaceLocal)
    {
        ui->networkInterfaceBox->addItem(i.humanReadableName());
    }
    //Network

    //Radio
    ui->sslrobocupPacket->setEnabled(false);
    connect(ui->connectRadio, &QPushButton::clicked, this, &systemidentification::vConnectRadio);
    connect(ui->refreshPort, &QPushButton::clicked, this, &systemidentification::vUpdateSerialPorts);
    connect(rdRadio->serial, &QSerialPort::readyRead, this, &systemidentification::vGetSerialPortData);

    ui->serialPortBox->clear();
    ui->baudRateBox->clear();

    ui->baudRateBox->addItem(QStringLiteral("9600")  , QSerialPort::Baud9600);
    ui->baudRateBox->addItem(QStringLiteral("19200") , QSerialPort::Baud19200);
    ui->baudRateBox->addItem(QStringLiteral("38400") , QSerialPort::Baud38400);
    ui->baudRateBox->addItem(QStringLiteral("115200"), QSerialPort::Baud115200);
    ui->baudRateBox->addItem(QStringLiteral("230400"), 230400);
    ui->baudRateBox->setCurrentIndex(4);

    for(const QSerialPortInfo &info : QSerialPortInfo::availablePorts())
    {
        QStringList list;
        list << info.portName();
        ui->serialPortBox->addItem(list.first(), list);
    }
    //Radio

    //Graficos
    vConfigurePlots("Vx", "[m/s]", ui->qcpPlotX);
    vConfigurePlots("Vy", "[m/s]", ui->qcpPlotY);
    vConfigurePlots("Vw", "[rad/s]", ui->qcpPlotW);
    //Graficos
}

void systemidentification::vUpdateUI()
{
    //Campo
    float fEscalaDesenhoX, fEscalaDesenhoY;
    QVector2D tamanhoCampo = futbolEnvironment->vt2dPegaTamanhoCampo(), tamanhoLabel;
    tamanhoLabel = QVector2D(ui->fieldImage->width(), ui->fieldImage->height());
    fEscalaDesenhoX = (tamanhoCampo.x() + iOffsetCampo*2) / tamanhoLabel.x();
    fEscalaDesenhoY = (tamanhoCampo.y() + iOffsetCampo*2) / tamanhoLabel.y();

    if(dmFieldPainter->vt2dEscalas().distanceToPoint(QVector2D(fEscalaDesenhoX, fEscalaDesenhoY)) > 1)
    {
        delete dmFieldPainter; dmFieldPainter = nullptr;
        delete pmFieldImage; pmFieldImage = nullptr;
        pmFieldImage = new QPixmap(tamanhoLabel.x(),tamanhoLabel.y());

        dmFieldPainter = new drawMap(tamanhoCampo, tamanhoLabel, fEscalaDesenhoX, fEscalaDesenhoY,
                                    futbolEnvironment->iPegaLarguraGol(), futbolEnvironment->iPegaProfundidadeGol(),
                                    futbolEnvironment->iPegaLarguraAreaPenalty(), futbolEnvironment->iPegaProfundidadeAreaPenalty());
    }

    //Desenha os campos
    dmFieldPainter->vDesenhaAmbiente(futbolEnvironment, *pmFieldImage);


    //Desenha o traço do robô monitorado
    if(iTamanhoTracoRobo > 0)
    {
        dmFieldPainter->vDesenhaPontos(futbolEnvironment->vtBufferRoboAliadosKALMAN[ui->robotID->value()], *pmFieldImage, Qt::red, 2.5);
    }

    //Desenha a direcao em que os robos estao mirando e caso algum queira chutar o mesmo eh circulado em vermelho
    Atributos atbRobo;
    for(auto &n : futbolEnvironment->rbtRoboAliado)
    {
        atbRobo = n->atbRetornaRobo();
        if(atbRobo.bEmCampo)
        {

            QVector<QVector2D> vt2dAnguloRobo;
            vt2dAnguloRobo.clear();
            vt2dAnguloRobo.append(atbRobo.vt2dPosicaoAtual);
            vt2dAnguloRobo.append(atbRobo.vt2dPosicaoAtual + QVector2D(150*qCos(atbRobo.rotation), 150*qSin(atbRobo.rotation)));

            dmFieldPainter->vDesenhaLinhas(vt2dAnguloRobo, *pmFieldImage, QColor(255,120,0));//Direcao que o robo esta olhando na cor laranja

        }
    }
    for(auto &n : futbolEnvironment->rbtRoboAdversario)
    {
        atbRobo = n->atbRetornaRobo();
        if(atbRobo.bEmCampo)
        {
            QVector<QVector2D> vt2dAnguloRobo;
            vt2dAnguloRobo.clear();
            vt2dAnguloRobo.append(atbRobo.vt2dPosicaoAtual);
            vt2dAnguloRobo.append(atbRobo.vt2dPosicaoAtual + QVector2D(150*qCos(atbRobo.rotation), 150*qSin(atbRobo.rotation)));

            dmFieldPainter->vDesenhaLinhas(vt2dAnguloRobo, *pmFieldImage, QColor(255,120,0));//Direcao que o adversario esta olhando
        }
    }

    int FPS = 1/(etmFPS.nsecsElapsed()/1e9);
    iFPSCounter.prepend(FPS);

    FPS = 0;
    if(iFPSCounter.size() > 15)
        iFPSCounter.pop_back();
    for(auto n : iFPSCounter)
        FPS += n;
    FPS = FPS/iFPSCounter.size();
    etmFPS.restart();

    dmFieldPainter->vMostrarFPS(FPS, *pmFieldImage);
    ui->fieldImage->setPixmap(*pmFieldImage);

    counterInterfaceLED++;

    if(counterInterfaceLED == 15 && outCurrentOutput != None)
    {
        counterInterfaceLED = 0;
        QPixmap pmLed;
        pmLed.load(strImages.at(ledIndex), "svg");
        pmLed = pmLed.scaled(25,25);
        ui->labelStatusImage->setPixmap(pmLed);
        ledIndex++;
        if(ledIndex > 1)
            ledIndex = 0;
    }

    vUpdatePlots();
}

void systemidentification::vRobotColorChanged(int index)
{
    Time color = static_cast<Time>(ui->robotColor->itemData(index).toInt());
    futbolEnvironment->CorTime = color;
}

void systemidentification::vUpdateField()
{
    const SSL_WrapperPacket _pkgUltimoPacoteRecebido = RoboController.getLastVision();

    if(_pkgUltimoPacoteRecebido.ByteSize() > 0)
    {
        //Atualiza as informacoes dos robos e da bola
        //Aqui sera detectado se o robo saiu da visao ou entao, caso ele esteja no campo sera adicionada sua posicao atual
        //no vetor utilizado pelo kalman para calcular sua posicao real, o mesmo vale para a bola.
        if(_pkgUltimoPacoteRecebido.has_detection())
        {
            const SSL_DetectionFrame & frameDeteccao = _pkgUltimoPacoteRecebido.detection();
            Time timeAtual = static_cast<Time>(ui->robotColor->itemData(ui->robotColor->currentIndex()).toInt());

            vDetectRobot(frameDeteccao);

            //Atualiza as informacoes da bola
            if(frameDeteccao.balls_size() > 0)
            {
                int iNumeroBolas = frameDeteccao.balls_size();

                for(int n=0; n < iNumeroBolas; ++n)
                {
                    vUpdateBall(frameDeteccao, n);
                }
            }

            int iNumeroRobosAliados = 0,
                iNumeroRobosAdversarios = 0;

            // Somos o time azul
            if(timeAtual == timeAzul)
            {
                iNumeroRobosAliados = frameDeteccao.robots_blue_size();
                iNumeroRobosAdversarios = frameDeteccao.robots_yellow_size();
            }
            // Somos o time amarelo
            else if(timeAtual == timeAmarelo)
            {
                iNumeroRobosAliados = frameDeteccao.robots_yellow_size();
                iNumeroRobosAdversarios = frameDeteccao.robots_blue_size();
            }

            for(int n=0; n < iNumeroRobosAliados; ++n)
            {
                vUpdateAlly(frameDeteccao, timeAtual, n);
            }

            for(int n=0; n < iNumeroRobosAdversarios; ++n)
            {
                vUpdateOpponent(frameDeteccao, timeAtual, n);
            }

            //Conta quantos robos aliados tem em campo
            int nAux = 0;
            for(int n=0; n < PLAYERS_PER_SIDE; ++n)
            {
                if(futbolEnvironment->rbtRoboAliado[n]->atbRetornaRobo().bEmCampo == true)
                    nAux++;
            }
            futbolEnvironment->vSetaRobosEmCampo(nAux);
        }

        //Recebe os dados de geometria do campo
        if(_pkgUltimoPacoteRecebido.has_geometry())
        {
            const SSL_GeometryData & frameGeometria = _pkgUltimoPacoteRecebido.geometry();
            bool campoMudou = false;
            vUpdateGeometry(frameGeometria, campoMudou);

            if(campoMudou == true)
            {
                QVector2D tamanhoCampo = futbolEnvironment->vt2dPegaTamanhoCampo();

                if(tmrUpdateInterface->isActive() == false)
                    tmrUpdateInterface->start(tempoAtualizacaoInterface);

                //Campo
                QVector2D tamanhoLabel = QVector2D(ui->fieldImage->width(), ui->fieldImage->height());
                float fEscalaDesenhoX = (tamanhoCampo.x() + iOffsetCampo*2) / tamanhoLabel.x(),
                      fEscalaDesenhoY = (tamanhoCampo.y() + iOffsetCampo*2) / tamanhoLabel.y();

                if(pmFieldImage == nullptr)
                {
                    pmFieldImage = new QPixmap(tamanhoLabel.x(), tamanhoLabel.y());
                }
                else
                {
                    delete pmFieldImage;
                    pmFieldImage = new QPixmap(tamanhoLabel.x(), tamanhoLabel.y());
                }

                if(dmFieldPainter == nullptr)
                {
                    dmFieldPainter = new drawMap(tamanhoCampo, tamanhoLabel, fEscalaDesenhoX, fEscalaDesenhoY,
                                                 futbolEnvironment->iPegaLarguraGol(), futbolEnvironment->iPegaProfundidadeGol(),
                                                 futbolEnvironment->iPegaLarguraAreaPenalty(), futbolEnvironment->iPegaProfundidadeAreaPenalty());
                }
                else
                {
                    dmFieldPainter->vMudarDimensoes(tamanhoCampo, tamanhoLabel, fEscalaDesenhoX, fEscalaDesenhoY,
                                                    futbolEnvironment->iPegaLarguraGol(), futbolEnvironment->iPegaProfundidadeGol(),
                                                    futbolEnvironment->iPegaLarguraAreaPenalty(), futbolEnvironment->iPegaProfundidadeAreaPenalty());
                }

                futbolEnvironment->vResetFrameBola();

                for(int n=0; n < PLAYERS_PER_SIDE; ++n)
                {
                    futbolEnvironment->rbtRoboAliado[n]->vResetFrameNumber();
                    futbolEnvironment->rbtRoboAdversario[n]->vResetFrameNumber();
                }

                //Pode inicializar o kalman aqui

                //Pode inicializar o kalman aqui

            }
        }

        // Kalman para os robos
        Atributos atbRoboAliado;
        QVector2D vt2dKalmanRobos;

        for(int i=0;i<PLAYERS_PER_SIDE;i++)
        {

            atbRoboAliado = futbolEnvironment->rbtRoboAliado[i]->atbRetornaRobo();
            if(futbolEnvironment->vtBufferPosRobosAliadosVisao[i].size() >= iTamanhoBufferRobo && atbRoboAliado.bEmCampo)
            {
                vt2dKalmanRobos = futbolEnvironment->vtBufferPosRobosAliadosVisao[i].constFirst();//vt2dKALMANRobo(futbolEnvironment->vtBufferPosRobosAliadosVisao[i]);

                if(vt2dKalmanRobos.distanceToPoint(atbRoboAliado.vt2dPosicaoAtual) > 1)
                {
                    futbolEnvironment->rbtRoboAliado[i]->vSetaPosicao(vt2dKalmanRobos);
                    atbRoboAliado = futbolEnvironment->rbtRoboAliado[i]->atbRetornaRobo();
                }

                float dist = 0;
                for(int n=0; n < iTamanhoBufferRobo-1; ++n)
                {
                    dist += futbolEnvironment->vtBufferPosRobosAliadosVisao[i].at(n).distanceToPoint(futbolEnvironment->vtBufferPosRobosAliadosVisao[i].at(n+1));
                }
                atbRoboAliado.dVelocidade  = dist/(iTamanhoBufferRobo-1) * /*dFPS*/60 * 1e-3;

                atbRoboAliado.vt2dVelocidade = atbRoboAliado.vt2dPosicaoAtual - atbRoboAliado.vt2dPosicaoAnterior;
                atbRoboAliado.vt2dVelocidade.normalize();
                atbRoboAliado.vt2dVelocidade *= atbRoboAliado.dVelocidade;
//                atbRoboAliado.vt2dVelocidade = decompoeVelocidadeRobo(atbRoboAliado.vt2dVelocidade, atbRoboAliado);

                futbolEnvironment->rbtRoboAliado[i]->vSetaRobo(atbRoboAliado);
            }

            futbolEnvironment->vtBufferRoboAliadosKALMAN[i].prepend(vt2dKalmanRobos);

            if(futbolEnvironment->vtBufferRoboAliadosKALMAN[i].size() > iTamanhoBufferRobo)
                futbolEnvironment->vtBufferRoboAliadosKALMAN[i].pop_back();
        }

        for(int i=0;i<PLAYERS_PER_SIDE;i++)
        {

            atbRoboAliado = futbolEnvironment->rbtRoboAdversario[i]->atbRetornaRobo();

            if(futbolEnvironment->vtBufferPosRobosAdversariosVisao[i].size() >= iTamanhoBufferRobo && atbRoboAliado.bEmCampo)
            {
                vt2dKalmanRobos = futbolEnvironment->vtBufferPosRobosAdversariosVisao[i].constFirst();//vt2dKALMANRobo(futbolEnvironment->vtBufferPosRobosAdversariosVisao[i]);

                futbolEnvironment->rbtRoboAdversario[i]->vSetaPosicao(vt2dKalmanRobos);
                atbRoboAliado = futbolEnvironment->rbtRoboAdversario[i]->atbRetornaRobo();

                if((atbRoboAliado.vt2dPosicaoAtual.distanceToPoint(atbRoboAliado.vt2dPosicaoAnterior)) > 1)
                {
                    atbRoboAliado.dVelocidade = (atbRoboAliado.vt2dPosicaoAtual.distanceToPoint(atbRoboAliado.vt2dPosicaoAnterior)/1e3) * dFPS;
                    atbRoboAliado.vt2dVelocidade = (atbRoboAliado.vt2dPosicaoAtual - atbRoboAliado.vt2dPosicaoAnterior)*1e-3f*dFPS;
                    atbRoboAliado.vt2dVelocidade = decompoeVelocidadeRobo(atbRoboAliado.vt2dVelocidade, atbRoboAliado);
                }
                else
                {
                    atbRoboAliado.vt2dVelocidade = QVector2D(0,0);
                }

                futbolEnvironment->rbtRoboAdversario[i]->vSetaRobo(atbRoboAliado);
            }

            futbolEnvironment->vtBufferRoboAdversariosKALMAN[i].prepend(vt2dKalmanRobos);

            if(futbolEnvironment->vtBufferRoboAdversariosKALMAN[i].size() > iTamanhoBufferRobo)
                futbolEnvironment->vtBufferRoboAdversariosKALMAN[i].pop_back();
        }

        // Kalman para bola
        QVector2D vt2dKalmanBola;

        //Atualiza a posicao da bola com o filtro de kalman
        if(futbolEnvironment->vtBufferPosBolaVisao.size() >= iTamanhoBufferBola)
        {
            vt2dKalmanBola = futbolEnvironment->vtBufferPosBolaVisao.constFirst();//vt2dKalmanBola = vt2dKALMANBola(futbolEnvironment->vtBufferPosBolaVisao);
            futbolEnvironment->vSetaPosicaoBola(vt2dKalmanBola);//Esta funcao ja atribui a posicao atual na anterior e a atual na atual
            futbolEnvironment->vtBufferBolaKALMAN.prepend(vt2dKalmanBola);

            float fVelocidadeAuxiliar=0;
            fVelocidadeAuxiliar = ((futbolEnvironment->vt2dPosicaoAtualBola().distanceToPoint(
                                    futbolEnvironment->vt2dPosicaoAnteriorBola())/1e6) * dFPS);
            if(fVelocidadeAuxiliar < 10)//So atualiza a velocida para um aumento menor que 10 m/s
                futbolEnvironment->vSetaVelocidadeBola(fVelocidadeAuxiliar);
        }




        if(futbolEnvironment->vtBufferBolaKALMAN.size() > iTamanhoBufferBola)
            futbolEnvironment->vtBufferBolaKALMAN.pop_back();
    }
}

void systemidentification::vDetectRobot(SSL_DetectionFrame frameDeteccao)
{
    Bola blBola;
    Atributos atbAtributosRobo;

    //Se o objeto esta na mesma camera e ja se passou o numero de frames para sair da visao, consideramos que ele esta fora de campo

    futbolEnvironment->vPegaBola(blBola);

    if(frameDeteccao.frame_number() > (blBola.iCameraFrame[blBola.iCameraBola] + iFramesParaSairDaVisao))
    {
        blBola.bEmCampo = false;
        futbolEnvironment->vSetaBola(&blBola);
    }

    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        atbAtributosRobo = futbolEnvironment->rbtRoboAliado[n]->atbRetornaRobo();

        //So compara se o frame atual é da camera que viu o robo por ultimo
        if(frameDeteccao.camera_id() == atbAtributosRobo.iCameraRobo)
        {
            if(frameDeteccao.frame_number() > (atbAtributosRobo.iCameraFrame[atbAtributosRobo.iCameraRobo] + iFramesParaSairDaVisao))
            {
                atbAtributosRobo.bEmCampo = false;
                atbAtributosRobo.jogadaAtual = Nenhuma;
                futbolEnvironment->rbtRoboAliado[n]->vSetaRobo(atbAtributosRobo);
                futbolEnvironment->vSetaRobosEmCampo(0);
            }
        }

        atbAtributosRobo = futbolEnvironment->rbtRoboAdversario[n]->atbRetornaRobo();

        if(frameDeteccao.camera_id() == atbAtributosRobo.iCameraRobo)
        {
            if(frameDeteccao.frame_number() > (atbAtributosRobo.iCameraFrame[atbAtributosRobo.iCameraRobo] + iFramesParaSairDaVisao))
            {
                atbAtributosRobo.bEmCampo = false;
                futbolEnvironment->rbtRoboAdversario[n]->vSetaRobo(atbAtributosRobo);
            }
        }
    }
}

void systemidentification::vUpdateBall(SSL_DetectionFrame frameDeteccao, int nBola)
{
    const SSL_DetectionBall & bola = frameDeteccao.balls(nBola);
    Bola blBola;
    futbolEnvironment->vPegaBola(blBola);

    //Atualiza se a bola esta no campo e o seu frame number e salva a posicao recebida no vetor que sera usado no kalman
    blBola.bEmCampo = true;

    blBola.iCameraBola = frameDeteccao.camera_id();
    blBola.iCameraFrame[blBola.iCameraBola] = frameDeteccao.frame_number();

    //Buffer da bola
    futbolEnvironment->vtBufferPosBolaVisao.prepend(QVector2D(qCeil(bola.x()), qCeil(bola.y())));
    if(futbolEnvironment->vtBufferPosBolaVisao.size() > iTamanhoBufferBola)
    {
        futbolEnvironment->vtBufferPosBolaVisao.pop_back();
    }

    futbolEnvironment->vSetaBola(&blBola);
}

void systemidentification::vUpdateAlly(SSL_DetectionFrame frameDeteccao, Time timeAtual, int nRobo)
{
    SSL_DetectionRobot roboAliado;
    timeAtual == timeAzul ? roboAliado = frameDeteccao.robots_blue(nRobo) : roboAliado = frameDeteccao.robots_yellow(nRobo);

    Atributos atbAtributosRobo; atbAtributosRobo.id = roboAliado.robot_id();

    if(atbAtributosRobo.id < PLAYERS_PER_SIDE)
    {

        atbAtributosRobo = futbolEnvironment->rbtRoboAliado[atbAtributosRobo.id]->atbRetornaRobo();


        futbolEnvironment->vSetaRobosEmCampo(futbolEnvironment->iPegaRobosEmCampo()+1);

        //Usar angulo de -inf a +inf
        float anguloVisao = roboAliado.orientation(), anguloAtualReduzido = atbAtributosRobo.rotation;
        if(anguloVisao < 0)// [-180;180] -> [0,360]
            anguloVisao += 2*M_PI;

        if(qAbs(anguloAtualReduzido) > 2*M_PI)
        {
            float fatorConversao = anguloAtualReduzido/(2*M_PI) - trunc(anguloAtualReduzido/(2*M_PI));
            anguloAtualReduzido = fatorConversao * 2*M_PI;
        }

        QVector2D vt2dAnguloVisao(qCos(anguloVisao)        , qSin(anguloVisao)         ),
                  vt2dAnguloAtual(qCos(anguloAtualReduzido), qSin(anguloAtualReduzido) );

        //Calcula a variacao de angulo em graus
        float anguloIncremento = clsAuxiliar::fAnguloVetor1Vetor2(vt2dAnguloVisao, vt2dAnguloAtual)*M_PI/180;

        //Se o sinal do anguloVisao e do anguloReduzido forem diferentes, transforma o anguloReduzido para um angulo positivo
        if(qRound(anguloAtualReduzido/qAbs(anguloAtualReduzido)) != qRound(anguloVisao/qAbs(anguloVisao)))
            anguloAtualReduzido += 2*M_PI;

        //Multiplica o incremento pelo sinal da diferença entre os angulos, isso fara com que o incremento seja somado caso anguloVisao > anguloReduzido, ou
        //seja subtraido caso anguloVisao < anguloReduzido
        anguloIncremento *= qRound((anguloVisao - anguloAtualReduzido)/qAbs(anguloVisao - anguloAtualReduzido));

        //Se houve troca de quadrante do angulo visao do 1 para o 4 ou 4 para o 1, devemos inverter o sinal
        if(clsAuxiliar::iCalculaQuadrante360(anguloVisao) == 4 && clsAuxiliar::iCalculaQuadrante360(anguloAtualReduzido) == 1)
            anguloIncremento *= -1;
        else if(clsAuxiliar::iCalculaQuadrante360(anguloVisao) == 1 && clsAuxiliar::iCalculaQuadrante360(anguloAtualReduzido) == 4)
            anguloIncremento *= -1;

        atbAtributosRobo.dVelocidadeAngular = anguloIncremento*dFPS;

        atbAtributosRobo.rotation += anguloIncremento;

//        if(atbAtributosRobo.id == ui->robotID->value())
//            qDebug()<< "Angulo Atual = "    << atbAtributosRobo.rotation*180/M_PI << "Angulo Visao = " << anguloVisao*180/M_PI
//                    << "Angulo Atual Reduzido = " << anguloAtualReduzido*180/M_PI << "Angulo Incremento = " << anguloIncremento*180/M_PI;

        atbAtributosRobo.bEmCampo = true;
        atbAtributosRobo.iCameraRobo = frameDeteccao.camera_id();
        atbAtributosRobo.iCameraFrame[atbAtributosRobo.iCameraRobo] = frameDeteccao.frame_number();

        futbolEnvironment->rbtRoboAliado[atbAtributosRobo.id]->vSetaRobo(atbAtributosRobo);

        //Buffer utilizado no calculo do kalman
        futbolEnvironment->vtBufferPosRobosAliadosVisao[atbAtributosRobo.id].prepend(QVector2D(qCeil(roboAliado.x()), qCeil(roboAliado.y())));

        if(futbolEnvironment->vtBufferPosRobosAliadosVisao[atbAtributosRobo.id].size() > iTamanhoBufferRobo)
            futbolEnvironment->vtBufferPosRobosAliadosVisao[atbAtributosRobo.id].pop_back();

    }
}

void systemidentification::vUpdateOpponent(SSL_DetectionFrame frameDeteccao, Time timeAtual, int nRobo)
{
    SSL_DetectionRobot roboAdversario;
    //Se o nosso time é o azul devemos pegar os robos amarelos e vice-versa
    timeAtual == timeAzul ? roboAdversario = frameDeteccao.robots_yellow(nRobo) : roboAdversario = frameDeteccao.robots_blue(nRobo);

    Atributos atbAtributosRobo; atbAtributosRobo.id = roboAdversario.robot_id();

    if(atbAtributosRobo.id < PLAYERS_PER_SIDE)
    {
        atbAtributosRobo = futbolEnvironment->rbtRoboAdversario[atbAtributosRobo.id]->atbRetornaRobo();


        atbAtributosRobo.rotation = roboAdversario.orientation();
        atbAtributosRobo.bEmCampo = true;
        atbAtributosRobo.iCameraRobo = frameDeteccao.camera_id();
        atbAtributosRobo.iCameraFrame[atbAtributosRobo.iCameraRobo] = frameDeteccao.frame_number();

        //futbolEnvironment->rbtRoboAdversario[atbAtributosRobo.id]->vSetaRobo(atbAtributosRobo);

        //Buffer utilizado no calculo do kalman
        futbolEnvironment->vtBufferPosRobosAdversariosVisao[atbAtributosRobo.id].prepend(QVector2D(qCeil(roboAdversario.x()), qCeil(roboAdversario.y())));

        if(futbolEnvironment->vtBufferPosRobosAdversariosVisao[atbAtributosRobo.id].size() > iTamanhoBufferRobo)
            futbolEnvironment->vtBufferPosRobosAdversariosVisao[atbAtributosRobo.id].pop_back();


    }
}

void systemidentification::vUpdateGeometry(SSL_GeometryData frameGeometria, bool &tamanhoCampoMudou)
{
    const SSL_GeometryFieldSize& geometriaCampo = frameGeometria.field();
    const google::protobuf::RepeatedPtrField<SSL_FieldCicularArc>& arcosCampo = geometriaCampo.field_arcs();
    const google::protobuf::RepeatedPtrField<SSL_FieldLineSegment>& retasCampo = geometriaCampo.field_lines();

    SSL_FieldCicularArc circulosCampo[arcosCampo.size()];
    SSL_FieldLineSegment linhasCampo[retasCampo.size()];

    //Recebe os circulos do campo
    for(int n=0; n < arcosCampo.size(); ++n)
    {
        circulosCampo[n] = arcosCampo.Get(n);

        if(circulosCampo[n].name() == "CenterCircle")
            futbolEnvironment->vSetaRaioCirculoCentroCampo(circulosCampo[n].radius());
    }

    //Recebe linhas do campo
    for(int n=0; n < retasCampo.size(); ++n)
    {
        linhasCampo[n] = retasCampo.Get(n);

        //Dimensoes da area do penalty
        if(linhasCampo[n].name() == "LeftPenaltyStretch" || linhasCampo[n].name() == "RightPenaltyStretch")
            futbolEnvironment->vSetaLarguraAreaPenalty( QVector2D(linhasCampo[n].p1().x(), linhasCampo[n].p1().y()).distanceToPoint(
                                                                                 QVector2D(linhasCampo[n].p2().x(), linhasCampo[n].p2().y())) );

        else if(linhasCampo[n].name() == "LeftFieldLeftPenaltyStretch" || linhasCampo[n].name() == "LeftFieldRightPenaltyStretch" ||
                linhasCampo[n].name() == "RightFieldRightPenaltyStretch" || linhasCampo[n].name() == "RightFieldLeftPenaltyStretch" )
            futbolEnvironment->vSetaProfundidadeAreaPenalty( QVector2D(linhasCampo[n].p1().x(), linhasCampo[n].p1().y()).distanceToPoint(
                                                                                 QVector2D(linhasCampo[n].p2().x(), linhasCampo[n].p2().y())) );
    }

    //Dimensoes do gol
    futbolEnvironment->vSetaLarguraGol(geometriaCampo.goal_width());
    futbolEnvironment->vSetaProfundidadeGol(geometriaCampo.goal_depth());

    if(futbolEnvironment->vt2dPegaTamanhoCampo() != QVector2D(geometriaCampo.field_length(), geometriaCampo.field_width()))
    {
        tamanhoCampoMudou = true;
        futbolEnvironment->vSetaTamanhoCampo(QVector2D(geometriaCampo.field_length(), geometriaCampo.field_width()));
    }
    //Atualiza o tamanho do campo

    //Assim que chega o pacote de geometria habilita os botoes de comecar jogo e comecar teste
    if(!ui->startButton->isEnabled())
        ui->startButton->setEnabled(true);
}

QString systemidentification::strCheckMatlabRuntimeFolder()
{
    QString warningString = "";

    if(matlabRuntDir.isEmpty())
    {
        warningString = "Warning! MATLAB Runtime folder undefined!";
    }
    else if(!QDir(matlabRuntDir + "/runtime").exists())
    {
        warningString = "The selected MATLAB Runtime folder doesn't contain the MATLAB Runtime. Please select the correct folder!";
    }
    return warningString;
}

QString systemidentification::strCheckMatlabAppFolder()
{
    QString warningString = "";

    if(matlabAppDir.isEmpty())
    {
        warningString = "Warning! App folder undefined!";
    }
    else if(!matlabAppDir.contains("ModelIdentificationApp.sh"))
    {
        warningString = "The selected app script isn't correct. Please select the correct script!";
    }
    return warningString;
}

void systemidentification::vConnectNetwork()
{
    ui->connectVision->setText(QStringLiteral("Disconnect"));

    QList<QNetworkInterface> ifInterfaceLocal = QNetworkInterface::allInterfaces();

    QHostAddress haEnderecoGrupoVisao(ui->visionIP->text());
    udpSSLVision = new QUdpSocket(this);
    udpSSLVision->bind(QHostAddress::AnyIPv4, ui->visionPort->text().toInt(), QUdpSocket::ShareAddress);
    udpSSLVision->joinMulticastGroup(haEnderecoGrupoVisao,
                                     ifInterfaceLocal.at(ui->networkInterfaceBox->currentIndex()));

    connect(udpSSLVision, &QUdpSocket::readyRead, this, &systemidentification::vProcessVision);

    ui->connectVision->disconnect(SIGNAL(clicked()));
    connect(ui->connectVision, &QPushButton::clicked, this, &systemidentification::vDisconnectNetwork);

    udpGrsim = new QUdpSocket(this);
    udpGrsim->bind(QHostAddress::AnyIPv4, 20011, QUdpSocket::ShareAddress);
    udpGrsim->setMulticastInterface(ifInterfaceLocal.at(ui->networkInterfaceBox->currentIndex()));
    udpGrsim->joinMulticastGroup(QHostAddress("127.0.0.1"));

    if(grsimSimulator == nullptr)
        grsimSimulator = new RoboFei_Simulator(udpGrsim, QHostAddress("127.0.0.1"), 20011);

    ui->startButton->setEnabled(true);
    ui->stopButton->setEnabled(true);
}

void systemidentification::vDisconnectNetwork()
{
    ui->connectVision->setText("Connect");

    udpSSLVision->disconnect(SIGNAL(readyRead() ) );
    udpSSLVision->close();

    udpGrsim->close();

    ui->connectVision->disconnect(SIGNAL(clicked()));
    connect(ui->connectVision, &QPushButton::clicked, this, &systemidentification::vConnectNetwork);

    ui->startButton->setEnabled(false);
    ui->stopButton->setEnabled(false);
}

void systemidentification::vProcessVision()
{
    while(udpSSLVision->hasPendingDatagrams())
    {
        QByteArray datagram;
        datagram.resize(udpSSLVision->pendingDatagramSize());
        datagram.fill(0, udpSSLVision->pendingDatagramSize());
        udpSSLVision->readDatagram(datagram.data(), datagram.size());

        SSL_WrapperPacket sslvision;
        sslvision.ParseFromArray(datagram, datagram.size());

        RoboController.addVision(sslvision);

        vUpdateField();
    }
}

void systemidentification::vConnectSerialPort(const ConfiguracaoRadio &ConfiguracoesRadio)
{
    rdRadio->setConfig(ConfiguracoesRadio);

    if(ConfiguracoesRadio.bConectar == true)
    {
        bool bRadioConectado = rdRadio->configureSerialPort();

        if(bRadioConectado == false)
            vHandleRadioError(rdRadio->serial->errorString());
        else
        {
            vHandleRadioError(QStringLiteral("Connected"));
        }
    }
    else
    {
        rdRadio->closePort();
        vHandleRadioError(QStringLiteral("Disconnected"));
    }
}

void systemidentification::vHandleRadioError(const QString &strErro)
{
    ui->statusBar->showMessage(strErro, 5000);//Mostra o erro durante 5 segundos

    if(strErro == QLatin1String("Connected"))
    {
        disconnect(ui->connectRadio, SIGNAL(clicked()), nullptr, nullptr );
        connect(ui->connectRadio, &QPushButton::clicked, this, &systemidentification::vDisconnectRadio);
        ui->connectRadio->setText(QStringLiteral("Disconnect"));
    }
    else if(strErro == QLatin1String("Disconnected"))
    {
        disconnect(ui->connectRadio, SIGNAL(clicked()), nullptr, nullptr );
        connect(ui->connectRadio, &QPushButton::clicked, this, &systemidentification::vConnectRadio );
        ui->connectRadio->setText(QStringLiteral("Connect"));
    }
}

void systemidentification::vHandlePacketError(ErrorCode erro)
{
    switch (erro)
    {

    case CHECKSUM_ERROR:
    {
       ui->statusBar->showMessage(QStringLiteral("CHECKSUM ERROR"), 400);
    }break;

    case DELIMITER_NOT_FOUND:
    {
        ui->statusBar->showMessage(QStringLiteral("DELIMITER NOT FOUND ERROR"), 400);
    }break;

    case PACKET_INCOMPLETE:
    {
        ui->statusBar->showMessage(QStringLiteral("PACKET INCOMPLETE ERROR"), 400);
    }break;

    case OTHER_RADIO_PCKT:
    {
        ui->statusBar->showMessage(QStringLiteral("OTHER RADIO PACKET ERROR"), 400);
    }break;

    case SAFETY_THRESHOLD_ERROR:
    {
        ui->statusBar->showMessage(QStringLiteral("SAFETY THRESHOLD ERROR"), 400);
    }break;

    default:
        break;
    }
}

void systemidentification::vUpdateSerialPorts()
{
    ui->serialPortBox->clear();

    for(const QSerialPortInfo &info : QSerialPortInfo::availablePorts())
    {
        QStringList list;
        list << info.portName();
        ui->serialPortBox->addItem(list.first(), list);
    }
}

void systemidentification::vConnectRadio()
{
    ConfiguracaoRadio configRadio;

    configRadio.bConectar = true;
    configRadio.Porta = ui->serialPortBox->currentText();
    configRadio.baudRate = static_cast<QSerialPort::BaudRate>(ui->baudRateBox->itemData(ui->baudRateBox->currentIndex()).toInt());
    configRadio.dataBits = QSerialPort::DataBits::Data8;
    configRadio.flowControl = QSerialPort::FlowControl::NoFlowControl;
    configRadio.parity = QSerialPort::Parity::NoParity;
    configRadio.stopBits = QSerialPort::StopBits::OneStop;
    configRadio.localEchoEnabled = false;

    vConnectSerialPort(configRadio);
}

void systemidentification::vDisconnectRadio()
{
    ConfiguracaoRadio configRadio;

    configRadio.bConectar = false;

    vConnectSerialPort(configRadio);
}

void systemidentification::vGetSerialPortData()
{
    QByteArray PacoteRecebido;
    ErrorCode erro;

    erro = rdRadio->readData(PacoteRecebido);

    if(erro != NOPCKT_ERROR)//Ocorreu um erro no pacote
    {
        PacoteRecebido.clear();

        vHandlePacketError(erro);

        return;
    }


    //Atribuicao dos valores dos sensores e nivel de bateria
    Atributos atbAtributoRobo;
    for(int n=0; n < PLAYERS_PER_SIDE; ++n)
    {
        atbAtributoRobo = futbolEnvironment->rbtRoboAliado[n]->atbRetornaRobo();
        atbAtributoRobo.kickSensor = rdRadio->kick_sensor[n];
        atbAtributoRobo.battery = rdRadio->battery_level[n];

        if(atbAtributoRobo.battery > 50 || atbAtributoRobo.battery < 0)
            atbAtributoRobo.battery = 0;

        if(rdRadio->measured_1[n].size() > 0)
        {
            atbAtributoRobo.dOdometriaRoda1.append(rdRadio->measured_1[n].last());
            atbAtributoRobo.dOdometriaRoda2.append(rdRadio->measured_2[n].last());
            atbAtributoRobo.dOdometriaRoda3.append(rdRadio->measured_3[n].last());
            atbAtributoRobo.dOdometriaRoda4.append(rdRadio->measured_4[n].last());
        }

        futbolEnvironment->rbtRoboAliado[n]->vSetaRobo(atbAtributoRobo);
    }


}

void systemidentification::vConfigurePlots(QString _velocidade, QString _unidade, QCustomPlot *customPlot)
{
    // add two new graphs and set their look:
    customPlot->addGraph();
    customPlot->graph(0)->setPen(QPen(Qt::blue)); // line color blue for first graph
    customPlot->graph(0)->setName( _velocidade + " sent " + _unidade);
    customPlot->addGraph();
    customPlot->graph(1)->setPen(QPen(Qt::red)); // line color red for second graph
    customPlot->graph(1)->setName( _velocidade + " measured " + _unidade);

    // set some pens, brushes and backgrounds:
    customPlot->xAxis->setBasePen(QPen(Qt::white, 1));
    customPlot->yAxis->setBasePen(QPen(Qt::white, 1));

    customPlot->xAxis->setTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setTickPen(QPen(Qt::white, 1));

    customPlot->xAxis->setTickLabelColor(Qt::white);
    customPlot->yAxis->setTickLabelColor(Qt::white);

    customPlot->xAxis->setSubTickPen(QPen(Qt::white, 1));
    customPlot->yAxis->setSubTickPen(QPen(Qt::white, 1));

    customPlot->xAxis->setLabelColor(Qt::white);
    customPlot->yAxis->setLabelColor(Qt::white);

    customPlot->xAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);
    customPlot->yAxis->setUpperEnding(QCPLineEnding::esSpikeArrow);

    QLinearGradient plotGradient;
    plotGradient.setStart(0, 0);
    plotGradient.setFinalStop(0, 350);
    plotGradient.setColorAt(0, QColor(80, 80, 80));
    plotGradient.setColorAt(1, QColor(50, 50, 50));
    customPlot->setBackground(plotGradient);


    // configure axis
    customPlot->xAxis->setLabel("Time [s]");
    customPlot->yAxis->setLabel("Velocity " + _unidade);

    // enable legends
    customPlot->legend->setVisible(true);
    customPlot->legend->setBrush(QColor(80, 80, 80));
    customPlot->legend->setTextColor(Qt::white);


    // Note: we could have also just called customPlot->rescaleAxes(); instead
    // Allow user to drag axis ranges with mouse, zoom with mouse wheel and select graphs by clicking:
    customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectPlottables);
}

void systemidentification::vUpdatePlots()
{
    QVector3D velRobo = futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->vt3dPegaVelocidadeRobo();

    double outX = dCurrentOutputX, outY = dCurrentOutputY, outW = dCurrentOutputW;
    double tempo = 0;

    if(ui->radCampoReal->isChecked())
    {
        outX *= dVelocidadeMaximaRobo/100;
        outY *= dVelocidadeMaximaRobo/100;
        outW *= dRotacaoMaximoRobo/100;
    }

    if((ui->signalPRBS->isChecked() || ui->signalGBN->isChecked()) && tmrSampleData->isActive())
    {
        tempo = timestampTimer.nsecsElapsed()/1e9;
    }
    else if(ui->signalStep->isChecked() && tmrSampleData->isActive())
    {
        tempo = dTStep;

        if(velRobo.x()/(0.632*ui->STEPlinearAmplitudeBox->value()) >= 1 && ui->labelTminXValue->text() == "0 ms")
        {
            ui->labelTminXValue->setText( QString::number(dTStep*1e3-ui->STEPriseInstantBox->value()) + " ms");
        }

        if(velRobo.y()/(0.632*ui->STEPlinearAmplitudeBox->value()) >= 1 && ui->labelTminYValue->text() == "0 ms")
        {
            ui->labelTminYValue->setText( QString::number(dTStep*1e3-ui->STEPriseInstantBox->value()) + " ms");
        }

        if(velRobo.z()/(0.632*ui->STEPangularAmplitudeBox->value()) >= 1 && ui->labelTminWValue->text() == "0 ms")
        {
            ui->labelTminWValue->setText( QString::number(dTStep*1e3-ui->STEPriseInstantBox->value()) + " ms");
        }
    }
    else if(ui->signalSine->isChecked() && tmrSampleData->isActive())
    {
        tempo = dTSine;
    }

    if((outCurrentOutput == X && ui->individualExcitation->isChecked() ) || ui->simultaneousExcitation->isChecked())
    {
        ui->qcpPlotX->graph(0)->addData(tempo, outX);
        ui->qcpPlotX->graph(1)->addData(tempo, velRobo.x());
    }

    if((outCurrentOutput == Y && ui->individualExcitation->isChecked() ) || ui->simultaneousExcitation->isChecked())
    {
        ui->qcpPlotY->graph(0)->addData(tempo, outY);
        ui->qcpPlotY->graph(1)->addData(tempo, velRobo.y());
    }

    if((outCurrentOutput == W && ui->individualExcitation->isChecked() ) || ui->simultaneousExcitation->isChecked())
    {
        ui->qcpPlotW->graph(0)->addData(tempo, outW);
        ui->qcpPlotW->graph(1)->addData(tempo, velRobo.z());
    }

    ui->qcpPlotX->rescaleAxes();
    ui->qcpPlotY->rescaleAxes();
    ui->qcpPlotW->rescaleAxes();

    ui->qcpPlotX->replot();
    ui->qcpPlotY->replot();
    ui->qcpPlotW->replot();
}

void systemidentification::vResetPlots()
{
    // Reset data
    QVector<double> auxData;
    auxData.clear();

    ui->qcpPlotX->graph(0)->setData(auxData, auxData);
    ui->qcpPlotX->graph(1)->setData(auxData, auxData);

    ui->qcpPlotY->graph(0)->setData(auxData, auxData);
    ui->qcpPlotY->graph(1)->setData(auxData, auxData);

    ui->qcpPlotW->graph(0)->setData(auxData, auxData);
    ui->qcpPlotW->graph(1)->setData(auxData, auxData);
}

void systemidentification::vSaveData()
{
    tmrSampleData->stop();
    QMessageBox msgBox;
    msgBox.setText("Finished data acquisition.");
    msgBox.setInformativeText("Would you like to save?");
    msgBox.setStandardButtons(QMessageBox::Save | QMessageBox::Cancel);
    msgBox.setDefaultButton(QMessageBox::Save);

    switch (msgBox.exec())
    {
      case QMessageBox::Save:
          on_saveButton_clicked();
          break;
      case QMessageBox::Cancel:
          break;
      default:
          // should never be reached
          break;
    }
}

void systemidentification::vOpenFiles(QString fileName, QFile &file)
{
    if(!file.isOpen())
    {
        if(fileName != "")
        {
            file.setFileName(fileName);
            file.open(QIODevice::ReadOnly | QIODevice::Text | QIODevice::ReadWrite);
        }
    }
}

void systemidentification::vSaveData(QString dados, QFile &file)
{
    if(file.isOpen())
    {
        file.flush();

        QTextStream stream(&file);

        stream << dados;
        file.close();
    }
}

void systemidentification::vFillSignalParameters(QString path)
{
    QFile fileParameters;

    QString parametros = "", signal = "None", linAmp = "", angAmp = "", extraParam = "", linUni = " m/s", angUni = " rad/s";

    ui->radCampoReal->isChecked() ? linUni = angUni = " %" : linUni;

    if(ui->signalPRBS->isChecked())
    {
        signal = "PRBS";
        linAmp = QString::number(ui->PRBSlinearAmplitudeBox->value());
        angAmp = QString::number(ui->PRBSangularAmplitudeBox->value());
        extraParam = "Switching time: "    + QString::number(ui->PRBSswitchingTimeBox->value())  + " ms" + "\n" +
                     "Linear Offset: "  + QString::number(ui->PRBSbaseValueLinearBox->value())   + linUni + "\n" +
                     "Angular Offset: " + QString::number(ui->PRBSbaseValueAngularBox->value())  + angUni + "\n" ;
    }
    else if(ui->signalGBN->isChecked())
    {
        signal = "GBN";
        linAmp = QString::number(ui->GBNlinearAmplitudeBox->value());
        angAmp = QString::number(ui->GBNangularAmplitudeBox->value());
        extraParam = "Probability X: "  + QString::number(ui->GBNprobabilityBoxX->value())      + "\n" +
                     "Probability Y: "  + QString::number(ui->GBNprobabilityBoxY->value())      + "\n" +
                     "Probability W: "  + QString::number(ui->GBNprobabilityBoxW->value())      + "\n" +
                     "Linear Offset: "  + QString::number(ui->GBNlinearBaseValueBox->value())  + linUni + "\n" +
                     "Angular Offset: " + QString::number(ui->GBNangularBaseValueBox->value()) + angUni + "\n" ;
    }
    else if(ui->signalStep->isChecked())
    {
        signal = "Step";
        linAmp = QString::number(ui->STEPlinearAmplitudeBox->value());
        angAmp = QString::number(ui->STEPangularAmplitudeBox->value());
        extraParam = "Initial Position(x,y): (" + QString::number(vt2dInitialPosition.x()) + "," + QString::number(vt2dInitialPosition.y()) + ")" + "\n";
    }
    else if(ui->signalSine->isChecked())
    {
        signal = "Sine";
        linAmp = QString::number(ui->SineLinearAmplitudeBox->value());
        angAmp = QString::number(ui->SineAngularAmplitudeBox->value());
        extraParam = "Angular Frequency: " + QString::number(ui->SineAngularFrequency->value()) + " rad/s";
    }

    parametros = "Signal Type: "       + signal + "\n" +
                 "Linear Amplitude: "  + linAmp + linUni + "\n" +
                 "Angular Amplitude: " + angAmp + angUni + "\n" +
                 extraParam;

    vOpenFiles(path + "DataParameters.txt", fileParameters);
    vSaveData(parametros, fileParameters);
}

void systemidentification::on_startButton_clicked()
{
    ui->saveButton->setEnabled(false);

//    dataX = dataY = "Time(s);Vsent(m/s);VMeasured(m/s);PosX(m);PosY(m)\n";
//    dataW = "Time(s);Vsent(rad/s);VMeasured(rad/s);Rotation(rad)\n";
    dataX = dataY = "Time(s);Vsent(m/s);VMeasured(m/s);PosX(m);PosY(m);Rotation(rad)\n";
    dataW = "Time(s);Vsent(rad/s);VMeasured(rad/s);PosX(m);PosY(m);Rotation(rad)\n";

    vResetPlots();
    tempoAmostragem = ui->samplingTime->value();
    tmrSampleData->start(tempoAmostragem);
    timestampTimer.restart();
    etmTeste.restart();

    if(ui->signalPRBS->isChecked())
    {
        ui->pagePRBSParameters->setEnabled(false);

        outCurrentOutput = X;
        counterInterfaceLED = 0;

        prbsGenX->startPRBS();
        prbsGenY->startPRBS();
        prbsGenW->startPRBS();

        if(ui->PRBSswitchingTimeBox->value() > 0)
        {
            tmrTimerPRBS->start(ui->PRBSswitchingTimeBox->value());
            tmrUpdateRobot->start(15);
            ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Running");
        }
        else
            ui->statusBar->showMessage("You must use a switching time greater than 0", 2000);
    }
    else if(ui->signalStep->isChecked())
    {
        ui->pageStepParameter->setEnabled(false);

        vt2dInitialPosition = futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->vt2dPosicaoAtualRobo();
        outCurrentOutput = X;
        counterInterfaceLED = 0;

        ui->labelTminXValue->setText("0 ms");
        ui->labelTminYValue->setText("0 ms");
        ui->labelTminWValue->setText("0 ms");

        dTStep = 0;
        tmrTimerStep->start(tempoAtualizacaoStep);
        futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->vSetaDestino(QVector2D(1000,600));
        tmrUpdateRobot->start(15);
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Running");
    }
    else if(ui->signalGBN->isChecked())
    {
        ui->pageGBNParameters->setEnabled(false);

        outCurrentOutput = X;
        counterInterfaceLED = 0;

        double probX = ui->GBNprobabilityBoxX->value(),
               probY = ui->GBNprobabilityBoxY->value(),
               probW = ui->GBNprobabilityBoxW->value();
        int nSamples = 60/(ui->GBNSwitchingTimeBox->value()/1e3);

        gbnGenX->startGBN(nSamples, probX);
        gbnGenY->startGBN(nSamples, probY);
        gbnGenW->startGBN(nSamples, probW);

        tmrTimerGBN->start(ui->GBNSwitchingTimeBox->value());
        tmrUpdateRobot->start(15);
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Running");
    }
    else if(ui->signalSine->isChecked())
    {
        ui->pageSineWaveParameters->setEnabled(false);

        outCurrentOutput = X;
        counterInterfaceLED = 0;

        dTSine = 0;
        tmrTimerSine->start(tempoAtualizacaoSeno);
        tmrUpdateRobot->start(15);
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Running");
    }
}

void systemidentification::vGetPRBSValue()
{
    if(ui->individualExcitation->isChecked())
    {
        vGetIndividualPRBS();
    }
    else if(ui->simultaneousExcitation->isChecked())
    {
        vGetSimultaneousPRBS();
    }
}

void systemidentification::vGetStepValue()
{
    if(ui->individualExcitation->isChecked())
    {
        vGetIndividualSTEP();
    }
    else if(ui->simultaneousExcitation->isChecked())
    {
        vGetSimultaneousSTEP();
    }


}

void systemidentification::vGetGBNValue()
{
    if(ui->individualExcitation->isChecked())
    {
        vGetIndividualGBN();
    }
    else if(ui->simultaneousExcitation->isChecked())
    {
        vGetSimultaneousGBN();
    }
}

void systemidentification::vGetSineValue()
{
    double linearAmp = ui->SineLinearAmplitudeBox->value();
    double angularAmp = ui->SineAngularAmplitudeBox->value();

    dTSine += tempoAtualizacaoSeno/1e3;

    switch (outCurrentOutput)
    {
    case X:
    {
        dCurrentOutputX = dGetSineValue(linearAmp);
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;

        if(dCurrentOutputX <= -900)
        {
            outCurrentOutput = Y;
            dTSine = 0;
            timestampTimer.restart();
        }
    }break;

    case Y:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = dGetSineValue(linearAmp);
        dCurrentOutputW = 0;

        if(dCurrentOutputY <= -900)
        {
            outCurrentOutput = W;
            dTSine = 0;
            timestampTimer.restart();
        }
    }break;

    case W:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = dGetSineValue(angularAmp);

        if(dCurrentOutputW <= -900)
        {
            outCurrentOutput = None;

            tmrUpdateRobot->stop();
            tmrTimerSine->stop();

            dCurrentOutputX = 0;
            dCurrentOutputY = 0;
            dCurrentOutputW = 0;

            ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished Sine Wave Response");
            ui->pageSineWaveParameters->setEnabled(true);
            ui->saveButton->setEnabled(true);
            dTSine = 0;

            vSaveData();
        }
    }break;

    default://None
    {
    }
        break;
    }
}

void systemidentification::on_antialiasingCheck_toggled(bool checked)
{
    ui->qcpPlotX->graph(0)->setAntialiased(checked);
    ui->qcpPlotX->graph(1)->setAntialiased(checked);
    ui->qcpPlotY->graph(0)->setAntialiased(checked);
    ui->qcpPlotY->graph(1)->setAntialiased(checked);
    ui->qcpPlotW->graph(0)->setAntialiased(checked);
    ui->qcpPlotW->graph(1)->setAntialiased(checked);
}

void systemidentification::vAtualizaRobo()
{
    int robo = ui->robotID->value();
    Atributos atbRobo = futbolEnvironment->rbtRoboAliado[robo]->atbRetornaRobo();

    QVector2D velDecomp = decompoeVelocidadeRobo(QVector2D(dCurrentOutputX,dCurrentOutputY), atbRobo);

    //Era
//    futbolEnvironment->rbtRoboAliado[robo]->vSetaVelocidade(dCurrentOutputX, dCurrentOutputY,
//                                                            dCurrentOutputW, 100);

    futbolEnvironment->rbtRoboAliado[robo]->vSetaVelocidade(velDecomp.x(), velDecomp.y(),
                                                            dCurrentOutputW, 100);
    if(!rdRadio->sendData(atbRobo) )
        ui->statusBar->showMessage("Serial Port disconnected", 500);

    ROBO_SIM simRobo;

    simRobo.velangular = atbRobo.cmd_w;//ret.z();//Gira
    simRobo.velnormal  = -atbRobo.cmd_vx;//ret.y();//Anda pra cima (-x)
    simRobo.veltangent = atbRobo.cmd_vy;//ret.x();//Anda pra frente (y)
    simRobo.id = robo;
    simRobo.wheelsspeed = false;

    grsimSimulator->send_packet(grsimSimulator->create_packet(simRobo,futbolEnvironment->CorTime));
}

void systemidentification::vAmostrar()
{
    QVector3D velRobo = futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->vt3dPegaVelocidadeRobo();
    QVector2D posRobo = futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->vt2dPosicaoAtualRobo();

    float anguloRobo  = futbolEnvironment->rbtRoboAliado[ui->robotID->value()]->atbRetornaRobo().rotation;
    double fatorX = 1, fatorY = 1, fatorW = 1;

    if(ui->radCampoReal->isChecked())
    {
        fatorX = fatorY = dVelocidadeMaximaRobo/100;
        fatorW = dRotacaoMaximoRobo/100;
    }
    QVector3D velEnviada(dCurrentOutputX*fatorX, dCurrentOutputY*fatorY, dCurrentOutputW*fatorW);

    float tempo = timestampTimer.nsecsElapsed()/1e9;

    if((outCurrentOutput == X && ui->individualExcitation->isChecked()) || ui->simultaneousExcitation->isChecked())
    {
//        dataX  += QString::number(tempo) + ";" + QString::number(velEnviada.x()) + ";" + QString::number(velRobo.x()) + ";" +
//                QString::number(posRobo.x()/1e3) + ";" + QString::number(posRobo.y()/1e3) + "\n";
        dataX  += QString::number(tempo) + ";" + QString::number(velEnviada.x()) + ";" + QString::number(velRobo.x()) + ";" +
                QString::number(posRobo.x()/1e3) + ";" + QString::number(posRobo.y()/1e3) + ";" + QString::number(anguloRobo) + "\n";
    }

    if((outCurrentOutput == Y && ui->individualExcitation->isChecked()) || ui->simultaneousExcitation->isChecked())
    {
//        dataY  += QString::number(tempo) + ";" + QString::number(velEnviada.y()) + ";" + QString::number(velRobo.y()) + ";" +
//                QString::number(posRobo.x()/1e3) + ";" + QString::number(posRobo.y()/1e3) + "\n";
        dataY  += QString::number(tempo) + ";" + QString::number(velEnviada.y()) + ";" + QString::number(velRobo.y()) + ";" +
                QString::number(posRobo.x()/1e3) + ";" + QString::number(posRobo.y()/1e3) + ";" + QString::number(anguloRobo) + "\n";

    }

    if((outCurrentOutput == W && ui->individualExcitation->isChecked()) || ui->simultaneousExcitation->isChecked())
    {
//        dataW  += QString::number(tempo) + ";" + QString::number(velEnviada.z()) + ";" + QString::number(velRobo.z()) + ";" +
//                QString::number(anguloRobo) + "\n";
        dataW  += QString::number(tempo) + ";" + QString::number(velEnviada.z()) + ";" + QString::number(velRobo.z()) + ";" +
                QString::number(posRobo.x()/1e3) + ";" + QString::number(posRobo.y()/1e3) + ";" + QString::number(anguloRobo) + "\n";
    }
}

void systemidentification::on_stopButton_clicked()
{
    outCurrentOutput = None;
    dCurrentOutputX = 0;
    dCurrentOutputY = 0;
    dCurrentOutputW = 0;
    ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Interrupted");

    //Stop all timers
    tmrTimerPRBS->stop();
    tmrTimerStep->stop();
    tmrTimerGBN->stop();
    tmrUpdateRobot->stop();
    tmrSampleData->stop();

    //Re-enable the parameters
    if(ui->signalPRBS->isChecked())
    {
        ui->pagePRBSParameters->setEnabled(true);
    }
    else if(ui->signalStep->isChecked())
    {
        ui->pageStepParameter->setEnabled(true);
    }
    else if(ui->signalGBN->isChecked())
    {
        ui->pageGBNParameters->setEnabled(true);
    }
    else if(ui->signalSine->isChecked())
    {
        ui->pageSineWaveParameters->setEnabled(true);
    }
}

QVector2D systemidentification::decompoeVelocidadeRobo(QVector2D tempVel, Atributos &_robot)
{
    float anguloRobo = _robot.rotation;

    float anguloY = 0.0, anguloX = 0.0;
    float _vMax = tempVel.length();//sqrt(pow(tempVel.x(),2) + pow(tempVel.y(),2));

    QVector2D vetorXRobo, vetorYRobo(qCos(anguloRobo), qSin(anguloRobo));

    vetorXRobo = clsAuxiliar::vt2dRotaciona(_robot.vt2dPosicaoAtual, vetorYRobo+_robot.vt2dPosicaoAtual, -90) - _robot.vt2dPosicaoAtual;

    anguloX = clsAuxiliar::fAnguloVetor1Vetor2(vetorXRobo, tempVel)*M_PI/180;
    anguloY = clsAuxiliar::fAnguloVetor1Vetor2(vetorYRobo, tempVel)*M_PI/180;

    tempVel.setX(_vMax * qCos(anguloX));//Rho é o angulo do vetor velocidade com o eixo X do robô
    tempVel.setY(_vMax * qCos(anguloY));//Phi é o angulo do vetor velocidade com o eixo Y do robô

    return tempVel;
}

int systemidentification::iCalculaQuadrante(double _angulo)
{
    if(_angulo >= 0 && _angulo < M_PI/2.0)//Primeiro
    {
        return 1;
    }
    else if(_angulo > M_PI/2.0 && _angulo < M_PI)//Segundo
    {
        return 2;
    }
    else if(_angulo < -M_PI/2.0 && _angulo > -M_PI)//Terceiro
    {
        return 3;
    }
    else if(_angulo < 0 && _angulo > -M_PI/2.0)//Quarto
    {
        return 4;
    }

    return 0;
}

double systemidentification::dGetStepValue(double amplitude)
{
    double out = 0;
    if(dTStep > ui->STEPriseInstantBox->value()/1e3)//ms
    {
       out = amplitude;
    }
    if(dTStep > ui->STEPmaximumTimeBox->value())
    {
        out = -950;
    }
    return out;
}

double systemidentification::dGetSineValue(double amplitude)
{
    double out = 0;
    double freq = ui->SineAngularFrequency->value(); //Angular frequency of the sine wave [rad/s]

    if(dTSine < 15)// Only acquire 15s of data per axis
    {
        out = amplitude * qSin(freq*dTSine);
    }
    else
    {
        out = -950;
    }
    return out;
}

void systemidentification::vGetIndividualPRBS()
{
    double linearAmp = ui->PRBSlinearAmplitudeBox->value();
    double angularAmp = ui->PRBSlinearAmplitudeBox->value();

    switch (outCurrentOutput)
    {
    case X:
    {
        dCurrentOutputX = prbsGenX->dGetValue(linearAmp)  + ui->PRBSbaseValueLinearBox->value();
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;

        if(dCurrentOutputX <= -900)
        {
            outCurrentOutput = Y;
            timestampTimer.restart();
        }
    }break;

    case Y:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = prbsGenY->dGetValue(linearAmp)  + ui->PRBSbaseValueLinearBox->value();
        dCurrentOutputW = 0;

        if(dCurrentOutputY <= -900)
        {
            outCurrentOutput = W;
            timestampTimer.restart();
        }
    }break;

    case W:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = prbsGenW->dGetValue(angularAmp) + ui->PRBSbaseValueAngularBox->value();

        if(dCurrentOutputW <= -900)
        {
            outCurrentOutput = None;
            tmrUpdateRobot->stop();
            tmrTimerPRBS->stop();
            dCurrentOutputX = 0;
            dCurrentOutputY = 0;
            dCurrentOutputW = 0;
            ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished PRBS Response");
            ui->pageStepParameter->setEnabled(true);
            ui->saveButton->setEnabled(true);

            vSaveData();
        }
    }break;

    default://None
    {
    }
        break;
    }
}

void systemidentification::vGetSimultaneousPRBS()
{
    double linearAmp = ui->PRBSlinearAmplitudeBox->value();
    double angularAmp = ui->PRBSangularAmplitudeBox->value();

    dCurrentOutputX = prbsGenX->dGetValue(linearAmp)  + ui->PRBSbaseValueLinearBox->value();
    dCurrentOutputY = prbsGenY->dGetValue(linearAmp)  + ui->PRBSbaseValueLinearBox->value();
    dCurrentOutputW = prbsGenW->dGetValue(angularAmp) + ui->PRBSbaseValueAngularBox->value();

    if(dCurrentOutputW <= -900 && dCurrentOutputY <= -900 && dCurrentOutputX <= -900)
    {
        outCurrentOutput = None;
        tmrTimerPRBS->stop();
        tmrUpdateRobot->stop();
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished PRBS Response");
        ui->pagePRBSParameters->setEnabled(true);
        ui->saveButton->setEnabled(true);

        vSaveData();
    }
}

void systemidentification::vGetIndividualGBN()
{
    double linearAmp = ui->GBNlinearAmplitudeBox->value();
    double angularAmp = ui->GBNlinearAmplitudeBox->value();

    switch (outCurrentOutput)
    {
    case X:
    {
        dCurrentOutputX = gbnGenX->dGetValue(linearAmp)  + ui->GBNlinearBaseValueBox->value();
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;

        if(dCurrentOutputX <= -900)
        {
            outCurrentOutput = Y;
            timestampTimer.restart();
        }
    }break;

    case Y:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = gbnGenY->dGetValue(linearAmp)  + ui->GBNlinearBaseValueBox->value();
        dCurrentOutputW = 0;

        if(dCurrentOutputY <= -900)
        {
            outCurrentOutput = W;
            timestampTimer.restart();
        }
    }break;

    case W:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = gbnGenW->dGetValue(angularAmp) + ui->GBNangularBaseValueBox->value();

        if(dCurrentOutputW <= -900)
        {
            outCurrentOutput = None;
            tmrUpdateRobot->stop();
            tmrTimerGBN->stop();
            dCurrentOutputX = 0;
            dCurrentOutputY = 0;
            dCurrentOutputW = 0;
            ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished GBN Response");
            ui->pageStepParameter->setEnabled(true);
            ui->saveButton->setEnabled(true);

            vSaveData();
        }
    }break;

    default://None
    {
    }
        break;
    }
}

void systemidentification::vGetSimultaneousGBN()
{
    double linearAmp = ui->GBNlinearAmplitudeBox->value();
    double angularAmp = ui->GBNangularAmplitudeBox->value();

    dCurrentOutputX = gbnGenX->dGetValue(linearAmp)  + ui->GBNlinearBaseValueBox->value();
    dCurrentOutputY = gbnGenY->dGetValue(linearAmp)  + ui->GBNlinearBaseValueBox->value();
    dCurrentOutputW = gbnGenW->dGetValue(angularAmp) + ui->GBNangularBaseValueBox->value();

    if(dCurrentOutputW <= -900 && dCurrentOutputY <= -900 && dCurrentOutputX <= -900)
    {
        outCurrentOutput = None;
        tmrTimerGBN->stop();
        tmrUpdateRobot->stop();
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished GBN Response");
        ui->pageGBNParameters->setEnabled(true);
        ui->saveButton->setEnabled(true);

        vSaveData();
    }
}

void systemidentification::vGetIndividualSTEP()
{
    double linearAmp = ui->STEPlinearAmplitudeBox->value();
    double angularAmp = ui->STEPangularAmplitudeBox->value();

    dTStep += tempoAtualizacaoStep/1e3;

    switch (outCurrentOutput)
    {
    case X:
    {
        dCurrentOutputX = dGetStepValue(linearAmp);
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;

        if(dCurrentOutputX <= -900)
        {
            outCurrentOutput = Y;
            dTStep = 0;
            timestampTimer.restart();
        }
    }break;

    case Y:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = dGetStepValue(linearAmp);
        dCurrentOutputW = 0;

        if(dCurrentOutputY <= -900)
        {
            outCurrentOutput = W;
            dTStep = 0;
            timestampTimer.restart();
        }
    }break;

    case W:
    {
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = dGetStepValue(angularAmp);

        if(dCurrentOutputW <= -900)
        {
            outCurrentOutput = None;
            tmrTimerStep->stop();
            dCurrentOutputX = 0;
            dCurrentOutputY = 0;
            dCurrentOutputW = 0;
            ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished Step Response");
            ui->pageStepParameter->setEnabled(true);
            ui->saveButton->setEnabled(true);
            dTStep = 0;

            vSaveData();
        }
    }break;

    default://None
    {
        tmrUpdateRobot->stop();
    }break;

    }
}

void systemidentification::vGetSimultaneousSTEP()
{
    double linearAmp = ui->STEPlinearAmplitudeBox->value();
    double angularAmp = ui->STEPangularAmplitudeBox->value();

    dTStep += tempoAtualizacaoStep/1e3;
    dCurrentOutputX = dGetStepValue(linearAmp);
    dCurrentOutputY = dGetStepValue(linearAmp);
    dCurrentOutputW = dGetStepValue(angularAmp);

    if(dCurrentOutputX <= -900 && dCurrentOutputY <= -900 && dCurrentOutputW <= -900)
    {
        outCurrentOutput = None;
        tmrTimerStep->stop();
        dCurrentOutputX = 0;
        dCurrentOutputY = 0;
        dCurrentOutputW = 0;
        ui->labelStatusIdent->setText(ui->labelStatusIdent->text().section(":", 0,0) + ": Finished Step Response");
        ui->pageStepParameter->setEnabled(true);
        ui->saveButton->setEnabled(true);
        dTStep = 0;

        vSaveData();

        tmrUpdateRobot->stop();
    }
}


void systemidentification::on_signalPRBS_clicked(bool checked)
{
    if(checked == true)
    {
        ui->toolSignals->setItemEnabled(1, true);
        ui->toolSignals->setItemEnabled(2, false);
        ui->toolSignals->setItemEnabled(3, false);
        ui->toolSignals->setItemEnabled(4, false);
    }
}

void systemidentification::on_signalStep_clicked(bool checked)
{
    if(checked == true)
    {
        ui->toolSignals->setItemEnabled(1, false);
        ui->toolSignals->setItemEnabled(2, true);
        ui->toolSignals->setItemEnabled(3, false);
        ui->toolSignals->setItemEnabled(4, false);
    }
}


void systemidentification::on_saveButton_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save data", "Data_", "CSV files (.csv)");

    vOpenFiles(fileName+"X.csv", fileX);
    vOpenFiles(fileName+"Y.csv", fileY);
    vOpenFiles(fileName+"W.csv", fileW);

    vSaveData(dataX, fileX);
    vSaveData(dataY, fileY);
    vSaveData(dataW, fileW);

    fileName.remove("Data_");
    ui->qcpPlotX->savePng(fileName+"XGraph.png");
    ui->qcpPlotY->savePng(fileName+"YGraph.png");
    ui->qcpPlotW->savePng(fileName+"WGraph.png");

    vFillSignalParameters(fileName);
}



void systemidentification::on_radCampoReal_clicked()
{
    if(ui->radCampoReal->isChecked())
    {
        ui->PRBSlinearAmplitudeBox->setSuffix(" %");
        ui->PRBSangularAmplitudeBox->setSuffix(" %");
        ui->PRBSbaseValueLinearBox->setSuffix(" %");
        ui->PRBSbaseValueAngularBox->setSuffix(" %");

        ui->STEPlinearAmplitudeBox->setSuffix(" %");
        ui->STEPangularAmplitudeBox->setSuffix(" %");

        ui->GBNlinearAmplitudeBox->setSuffix(" %");
        ui->GBNangularAmplitudeBox->setSuffix(" %");
        ui->GBNlinearBaseValueBox->setSuffix(" %");
        ui->GBNangularBaseValueBox->setSuffix(" %");

        ui->SineAngularAmplitudeBox->setSuffix(" %");
        ui->SineLinearAmplitudeBox->setSuffix(" %");
    }
}

void systemidentification::on_radGrSim_clicked()
{
    if(ui->radGrSim->isChecked())
    {
        ui->PRBSlinearAmplitudeBox->setSuffix(" m/s");
        ui->PRBSangularAmplitudeBox->setSuffix(" rad/s");
        ui->PRBSbaseValueLinearBox->setSuffix(" m/s");
        ui->PRBSbaseValueAngularBox->setSuffix(" rad/s");

        ui->STEPlinearAmplitudeBox->setSuffix(" m/s");
        ui->STEPangularAmplitudeBox->setSuffix(" rad/s");

        ui->GBNlinearAmplitudeBox->setSuffix(" m/s");
        ui->GBNangularAmplitudeBox->setSuffix(" rad/s");
        ui->GBNlinearBaseValueBox->setSuffix(" m/s");
        ui->GBNangularBaseValueBox->setSuffix(" rad/s");


        ui->SineAngularAmplitudeBox->setSuffix(" rad/s");
        ui->SineLinearAmplitudeBox->setSuffix(" m/s");
    }
}

void systemidentification::on_signalGBN_clicked(bool checked)
{
    if(checked)
    {
        ui->toolSignals->setItemEnabled(1, false);
        ui->toolSignals->setItemEnabled(2, false);
        ui->toolSignals->setItemEnabled(3, true);
        ui->toolSignals->setItemEnabled(4, false);
    }
}

void systemidentification::on_signalSine_clicked(bool checked)
{
    if(checked)
    {
        ui->toolSignals->setItemEnabled(1, false);
        ui->toolSignals->setItemEnabled(2, false);
        ui->toolSignals->setItemEnabled(3, false);
        ui->toolSignals->setItemEnabled(4, true);
    }
}

void systemidentification::on_createModel_clicked()
{
    if(matlabRuntDir.isEmpty())
        matlabRuntDir = QFileDialog::getExistingDirectory(this, tr("Please select MATLAB Runtime folder: ../MATLAB/R2019a/"),
                                                        QDir::currentPath(), QFileDialog::ShowDirsOnly);
    if(matlabAppDir.isEmpty())
        matlabAppDir =  QFileDialog::getOpenFileName(this, tr("Please select the System Identification App script (.sh)"),
                                                        QDir::currentPath(),"*.sh");

    QString warningString = strCheckMatlabRuntimeFolder() + "\n\n" + strCheckMatlabAppFolder();

    if(warningString != "\n\n")
    {
        QMessageBox msgBox;
        msgBox.setText(warningString);
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.exec();
        matlabRuntDir.clear();
        matlabAppDir.clear();
    }

    if(!matlabRuntDir.isEmpty() && !matlabAppDir.isEmpty() && !matlabAppThread.isRunning())
    {
        QString openAppString = matlabAppDir + " " + matlabRuntDir;
        matlabAppThread = QtConcurrent::run(runMATLABApp,openAppString);
    }
    else
    {
        warningString = "App is already running.";
    }
}

void systemidentification::on_setRuntimeFolder_clicked()
{
    matlabRuntDir = QFileDialog::getExistingDirectory(this, tr("Select MATLAB Runtime folder: ../MATLAB/R2019a/"),
                                                    QDir::currentPath(),
                                                    QFileDialog::ShowDirsOnly);

    QString warningString = strCheckMatlabRuntimeFolder();
    QMessageBox msgBox;

    if(!warningString.isEmpty())
    {
        msgBox.setText(warningString);
        msgBox.setIcon(QMessageBox::Warning);
        matlabAppDir.clear();

    }
    else
    {
        msgBox.setText("Valid folder!");
        msgBox.setIcon(QMessageBox::Information);
    }
    ui->runtimeFolder->setText(matlabRuntDir);
    msgBox.exec();
}

void systemidentification::on_setMATLABAppFolder_clicked()
{
    matlabAppDir =  QFileDialog::getOpenFileName(this, tr("Select the System Identification App script (.sh)"),
                                                    QDir::currentPath(),"*.sh");

    QString warningString = strCheckMatlabAppFolder();
    QMessageBox msgBox;

    if(!warningString.isEmpty())
    {
        msgBox.setText(warningString);
        msgBox.setIcon(QMessageBox::Warning);
        matlabAppDir.clear();
    }
    else
    {
        msgBox.setText("Valid app!");
        msgBox.setIcon(QMessageBox::Information);
    }
    ui->appFolder->setText(matlabAppDir);
    msgBox.exec();

}
